import React from 'react';
import {
  Router,
  Route,
  // IndexRoute,
  IndexRedirect,
} from 'dva/router';
import frame from './views/frame';
import home from './views/home';
import appList from './views/app/list';
import userManager from './views/userManager/list';
import indexList from './views/index/list';
import datasourceList from './views/datasource/list';
import controlList from './views/control/list';
import logon from './views/logon/index';
import OverView from './views/overview';
import IndexView from './views/indexview';
import ApiView from './views/apiview';
import HardwareView from './views/hardwareview';

export default function ({ history }) { // eslint-disable-line
  return (<Router history={history}>
    <Route path="/" component={frame}>
      <IndexRedirect to="/searchcloud/home" />
      <Route path="searchcloud/home" component={home} />
      <Route path="searchcloud/app" component={appList} />
      <Route path="searchcloud/index" component={indexList} />
      <Route path="searchcloud/datasource" component={datasourceList} />
      <Route path="searchcloud/control" component={controlList} />
      <Route path="searchcloud/logon" component={logon} />
      <Route path="searchcloud/overview" component={OverView} />
      <Route path="searchcloud/indexview" component={IndexView} />
      <Route path="searchcloud/apiview" component={ApiView} />
      <Route path="searchcloud/hardwareview" component={HardwareView} />
      <Route path="searchcloud/userManager" component={userManager} />
    </Route>
  </Router>);
}
