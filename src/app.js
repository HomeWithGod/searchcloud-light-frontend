import dva from 'dva';
import { browserHistory } from 'dva/router';
import createLoading from 'dva-loading';

// 1. Initialize
const app = dva({
  history: browserHistory,
});

window.apps = app;

app.use(createLoading());

// 2. Model
app.model(require('./models/frame'));
app.model(require('./models/app'));
app.model(require('./models/index'));
app.model(require('./models/datasource'));
app.model(require('./models/control'));
app.model(require('./models/createIndex'));
app.model(require('./models/logon'));
app.model(require('./models/overview'));
app.model(require('./models/indexview'));
app.model(require('./models/apiview'));
app.model(require('./models/hardwareview'));
app.model(require('./models/userManager'));

// 3. Router
app.router(require('./router'));

// 4. Start
app.start('#app');
