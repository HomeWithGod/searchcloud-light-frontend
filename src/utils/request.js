import { message } from 'antd';
import _ from 'underscore';

const Ajax = require('axios');

const delayCloseTime = 5;

const prefix = '/web';

const converterMap = {};

const info = function (error, text) {
  message.error(
    error && error.response && error.response.data && error.response.data.message || '服务器错误',
    delayCloseTime,
  );
};

const httpStatus = {
  409: (error) => {
    if (error && error.response && error.response.data) {
      const data = error.response.data;
      if (data.message) {
        info(error, data.message);
      } else if (data.fieldErrors && !_.isEmpty(data.fieldErrors)) {
        return data;
      } else {
        info(error, '服务器错误');
      }
    } else {
      info(error, '服务器错误');
    }
  },
  500: error => info(error, '服务器错误'),
  403: error => info(error, '权限错误'),
  401: error => error.response.data,
};

function errorHandle(error) {
  const code = error && error.response && error.response.status;
  let err;
  if (code && httpStatus[code]) {
    err = httpStatus[error.response.status](error);
  } else {
    info(error, '服务器错误');
  }
  return err ? { error: err } : { error };
}

export function request(url, options, urlPattern) {
  const method = options.method || 'GET';
  const converter = converterMap[`${method} ${urlPattern || url}`] || {};
  const preHandle = converter.pre;
  const postHandle = converter.post;
  const proxyHandle = converter.proxy; // 为什么要弄这玩意替换请求的method和url？u guess, haha...
  let proxy,
    params;

  if (options.params) {
    proxy = proxyHandle && proxyHandle(options.params);
    options.params = preHandle && preHandle(options.params) || options.params;
  } else if (options.data) {
    proxy = proxyHandle && proxyHandle(options.data);
    options.data = preHandle && preHandle(options.data) || options.data;
  }

  if ((options.method === 'GET' || !options.method) && options.data) {
    options.params = options.data;
    options.data = null;
  }

  if ((options.method === 'POST' || options.method === 'PUT') && !options.data && options.params) {
    options.data = options.params;
    options.params = null;
  }

  return Ajax({
    method: proxy && proxy.method || options.method || 'GET',
    url: prefix + (proxy && proxy.url || url),
    data: options.data || {},
    params: params || options.params || {},
  })
  .then((response) => {
    const result = postHandle && postHandle(response.data);
    return { data: result || response.data || '' };
  })
  .catch(error => errorHandle(error),
  );
}

export function upload(url, data) {
  const form = new FormData(); // FormData 对象
  _.each(
    data,
    (value, key) => {
      form.append(key, value);
    },
  );
  return Ajax.post(
    prefix + url,
    form,
    {
      method: 'post',
      headers: { 'Content-Type': 'multipart/form-data' },
    }).then(response => ({ data: response.data || '' }))
    .catch(error => errorHandle(error),
  );
}

export function addConverter(method, url, { pre, post, proxy }) {
  let converter = converterMap[`${method} ${url}`];
  if (!converter) {
    converter = converterMap[`${method} ${url}`] = {};
  }

  pre && (converter.pre = pre);
  post && (converter.post = post);
  proxy && (converter.proxy = proxy);
}

export function removeConverter(method, url, { pre, post, proxy }) {
  const converter = converterMap[`${method} ${url}`];
  if (converter) {
    if (pre) {
      delete converter.pre;
    }
    if (post) {
      delete converter.post;
    }
    if (proxy) {
      delete converter.proxy;
    }
  }
}
