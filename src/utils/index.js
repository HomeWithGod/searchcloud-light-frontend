import * as ajax from './request';
import constants from './constants';
import transform from './transform';
import tools from './tools';

export default {
  ...ajax,
  constants,
  transform,
  tools,
};
