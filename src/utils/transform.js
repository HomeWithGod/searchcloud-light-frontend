import React, { PropTypes } from 'react';
import { Select, Form, Input, Button } from 'antd';
import moment from 'moment';
import _ from 'underscore';

const Option = Select.Option;
const FormItem = Form.Item;

export default {
  getDate(time, type = 'YYYY-MM-DD') {
    const date = moment(parseInt(time, 10));
    return date.isValid() ? date.format(type) : '';
  },

  addKey(arr) {
    return _.map(arr, (item, key) => {
      item.key = key;
      return item;
    });
  },

  objectToHashString(obj) {
    let str = '?';
    if (!obj) {
      return str;
    }
    for (const key in obj) {
      if (obj[key]) {
        str += `${key}=${obj[key]}&`;
      }
    }
    return str;
  },

  selectOptions(constantsTypes, fieldOp) {
    if (!_.isArray(constantsTypes)) {
      const options = [];
      _.each(constantsTypes, (value, key) => {
        let trueValue;
        let disabled = false;
        if (_.isObject(value)) {
          trueValue = value.value;
          disabled = value.disabled;
        } else {
          trueValue = value;
        }
        return key !== '-1'
          ? options.push(
            <Option key={key} value={key} disabled={disabled}>
              {trueValue}
            </Option>,
            )
          : options.unshift(
            <Option key={key} value={key} disabled={disabled}>
              {trueValue}
            </Option>,
            );
      });
      return options;
    }
    return constantsTypes.reduce((options, value, key) => {
      let realKey;
      let display;
      let disabled = false;
      if (fieldOp) {
        realKey = `${value[fieldOp.key]}`;
        display = value[fieldOp.value];
        disabled = fieldOp.disable && value[fieldOp.disable];
      } else {
        realKey = `${key}`;
        display = value;
      }

      options.push(
        <Option key={realKey} value={realKey} disabled={disabled}>
          {display}
        </Option>,
      );
      return options;
    }, []);
  },

  radioOptions(constantsTypes, fieldOp) {
    if (!_.isArray(constantsTypes)) {
      const options = [];
      _.each(constantsTypes, (value, key) =>
        options.push({ label: value, value: key }),
      );
      return options;
    }
    return constantsTypes.reduce((options, value, key) => {
      let realKey;
      let display;
      if (fieldOp) {
        realKey = value[fieldOp.key];
        display = value[fieldOp.value];
      } else {
        realKey = key;
        display = value;
      }

      options.push({ label: value, value: realKey });
      return options;
    }, []);
  },

  fieldsToParams(fields, transformer) {
    return { ...fields };
  },

  createPageConfig(model, query) {
    return {
      pageSize: model.searchParams.pageSize,
      total: model.total,
      current: model.searchParams.pageNo,
      onChange(page, pageSize) {
        const params = {
          ...model.searchParams,
          pageNo: page,
          pageSize,
        };
        query(params);
      },
    };
  },

  formatJson(json, options) {
    let reg = null,
      formatted = '',
      pad = 0,
      PADDING = '    '; // one can also use '\t' or a different number of spaces

    // optional settings
    options = options || {};
    // remove newline where '{' or '[' follows ':'
    options.newlineAfterColonIfBeforeBraceOrBracket =
      options.newlineAfterColonIfBeforeBraceOrBracket === true;
    // use a space after a colon
    options.spaceAfterColon = options.spaceAfterColon !== false;

    // begin formatting...
    if (typeof json !== 'string') {
      // make sure we start with the JSON as a string
      json = JSON.stringify(json);
    } else {
      // is already a string, so parse and re-stringify in order to remove extra whitespace
      json = JSON.parse(json);
      json = JSON.stringify(json);
    }

    // add newline before and after curly braces
    reg = /([\{\}])/g;
    json = json.replace(reg, '\r\n$1\r\n');

    // add newline before and after square brackets
    reg = /([\[\]])/g;
    json = json.replace(reg, '\r\n$1\r\n');

    // add newline after comma
    reg = /(\,)/g;
    json = json.replace(reg, '$1\r\n');

    // remove multiple newlines
    reg = /(\r\n\r\n)/g;
    json = json.replace(reg, '\r\n');

    // remove newlines before commas
    reg = /\r\n\,/g;
    json = json.replace(reg, ',');

    // optional formatting...
    if (!options.newlineAfterColonIfBeforeBraceOrBracket) {
      reg = /\:\r\n\{/g;
      json = json.replace(reg, ':{');
      reg = /\:\r\n\[/g;
      json = json.replace(reg, ':[');
    }
    if (options.spaceAfterColon) {
      reg = /\:/g;
      json = json.replace(reg, ':');
    }

    json.split('\r\n').forEach((node, index) => {
      let i = 0,
        indent = 0,
        padding = '';

      if (node.match(/\{$/) || node.match(/\[$/)) {
        indent = 1;
      } else if (node.match(/\}/) || node.match(/\]/)) {
        if (pad !== 0) {
          pad -= 1;
        }
      } else {
        indent = 0;
      }

      for (i = 0; i < pad; i++) {
        padding += PADDING;
      }

      formatted += `${padding + node}\r\n`;
      pad += indent;
    });

    return formatted;
  },
};
