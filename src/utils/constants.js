import moment from 'moment';

export default {
  appUrlPrefix: '/searchcloud',
  systemConstants: {
    dateFormat: {
      frontEnd: 'YYYY-MM-DD',
      backEnd: 'YYYY-MM-DD HH:mm:s',
    },
    range: [moment().add(-3, 'months'), moment()],
    range2: [moment().add(-20, 'days'), moment()],
    range3: ['', ''],
    range4: ['', ''],
    pageSize: 10,
    pageSizeOption: ['10', '30', '50'],
    app: {
      status: {
        0: '正常',
        1: '停用',
      },
    },
    index: {
      status: {
        1: '停用',
        0: '正常',
      },
      metadataStatus: {
        1: '停用',
        0: '正常',
      },
    },
    warnStatus: {
      1: '已预警',
      2: '未预警',
    },
    warnTypeMap: {
      indexview: 1,
      apiview: 2,
      hardwareview: 3,
    },
    control: {
      status: {
        5: '暂停',
        4: '终止',
        3: '失败',
        2: '成功',
        1: '执行中',
        0: '待执行',
      },
      indexBuildType: {
        0: '全量',
        1: '增量',
      },
      queryOptimizeType: {
        0: '否',
        1: '是',
      },
      gradingType: {
        0: '全索引',
        1: '单数据源',
      },
      runType: {
        0: '定时触发',
        1: '人工触发 ',
      },
    },
    datasource: {
      incDatasourceType: {
        1: '配置增量',
      },
      incDatasourceBufferStatus: {
        0: '启用',
        1: '停用',
      },
      datasourceTypes: {
        0: 'JSON_RPC',
        1: { value: '消息队列（暂不支持）', disabled: true },
        2: { value: '数据库SQL（暂不支持）', disabled: true },
      },
    },
    frame: {
      env: {
        1: 'dev',
        2: 'st',
        3: 'anhouse',
        4: 'ga',
      },
    },
    warn: {
      operators: {
        0: '=',
        1: '>',
        2: '<',
      },
    },
  },
  crumbMap: {
    '/app': {
      key: '/app',
      name: '项目管理',
      child: [
        {
          key: '/index',
          name: '索引管理',
          parameterKey: 'projectId',
          child: [
            {
              key: '/datasource',
              name: '数据源管理',
            },
            {
              key: '/control',
              name: '调度管理',
            },
            {
              key: '/query',
              name: '数据查询',
            },
          ],
        },
      ],
    },
    '/userManager': {
      key: '/userManager',
      name: '用户管理',
    },
    '/overview': {
      key: '/overview',
      name: '总览图',
    },
    '/indexview': {
      key: '/indexview',
      name: '索引视图',
      // "parameterKey": "projectId",
    },
    '/apiview': {
      key: '/apiview',
      name: 'API视图',
      // "parameterKey": "projectId",
    },
    '/hardwareview': {
      key: '/hardwareview',
      name: '硬件视图',
      // "parameterKey": "projectId",
    },
  },
};
