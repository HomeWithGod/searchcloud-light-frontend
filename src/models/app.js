import { list, detail, update, create, remove } from "../services/app";
import { getUsers } from "src/services/frame";
import {
  stateCreate,
  effectsCreate,
  reducers,
  dataHandle
} from "src/models/modelCreator";

const localDataHandle = {
  ...dataHandle,
  *usersHandle({ data, error }, { put }) {
    if (data) {
      yield put({
        type: "loadUsersSuccess",
        payload: { users: data.datas }
      });
    } else if (error && error.fieldErrors) {
      yield put({
        type: "putError",
        payload: {
          fieldErrors: error.fieldErrors
        }
      });
    }
  }
};

export default {
  namespace: "app",
  state: {
    ...stateCreate(),
    moduleName: "app",
    entityDescription: "项目",
    searchParams: {},
    users: [] // 用户下拉列表选项（除了当前用户）
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname, query }) => {
        if (pathname === "/searchcloud/app") {
          dispatch({
            type: "getUsers"
          });
        }
      });
    }
  },
  effects: {
    ...effectsCreate({ list, detail, update, create, remove }, localDataHandle),

    *list({ payload }, { call, put, select }) {
      const searchParams = yield select(
        state => state[payload.module].searchParams
      );
      const { data, error } = yield call(list, searchParams);
      yield dataHandle.nakeListHandle({ data, error }, { put });
    },

    *getUsers({ payload }, { call, put }) {
      const { data } = yield call(getUsers);
      yield put({
        type: "updateState",
        payload: {
          users: data.data
        }
      });
    }
  },
  reducers: {
    ...reducers,
    updateState(state, action) {
      return {
        ...state,
        ...action.payload
      };
    }
  }
};
