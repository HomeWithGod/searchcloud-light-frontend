import { list, detail, update, create, remove, projectList, indexList, chartList, warningConfigList, deleteWarnCfg, saveDetail } from '../services/indexview';
import { users } from 'src/services/frame';
import { stateCreate, effectsCreate, reducers, dataHandle } from 'src/models/modelCreator';
import { browserHistory } from 'react-router';
import { systemConstants, appUrlPrefix } from 'src/utils/constants';

const moduleName = 'indexview';

export default {
  namespace: 'indexview',
  state: {
    ...stateCreate(),
    moduleName: 'indexview',
    entityDescription: '预警',
    searchParams: {
      selectedProject: '-1',
      selectedIndex: '-1',
      selectedWarnStatus: '-1',
    },
    trigger: null,
    pagination: {
      pageNo: 1,
      pageSize: 10,
    },
    currentTab: '',
    selections: {
      project: {},
      index: {},
      warnStatus: {
        ...systemConstants.warnStatus,
      },
    },
    relatedData: {
      project: {
        statisticsList: [],
        chart: {
          dataSet: [],
        },
      },
      index: {
        statisticsList: [],
        chart: {
          dataSet: [],
        },
      },
      warn: {
        statisticsList: [],
        chart: {
          dataSet: [],
        },
      },
      default: {
        statisticsList: [],
        chart: {
          dataSet: [],
        },
      },
    },

  },
  effects: {
    ...effectsCreate({ list, detail, update, create, remove, warningConfigList }, dataHandle),

    * list({ payload }, { call, put, select }) {
      let currentModule = yield select(state => state[payload.module]);
      if (currentModule.trigger === 'clickFormSubmitButton') {
        const { selectedProject, selectedIndex } = currentModule.searchParams;

        if (selectedProject != -1) {
          if (selectedIndex != -1) {
            yield put({ type: 'tabChange', payload: 'index' });
          } else {
            yield put({ type: 'tabChange', payload: 'project' });
          }
        } else {
          yield put({ type: 'tabChange', payload: '' });
        }
      } else if (!currentModule.trigger || currentModule.trigger === 'clickTabRadio') {
        const { currentTab } = currentModule;
        const { selectedProject } = currentModule.searchParams;
        let selectProject = selectedProject || '-1',
          selectIndex = '-1';
        if (currentTab === 'project') {
          selectProject == '-1' && (selectProject = Object.keys(currentModule.selections.project)[0]);
        } else if (currentTab === 'index') {
          selectProject == '-1' && (selectProject = Object.keys(currentModule.selections.project)[0]);
          yield call(indexList, selectProject);
          selectIndex = Object.keys(currentModule.selections.index)[0];
        }
        yield put({ type: 'selectProject', payload: selectProject });
        yield put({ type: 'selectIndex', payload: selectIndex });
        currentModule = yield select(state => state[payload.module]);
      }

      const { pagination, searchParams } = currentModule;
      const { pageNo, pageSize } = pagination;

        // console.log(searchParams)
      const { data, error } = yield call(list, { ...searchParams, pageNo, pageSize });
      yield dataHandle.relatedDataHandle({ data, error }, { put });
      if (searchParams.selectedProject == -1 || searchParams.selectedIndex == -1) {
        const chartData = yield call(chartList, searchParams);
        yield put({ type: 'updateChartData', payload: chartData.data });
      }
      yield put({ type: 'updateTrigger', payload: '' });
    },

    * projectList({ payload }, { call, put, select }) {
      let { data, error } = yield call(projectList);
      data = { project: data };
      yield dataHandle.selectionHandle({ data, error }, { put });
    },

    * initSelections({ payload }, { call, put, select }) {
      let rsp = yield call(projectList);
      yield dataHandle.selectionHandle({ data: { project: rsp.data }, error: rsp.data }, { put });
      const firstProject = Object.keys(rsp.data)[0];
      rsp = yield call(indexList, firstProject);
      yield dataHandle.selectionHandle({ data: { index: rsp.data }, error: rsp.error }, { put });
    },

    * indexList({ payload }, { call, put, select }) {
        // const searchParams = yield select(state => state[payload.module]['searchParams']);
      let { data, error } = yield call(indexList, payload.projectId);
      data = { index: data };
      yield dataHandle.selectionHandle({ data, error }, { put });
      yield put({ type: 'selectIndex', payload: '-1' });
    },

    * deleteWarnCfg({ payload }, { call, put, select }) {
      const { data } = yield call(deleteWarnCfg, payload);
      if (data) {
        if (data.success) {
          yield put({ type: 'deleteSuccess' });
          yield put({ type: 'warningConfigList', payload: { module: moduleName } });
        } else {
          yield put({
            type: 'putGlobalError',
            payload: {
              message: data.reason,
            },
          });
        }
      } else {
        yield put({
          type: 'putGlobalError',
          payload: {
            message: '未知错误',
          },
        });
      }
    },
    * saveDetail({ payload }, { call, put, select }) {
      const { data } = yield call(saveDetail, payload.param);
      if (data) {
        if (data.success) {
          yield put({ type: 'closeDetailModal' });
          yield put({ type: 'warningConfigList', payload: { module: moduleName } });
        } else {
          yield put({
            type: 'putGlobalError',
            payload: {
              message: data.reason,
            },
          });
        }
      } else {
        yield put({
          type: 'putGlobalError',
          payload: {
            message: '未知错误',
          },
        });
      }
    },
  },
  reducers: {
    ...reducers,
    tabChange(state, action) {
      return {
        ...state,
        currentTab: action.payload,
      };
    },
    clearSearchParams(state, action) {
      return {
        ...state,
        searchParams: {},
      };
    },
    changePage(state, action) {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          ...action.payload,
        },
      };
    },
    relatedDataSuccess(state, action) {
      const currentTab = state.currentTab || 'default';
      return {
        ...state,
        relatedData: {
          ...state.relatedData,
          [currentTab]: {
            ...action.payload.datas,
          },
        },
        pagination: {
          ...state.pagination,
          total: action.payload.total,
        },
      };
    },

    updateSelection(state, action) {
      return {
        ...state,
        selections: {
          ...state.selections,
          ...action.payload,
        },
      };
    },

    updateTrigger(state, action) {
      return {
        ...state,
        trigger: action.payload,
      };
    },

    updateChartData(state, action) {
      const currentTab = state.currentTab || 'default';
      return {
        ...state,
        relatedData: {
          ...state.relatedData,
          [currentTab]: {
            ...state.relatedData[currentTab],
            chart: action.payload,
          },
        },
      };
    },

    selectProject(state, action) {
      return {
        ...state,
        searchParams: {
          ...state.searchParams,
          selectedProject: action.payload,
        },
      };
    },

    selectIndex(state, action) {
      return {
        ...state,
        searchParams: {
          ...state.searchParams,
          selectedIndex: action.payload,
        },
      };
    },
  },
    // subscriptions: {
    //   setup({ dispatch, history }) {
    //     history.listen(({ pathname, query }) => {
    //       if (!/\/indexview$/.test(pathname)) {
    //         dispatch({
    //           type: 'clearSearchParams',
    //           payload: { pathname, query },
    //         });
    //       }
    //     });
    //   },
    // },
};
