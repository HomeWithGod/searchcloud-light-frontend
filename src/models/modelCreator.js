import { systemConstants } from 'src/utils/constants';
import { notification } from 'antd';

const constants = {
  stateCreate() {
    return {
      fieldErrors: {},
      total: 0,
      entity: {},
      detailModalTitle: '',
      showDetailModal: false,
      searchParams: {
        pageSize: systemConstants.pageSize,
        pageNo: 1,
      },
      list: [],
    };
  },

  effectsCreate(
    { list, detail, update, create, remove, warningConfigList },
    dataHandle,
  ) {
    return {
      * list({ payload }, { call, put, select }) {
        const searchParams = yield select(
          state => state[payload.module].searchParams,
        );
        const { data, error } = yield call(list, searchParams);
        yield dataHandle.listHandle({ data, error }, { put });
      },
      * saveDetail({ payload }, { call, put, select }) {
        const { data, error } = payload.param.id
          ? yield call(update, payload.param)
          : yield call(create, payload.param);
        yield dataHandle.saveDetailFinishHandle({ data, error }, { put });
        if (!error) {
          yield put({
            type: 'list',
            payload: { module: payload.module },
          });
        }
      },
      * deleteItem({ payload }, { call, put, select }) {
        const { data, error } = yield call(remove, payload.id);
        yield dataHandle.deleteHandle({ data, error }, { put });
        yield put({
          type: 'list',
          payload: { module: payload.module },
        });
      },

      * read({ payload }, { call, put, select }) {
        const { data, error } = yield call(detail, payload);
        yield dataHandle.detailHandle({ data, error }, { put });
      },

      * warningConfigList({ payload }, { call, put, select }) {
        const currentModule = yield select(state => state[payload.module]);
        const searchParams = currentModule.searchParams;
        const { pageNo, pageSize } = currentModule.pagination;
        const warningType = systemConstants.warnTypeMap[payload.module];
        const { data, error } = yield call(warningConfigList, {
          ...searchParams,
          pageNo,
          pageSize,
          warningType,
        });
        yield dataHandle.relatedDataHandle({ data, error }, { put });
      },
    };
  },

  reducers: {
    paramsChange(state, action) {
      return {
        ...state,
        searchParams: {
          ...state.searchParams,
          ...action.payload,
        },
      };
    },

    putGlobalError(state, action) {
      const { formId, fieldErrors, message } = action.payload;
      let description = '';
      if (message) {
        description = message;
      } else if (fieldErrors) {
        const keys = Object.keys(fieldErrors);

        description = keys
          .reduce((text, key) => {
            text.push(fieldErrors[key]);
            return text;
          }, [])
          .join('\n');
      }

      notification.error({
        message: '错误提示',
        description,
        duration: 0,
        placement: 'topLeft',
      });

      return { ...state };
    },

    putGlobalInfo(state, action) {
      const { message } = action.payload;
      if (message) {
        const description = message;
        notification.info({
          message: '提示',
          description,
          duration: 0,
          placement: 'topLeft',
        });
      }
      return { ...state };
    },

    putFieldError(state, action) {
      const { formId, fieldErrors } = action.payload;
      const errorText = formId ? `${formId}fieldErrors` : 'fieldErrors';

      const keys = Object.keys(fieldErrors);
      const realErrors = {};
      Object.keys(fieldErrors).forEach((key) => {
        realErrors[`entity.${key}`] = fieldErrors[key];
      });
      return {
        ...state,
        [errorText]: realErrors,
      };
    },

    putError(state, action) {
      const { formId, fieldErrors } = action.payload;
      const errorText = formId ? `${formId}fieldErrors` : 'fieldErrors';

      const keys = Object.keys(fieldErrors);
      const realErrors = {};
      Object.keys(fieldErrors).forEach((key) => {
        realErrors[`entity.${key}`] = fieldErrors[key];
      });
      return {
        ...state,
        [errorText]: realErrors,
      };
    },

    nakeListSuccess(state, action) {
      let datas = action.payload || [];
      datas = datas.map((item, index) => ({
        ...item,
        key: index + new Date().getTime(),
        antOrderNumber: index + 1,
      }));
      return {
        ...state,
        list: datas,
      };
    },

    success(state, action) {
      const { pageNo, pageSize } = state.searchParams;
      const datas = action.payload.datas.map((item, index) => ({
        ...item,
        key: index + new Date().getTime(),
        antOrderNumber: index + 1 + (pageNo - 1) * pageSize,
      }));

      return {
        ...state,
        list: datas,
        total: action.payload.total,
        searchParams: {
          ...state.searchParams,
          pageNo: action.payload.pageNo,
          pageSize: action.payload.pageSize,
        },
      };
    },

    loadDetailSuccess(state, action) {
      return {
        ...state,
        entity: action.payload,
      };
    },

    deleteSuccess(state, action) {
      return { ...state };
    },

    showDetailModal(state, action) {
      const { modalType } = action.payload;
      let prefix = '';

      if (modalType === 'create') {
        prefix = '新建';
      } else if (state.entity) {
        if (modalType === 'update') {
          prefix = '修改';
        } else {
          prefix = '查看';
        }
      }
      return {
        ...state,
        detailModalTitle: prefix + state.entityDescription,
        showDetailModal: true,
        modalType,
      };
    },
    closeDetailModal(state) {
      return {
        ...state,
        entity: {},
        fieldErrors: {},
        showDetailModal: false,
      };
    },

    clearError(state) {
      return {
        ...state,
        fieldErrors: {},
      };
    },
  },

  dataHandle: {
    * nakeListHandle({ data, error }, { put }) {
      console.log(data);
      if (data) {
        yield put({
          type: 'nakeListSuccess',
          payload: data,
        });
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },

    * listHandle({ data, error }, { put }) {
      if (data) {
        yield put({
          type: 'success',
          payload: data,
        });
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },
    * detailHandle({ data, error }, { put }) {
      if (error && error.fieldErrors) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      } else {
        yield put({
          type: 'loadDetailSuccess',
          payload: data,
        });
      }
    },
    * deleteHandle({ data, error }, { put }) {
      if (error && error.fieldErrors) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      } else {
        yield put({
          type: 'deleteSuccess',
        });
      }
    },
    * saveDetailFinishHandle({ data, error }, { put }) {
      if (error && error.fieldErrors) {
        yield put({
          type: 'putFieldError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      } else {
        yield put({
          type: 'closeDetailModal',
        });
      }
    },

    * relatedDataHandle({ data, error }, { put }) {
      if (data) {
        yield put({
          type: 'relatedDataSuccess',
          payload: data,
        });
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },

    * selectionHandle({ data, error }, { put }) {
      if (data) {
        yield put({
          type: 'updateSelection',
          payload: data,
        });
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },
  },
};

export default constants;
