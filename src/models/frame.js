import { parse } from 'qs';
import { notification, message } from 'antd';
import _ from 'underscore';
import { routerRedux } from 'dva/router';

import { userInfo, logout, envInfo, getMenu } from 'src/services/frame';
import { crumbMap, appUrlPrefix } from 'src/utils/constants';
import { addUser, editUser, editPassword } from '../services/userManager';

const MENU = [
  {
    key: '/app',
    name: '项目管理',
    path: `${appUrlPrefix}/app`,
  },
  {
    key: '/userManager',
    name: '用户管理',
    path: `${appUrlPrefix}/userManager`,
  },
  {
    key: '/reportAndWarn',
    name: '报表及预警',
    child: [
      {
        key: '/overview',
        name: '总览图',
        path: `${appUrlPrefix}/overview`,
      },
      {
        key: '/indexview',
        name: '索引视图',
        path: `${appUrlPrefix}/indexview`,
      },
      {
        key: '/apiview',
        name: 'API视图',
        path: `${appUrlPrefix}/apiview`,
      },
      {
        key: '/hardwareview',
        name: '硬件视图',
        path: `${appUrlPrefix}/hardwareview`,
      },
    ],
  },
];

function* getCrumbItem(flattenMap, pathname) {
  if (flattenMap[pathname]) {
    const { key, name, parent, path } = flattenMap[pathname];
    yield { key, name, path };
    yield* getCrumbItem(flattenMap, parent);
  }
}

function* flatCrumb(data = {}, parent, query) {
  const { key, name, child, parameterKey } = data;
  let path = key;
  if (query && parameterKey && query[parameterKey]) {
    path += `?${parameterKey}=${query[parameterKey]}`;
  }
  path = appUrlPrefix + path;
  yield { key, name, parent, path };
  if (child) {
    for (let i = 0; i < child.length; i++) {
      // yield* flatCrumb(child[i], key, query);
      yield* flatCrumb(child[i], key, query);
    }
  }
}

function parseCrumb({ pathname, query }) {
  pathname = pathname.split(appUrlPrefix)[1];
  const flattenMap = {};
  for (const flattenItem of flatCrumb(crumbMap[pathname], 0, query)) {
    flattenMap[flattenItem.key] = flattenItem;
  }
  console.log(flattenMap);
  const crumbList = [];
  for (const crumbItem of getCrumbItem(flattenMap, pathname)) {
    crumbList.push(crumbItem);
  }
  return crumbList.reverse();
}

// 将树形结构转为一维数组
function treeToArray(data, arr = []) {
  data.forEach((item) => {
    arr.push(item);
    if (item.child) {
      treeToArray(item.child, arr);
    }
  });
  return arr;
}

function getOpenedMenu(menu) {
  const result = [];
  const menuArr = treeToArray(MENU);
  menu.forEach((item) => {
    let obj = {};
    const target = _.filter(menuArr, i => i.name === item.name)[0];
    if (target) {
      obj = {
        ...target,
        child: item.subList ? getOpenedMenu(item.subList) : [],
      };
      result.push(obj);
    }
  });
  return result;
}

export default {
  namespace: 'frame',
  state: {
    login: false,
    userName: '',
    userId: '',
    roleId: null,
    containerClass: 'layout-container',
    crumbList: [],
    showPasswordModal: false,
    fieldErrors: {},
    header: {
      className: '',
      current: '',
    },
    sidebar: {
      sideBarClassName: 'searchcloud-sidebar',
      switchBoxClassName: '',
      openKeys: ['/reportAndWarn'],
      current: '/app',
      switchBoxText: '',
    },
    sidebarJson: [],
    showHeader: false, // 是否显示头部，默认不显示
  },
  subscriptions: {
    setup({ dispatch, history }) {
      dispatch({ type: 'getUser' });
      dispatch({ type: 'getEnv' });
      dispatch({ type: 'getMenu' });
      history.listen(({ pathname, query }) => {
        dispatch({
          type: 'pathChange',
          payload: { pathname, query },
        });
      });
    },
  },
  effects: {
    * getUser({}, { call, put }) {
      const { data } = yield call(userInfo);
      if (!data.errorCode) {
        yield put({
          type: 'passUserCheck',
          payload: { ...data },
        });
        if (location.pathname.match(/\/logon/)) {
          yield put(routerRedux.push(`${appUrlPrefix}/app`));
        }
      } else if (!location.pathname.match(/\/logon/)) {
        const from = encodeURIComponent(location.href);
        window.location = `${
          location.origin
        }${appUrlPrefix}/logon?from=${from}`;
      }
    },
    * getEnv({}, { call, put }) {
      const { data } = yield call(envInfo);
      if (data) {
        yield put({
          type: 'setEnv',
          payload: { env: data },
        });
      }
    },

    * getMenu({}, { call, put }) {
      const { data } = yield call(getMenu);

      const list = data ? data.data : [];
      const sidebarJson = getOpenedMenu(list);

      yield put({ type: 'setMenu', payload: { sidebarJson } });
    },

    * logout({ payload }, { call, put }) {
      const { data, error } = yield call(logout);
      if (error) {
        yield put({
          type: 'putLogoutError',
          payload: error.message,
        });
      } else {
        yield put({ type: 'clearUser' });
        const from = encodeURIComponent(location.href);
        window.location = `${
          location.origin
        }${appUrlPrefix}/logon?from=${from}`;
      }
    },
    * modifyPassword({ payload }, { call, put }) {
      console.log('passwordForm', payload);
      const { param } = payload;
      const { data, error, msg } = yield call(editPassword, param);
      if (data && data !== null) {
        yield put({
          type: 'closeDetailModal',
        });
        yield put({
          type: 'logout',
        });
      } else {
        message.error(msg);
      }
    },
  },
  reducers: {
    setMenu(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    pathChange(state, action) {
      const crumbDataList = parseCrumb(action.payload);
      const key = action.payload.pathname.split(appUrlPrefix)[1];
      return {
        ...state,
        crumbList: crumbDataList,
        sidebar: {
          ...state.sidebar,
          current: key,
          // openKeys: ['/reportAndWarn']
        },
      };
    },
    showPasswordModal(state, action) {
      return {
        ...state,
        showPasswordModal: true,
      };
    },
    closeDetailModal(state, action) {
      return {
        ...state,
        showPasswordModal: false,
      };
    },
    passUserCheck(state, action) {
      return {
        ...state,
        ...action.payload,
        login: true,
      };
    },

    setEnv(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },

    clearUser(state, action) {
      return {
        ...state,
        userId: '',
        userName: '',
        login: false,
        roleId: null,
      };
    },

    putLogoutError(state, action) {
      notification.error({
        message: '错误提示',
        description: action.payload,
        duration: 0,
        placement: 'topLeft',
      });

      return { ...state };
    },
  },
};
