import { parse } from 'qs';
import { notification } from 'antd';
import { routerRedux } from 'dva/router';

import { login } from 'src/services/frame';
import { queryURL } from 'src/utils/tools';

export default {
  namespace: 'logon',
  state: {
    loginLoading: false,
  },

  effects: {
    * checkRedirect({}, { put, select }) {
      const isLogin = yield select(state => state.frame.login);

      if (isLogin) {
        yield put(routerRedux.push('/searchcloud/app'));
      }
    },

    * login({ payload }, { put, call }) {
      yield put({ type: 'showLoginLoading' });
      const { data, error } = yield call(login, payload);
      yield put({ type: 'hideLoginLoading' });
      if (!error) {
        const from = queryURL('from');
        if (from) {
          window.location = from;
        } else {
          yield put({
            type: 'frame/getUser',
          });
          yield put(routerRedux.push('/searchcloud/app'));
        }
      }
    },
  },
  reducers: {
    showLoginLoading(state) {
      return {
        ...state,
        loginLoading: true,
      };
    },
    hideLoginLoading(state) {
      return {
        ...state,
        loginLoading: false,
      };
    },
  },
};
