import { list as datasourceList, scheduleList as list, scheduleDetail as detail, importData, importIncrementData, incList, interrupt, pause, resume, getLogUrl } from '../services/datasource';
import { stateCreate, effectsCreate, reducers, dataHandle } from 'src/models/modelCreator';
import { systemConstants } from 'src/utils/constants';
import { basicInfo as indexDetail } from '../services/index';
import { detail as appDetail } from '../services/app';

export default {
  namespace: 'control',
  state: {
    ...stateCreate(),
    moduleName: 'control',
    gradingType: '0',
    IncrementType: '0',
    relatedData: {
      appData: {},
      datasourceList: [],
      incList: [],
      indexData: {},
    },
    datasourceSelectStyle: {
      width: 200,
      visibility: 'hidden',
    },
    importedDatasource: null,
    importedIncrementDatasource: null,
    searchParams: {
      pageSize: systemConstants.pageSize,
      pageNo: 1,
      beginDate: systemConstants.range[0].format(systemConstants.dateFormat.backEnd),
      endDate: systemConstants.range[1].format(systemConstants.dateFormat.backEnd),
    },
  },
  subscriptions: {
    setup({ history, dispatch }) {
        // 监听 history 变化，当进入 `/` 时触发 `load` action
      return history.listen(({ pathname, query }) => {
        dispatch({
          type: 'paramsChange',
          payload: query,
        });

        if (pathname.match(/\/control$/)) {
          dispatch({
            type: 'relatedData',
            payload: { module: 'control' },
          });
        }
      });
    },
  },
  effects: {
    ...effectsCreate({ list, detail }, dataHandle),

    * doImport({ payload }, { call, put, select }) {
      const { data, error } = yield call(importData, payload);
      if (data) {
        if (data.success === true) {
          yield put({
            type: 'list',
            payload: { module: 'control' },
          });
        } else {
          yield put({
            type: 'putGlobalError',
            payload: {
              fieldErrors: data.reason || '失败了',
            },
          });
        }
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },

    * doIncrementImport({ payload }, { call, put, select }) {
      const { data, error } = yield call(importIncrementData, payload);
      if (data) {
        if (data.success === true) {
          yield put({
            type: 'list',
            payload: { module: 'control' },
          });
        } else {
          yield put({
            type: 'putGlobalError',
            payload: {
              fieldErrors: data.reason || '失败了',
            },
          });
        }
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },

    * relatedData({ payload }, { call, put, select }) {
      const { projectId, indexId } = yield select(state => state[payload.module].searchParams);

      const appDetailReq = call(appDetail, projectId);
      const indexDetailReq = call(indexDetail, indexId);
      const datasourceListReq = call(datasourceList, { indexId, projectId });
      const incListReq = yield call(incList, indexId);
      const appResp = yield appDetailReq;
      const indexResp = yield indexDetailReq;
      const datasourceListResp = yield datasourceListReq;
      console.log(incListReq, 'incListReq');
      const frame = yield select(state => state.frame);
      const { data, error } = {
        data: { appData: appResp.data, indexData: indexResp.data, incList: incListReq.data, datasourceList: datasourceListResp.data, frame },
        error: appResp.error || indexResp.error || datasourceListResp.error,
      };
      yield dataHandle.relatedDataHandle({ data, error }, { put });
    },

    * doInterrupt({ payload }, { call, put, select }) {
      const { data, error } = yield call(interrupt, payload);
				      if (data.success) {
								      yield put({
												      type: 'putGlobalInfo',
												      payload: {
																      message: data.reason,
												      },
								      });
				      } else {
								      yield put({
												      type: 'putGlobalError',
												      payload: {
																      message: data.reason,
												      },
								      });
				      }
    },

    * doPause({ payload }, { call, put, select }) {
      const { data, error } = yield call(pause, payload);
      if (data.success) {
        yield put({
          type: 'putGlobalInfo',
          payload: {
            message: data.reason,
          },
        });
      } else {
        yield put({
          type: 'putGlobalError',
          payload: {
            message: data.reason,
          },
        });
      }
    },

    * doResume({ payload }, { call, put, select }) {
      const { data, error } = yield call(resume, payload);
      if (data.success) {
        yield put({
          type: 'putGlobalInfo',
          payload: {
            message: data.reason,
          },
        });
      } else {
        yield put({
          type: 'putGlobalError',
          payload: {
            message: data.reason,
          },
        });
      }
    },

    * doLog({ payload }, { call, put, select }) {
      const { data, error } = yield call(getLogUrl, payload);
      if (data) {
        const logUrl = data;
        window.open(logUrl);
      } else {
		        yield put({
          type: 'putGlobalError',
				        payload: {
          message: error,
				        },
		        });
      }
    },
  },
  reducers: {
    ...reducers,
    gradingTypeChange(state, action) {
      const datasourceSelectStyle = { ...state.datasourceSelectStyle };
      datasourceSelectStyle.visibility = action.payload === '0' ? 'hidden' : 'visible';

      return {
        ...state,
        datasourceSelectStyle,
        gradingType: action.payload,
      };
    },
    IncrementTypeChange(state, action) {
      return {
        ...state,
        IncrementType: action.payload,
      };
    },
    datasourceChange(state, action) {
      return {
        ...state,
        importedDatasource: action.payload,
      };
    },

    onIncrementDatasourceChange(state, action) {
      return {
        ...state,
        importedIncrementDatasource: action.payload,
      };
    },

    relatedDataSuccess(state, action) {
      const { appData, indexData, datasourceList } = action.payload;
      return {
        ...state,
        relatedData: { ...action.payload },
      };
    },

    removeStatus(state, action) {
      return {
        ...state,
        searchParams: {
          ...state.searchParams,
          status: '',
        },
      };
    },
  },
};
