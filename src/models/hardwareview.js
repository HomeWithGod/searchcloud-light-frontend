import { list, detail, update, create, remove, clusterList, chartList, warningConfigList, deleteWarnCfg, saveDetail } from '../services/hardwareview';
import { users } from 'src/services/frame';
import { stateCreate, effectsCreate, reducers, dataHandle } from 'src/models/modelCreator';
import { systemConstants, appUrlPrefix } from 'src/utils/constants';

const moduleName = 'hardwareview';

export default {
  namespace: 'hardwareview',
  state: {
    ...stateCreate(),
    moduleName: 'hardwareview',
    entityDescription: '预警',
    searchParams: {
      selectedWarnStatus: '-1',
      selectedCluster: '-1',
    },
    entity: {},
    pagination: {
      pageNo: 1,
      pageSize: 10,
    },
    currentTab: '',
    selections: {
      warnStatus: {
        ...systemConstants.warnStatus,
      },
      cluster: {},
    },
    relatedData: {

      warn: {
        statisticsList: [],
        chart: {
          dataSet: [],
        },
      },
      default: {
        statisticsList: [],
        chart: {
          dataSet: [],
        },
      },
    },
    radioDisabled: false,

  },
  effects: {
    ...effectsCreate({ list, detail, update, create, remove, warningConfigList }, dataHandle),

    * list({ payload }, { call, put, select }) {
      yield put({ type: 'updateRadioDisabled', payload: true });
      const searchParams = yield select(state => state[payload.module].searchParams);
      const { pageNo, pageSize } = yield select(state => state[payload.module].pagination);
      const { data, error } = yield call(list, { ...searchParams, pageNo, pageSize });
      const chartData = yield call(chartList, searchParams);
      yield dataHandle.relatedDataHandle({ data, error }, { put });
      yield put({ type: 'updateChartData', payload: chartData.data });
    },

    * clusterList({ payload }, { call, put, select }) {
      let { data, error } = yield call(clusterList);
      data = { cluster: data };
      yield dataHandle.selectionHandle({ data, error }, { put });
    },

    * deleteWarnCfg({ payload }, { call, put, select }) {
      const { data } = yield call(deleteWarnCfg, payload);
      if (data) {
        if (data.success) {
          yield put({ type: 'deleteSuccess' });
          yield put({ type: 'warningConfigList', payload: { module: moduleName } });
        } else {
          yield put({
            type: 'putGlobalError',
            payload: {
              message: data.reason,
            },
          });
        }
      } else {
        yield put({
          type: 'putGlobalError',
          payload: {
            message: '未知错误',
          },
        });
      }
    },
    * saveDetail({ payload }, { call, put, select }) {
      const { data } = yield call(saveDetail, payload.param);
      if (data) {
        if (data.success) {
          yield put({ type: 'closeDetailModal' });
          yield put({ type: 'warningConfigList', payload: { module: moduleName } });
        } else {
          yield put({
            type: 'putGlobalError',
            payload: {
              message: data.reason,
            },
          });
        }
      } else {
        yield put({
          type: 'putGlobalError',
          payload: {
            message: '未知错误',
          },
        });
      }
    },
  },
  reducers: {
    ...reducers,
    tabChange(state, action) {
      return {
        ...state,
        currentTab: action.payload,
      };
    },
    clearSearchParams(state, action) {
      return {
        ...state,
        searchParams: {},
      };
    },
    changePage(state, action) {
      return {
        ...state,
        pagination: {
          ...state.pagination,
          ...action.payload,
        },
      };
    },
    relatedDataSuccess(state, action) {
      const currentTab = state.currentTab || 'default';
      return {
        ...state,
        relatedData: {
          ...state.relatedData,
          [currentTab]: {
            ...action.payload.datas,
          },
        },
        pagination: {
          ...state.pagination,
          total: action.payload.total,
        },
      };
    },

    updateSelection(state, action) {
      return {
        ...state,
        selections: {
          ...state.selections,
          ...action.payload,
        },
      };
    },

    updateChartData(state, action) {
      const currentTab = state.currentTab || 'default';
      return {
        ...state,
        relatedData: {
          ...state.relatedData,
          [currentTab]: {
            ...state.relatedData[currentTab],
            chart: action.payload,
          },
        },
        radioDisabled: false,
      };
    },
    updateRadioDisabled(state, action) {
      return {
        ...state,
        radioDisabled: action.payload,
      };
    },
  },

  updateTrigger(state, action) {
    return {
      ...state,
      trigger: action.payload,
    };
  },
    // subscriptions: {
    //   setup({ dispatch, history }) {
    //     history.listen(({ pathname, query }) => {
    //       if (!/\/hardwareview$/.test(pathname)) {
    //         dispatch({
    //           type: 'clearSearchParams',
    //           payload: { pathname, query },
    //         });
    //       }
    //     });
    //   },
    // },
};
