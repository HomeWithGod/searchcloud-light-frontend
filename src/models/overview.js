import { list, detail, update, create, remove } from '../services/overview';
import { users } from 'src/services/frame';
import { stateCreate, effectsCreate, reducers, dataHandle } from 'src/models/modelCreator';

export default {
  namespace: 'overview',
  state: {
    ...stateCreate(),
    moduleName: 'overview',
    searchParams: {},
    relatedData: {
      generalList: [
        {
          indexNum: 24,
          projectNum: 1023,
          requestNum: 2342,
          averageResponseTime: 12,
          indexSuccessRate: 0.98,
          indexConsumedTime: 1232,
          solrHealthRate: 0.923,
          harddiskUsageRate: 0.47,
          cpuUsageRate: 0.89,
          apiSuccessRate: 0.64,
          fourXXTimes: 239,
          fiveXXTimes: 122,
          key: 'k-e-y',
        },
      ],
      topCallNumList: [
        {
          index: '索引123',
          project: '项目234',
          callNum: 2234,
          averageResponseTime: 12,
            // successRate: 0.94,
          key: 'k-e-y',
        },
      ],
      topAverageResponseTimeList: [
        {
          index: '索引123',
          project: '项目234',
          callNum: 2234,
          averageResponseTime: 12,
            // successRate: 0.94,
          num_0_100: 12312,
          num_100_1000: 3242,
          num_1000_3000: 632,
          num_3000_: 129,
          key: 'k-e-y',
        },
      ],
      topIndexLoseRateList: [
        {
          index: '索引123',
          project: '项目234',
          callNum: 2234,
          loseNum: 872,
          failedRate: (872 / 2234).toFixed(2),
          time: '2018-10-23 13:22:34',
          key: 'k-e-y',
        },
      ],
      topIndexConsumedTimeList: [
        {
          index: '索引123',
          project: '项目234',
          callNum: 2234,
          successNum: 1872,
          failedRate: (1872 / 2234).toFixed(2),
          indexNum: 123232,
          consumedTime: 234,
          key: 'k-e-y',
        },
      ],
    },

  },
  effects: {
    ...effectsCreate({ list, detail, update, create, remove }, dataHandle),

    * list({ payload }, { call, put, select }) {
      const searchParams = yield select(state => state[payload.module].searchParams);
      const { data, error } = yield call(list, searchParams);
      yield dataHandle.relatedDataHandle({ data, error }, { put });
    },
  },
  reducers: {
    ...reducers,
    relatedDataSuccess(state, action) {
      return {
        ...state,
        relatedData: {
          ...state.relatedData,
          ...action.payload,
        },
      };
    },
  },
};
