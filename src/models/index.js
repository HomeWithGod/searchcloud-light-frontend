import {
  list, detail, update, create, queryData, basicInfo, remove,
  metaTypes, copyPath, updateIndex, enable, clusterInfo, hot, suggest,
} from '../services/index';
import { detail as appDetail } from '../services/app';
import { stateCreate, effectsCreate, reducers, dataHandle } from 'src/models/modelCreator';
import { systemConstants } from 'src/utils/constants';
import { transform, tools } from 'src/utils';
import { message } from 'antd';
import _ from 'underscore';

export default {
  namespace: 'index',
  state: {
    ...stateCreate(),
    moduleName: 'index',
    entityDescription: '索引',
    relatedData: {
      appData: {},
      metaTypes: [],
      engineTypes: [],
      copyPath: {
        suggestion: 'suggestion',
        default_search_field: 'default_search_field',
      },
    },
    showQueryModal: false,
    showSuggestModal: false,
    showHotModal: false,
    hotWords: [],
    suggests: [],
    indexName: '',
    dataQueryType: 'sql',
    dataQueryResult: {
      generalInfo: '',
      columns: [],
      list: [],
    },
    entity: {
      basicInfo: {
        id: null,
      },
      fields: [],
    },
  },
  effects: {
    ...effectsCreate({ list, detail, update, create, remove }, dataHandle),

    * enableItem({ payload }, { call, put }) {
      const { data, error } = yield call(enable, payload.id);
      if (data) {
          // TODO: 修改成局部更新，不刷新list
        yield put({
          type: 'list',
          payload: { module: 'index' },
        });
      } else if (error && error.message) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.message,
          },
        });
      }
    },

    * updateIndex({ payload }, { call, put }) {
      const { data, error } = yield call(updateIndex, payload);

      if (!error) {
        yield put({
          type: 'closeDetailModal',
        });
        yield put({
          type: 'list',
          payload: { module: 'index' },
        });
      } else if (error.fieldErrors) {
        const text = [];
        _.each(
            error.fieldErrors,
            (value, key) => {
              text.push(`${key}${value}`);
            },
          );
        message.error(text.join(';'), 5);
      } else {
        message.error(error.toString(), 5);
      }
    },

    * readBasic({ payload }, { call, put, select }) {
      const { data, error } = yield call(basicInfo, payload);
      yield dataHandle.detailHandle({ data: { basicInfo: data }, error }, { put });
    },

    * readHot({ payload }, { call, put, select }) {
      const data = yield call(hot, payload);
      console.log(data, 'data');
      if (data) {
        yield put({
          type: 'setHotwords',
          payload: data,
        });
      }
    },
    * getSuggest({ payload }, { call, put, select }) {
      const { data } = yield call(suggest, payload);
      console.log(data, 'data');
      if (data) {
        yield put({
          type: 'setSuggest',
          payload: data,
        });
      }
    },

    * relatedData({ payload }, { call, put, select }) {
      const { projectId } = yield select(state => state[payload.module].searchParams);

      const appDetailReq = call(appDetail, projectId);
      const metaTypesReq = call(metaTypes);
      const clusterInfoReq = call(clusterInfo);


      const appResp = yield appDetailReq;
      const metaTypesResp = yield metaTypesReq;
      const clusterInfoResp = yield clusterInfoReq;

      const { data, error } = {
        data: { appData: appResp.data, metaTypes: metaTypesResp.data, engineTypes: clusterInfoResp.data },
        error: appResp.error || metaTypes.error || clusterInfo.error,
      };
      yield dataHandle.relatedDataHandle({ data, error }, { put });
    },

    * startShowQuery({ payload }, { call, put, select }) {
      const { data, error } = yield call(basicInfo, payload);
      yield dataHandle.detailHandle({ data: { basicInfo: data }, error }, { put });

      yield put({
        type: 'showQuery',
        payload,
      });
    },

    * handleQuerySubmit({ payload }, { call, put, select }) {
      const dataQueryType = yield select(state => state[payload.module].dataQueryType);
      const indexName = yield select(state => state[payload.module].entity.basicInfo.indexName);
      const searchParams = { ...payload[dataQueryType], type: dataQueryType, indexName };
      const { data, error } = yield call(queryData, searchParams);
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: data,
        });
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },
  },
  subscriptions: {
    setup({ history, dispatch }) {
        // 监听 history 变化，当进入 `/` 时触发 `load` action
      return history.listen(({ pathname, query }) => {
        dispatch({
          type: 'paramsChange',
          payload: query,
        });

        if (pathname.match(/\/index$/)) {
          dispatch({
            type: 'list',
            payload: { module: 'index' },
          });

          dispatch({
            type: 'relatedData',
            payload: { module: 'index' },
          });
        }
      });
    },
  },
  reducers: {
    ...reducers,
    relatedDataSuccess(state, action) {
      const { appData, metaTypes, engineTypes } = action.payload;
      return {
        ...state,
        relatedData: {
          ...state.relatedData,
          appData,
          metaTypes,
          engineTypes,
        },
      };
    },

    loadDetailSuccess(state, action) {
      const newEntity = { basicInfo: action.payload.basicInfo };
      if (action.payload.fields) {
        newEntity.fields = transform.addKey(action.payload.fields);
      }
      return {
        ...state,
        entity: {
          ...state.entity,
          ...newEntity,
        },
      };
    },

    setHotwords(state, action) {
      return {
        ...state,
        hotWords: action.payload.data,
      };
    },

    setSuggest(state, action) {
      return {
        ...state,
        suggests: action.payload.textList,
      };
    },

    showQueryModal(state, action) {
      return {
        ...state,
        showQueryModal: true,
      };
    },

    showSuggestModal(state, action) {
      return {
        ...state,
        showSuggestModal: true,
        indexName: action.payload,
      };
    },
    showHotModal(state, action) {
      return {
        ...state,
        showHotModal: true,
      };
    },

    closeQueryModal(state, action) {
      return {
        ...state,
        showQueryModal: false,
      };
    },
    closeSuggestModal(state, action) {
      return {
        ...state,
        showSuggestModal: false,
      };
    },
    closeHotModal(state, action) {
      return {
        ...state,
        showHotModal: false,
      };
    },
    onDataQueryTypeChange(state, action) {
      return {
        ...state,
        dataQueryType: action.payload,
      };
    },

    querySuccess(state, action) {
      let { resultColumns, result, totalFound, resultSize, time } = action.payload;

      resultColumns = resultColumns || [];
      result = result || [];

      const columns = resultColumns.map(
          (item, index) => ({
            title: item,
            dataIndex: item,
            width: 150,
          }),
        );

      const list = result.map(
          (item, index) => ({
            ...item,
            key: index + new Date().getTime(),
          }),
        );

      const generalInfo = `查到数据共${totalFound}条，返回${resultSize}条，耗时${time}ms`;

      return {
        ...state,
        dataQueryResult: {
          list,
          columns,
          generalInfo,
          result,
        },
      };
    },

    closeDetailModal(state) {
      return {
        ...state,
        entity: {
          basicInfo: {
            id: null,
          },
          fields: [],
        },
        showDetailModal: false,
      };
    },
    pasteData(state, action) {
      const payload = action.payload;
      payload.basicInfo.id = state.entity.basicInfo.id;
      return {
        ...state,
        entity: payload,
      };
    },
  },
};
