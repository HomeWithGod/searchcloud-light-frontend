/*
 * Created by Alex Zhang on 2019/1/11
 */
import { list, detail, update, create, remove } from '../services/app';
import {
  userList,
  editPassword,
  editUser,
  deleteUser,
  userInfo,
  addUser,
} from '../services/userManager';
import { users } from 'src/services/frame';
import {
  stateCreate,
  effectsCreate,
  reducers,
  dataHandle,
} from 'src/models/modelCreator';

const localDataHandle = {
  ...dataHandle,
  * usersHandle({ data, error }, { put }) {
    if (data) {
      yield put({
        type: 'loadUsersSuccess',
        payload: { users: data.datas },
      });
    } else if (error && error.fieldErrors) {
      yield put({
        type: 'putError',
        payload: {
          fieldErrors: error.fieldErrors,
        },
      });
    }
  },
};

export default {
  namespace: 'userManager',
  state: {
    ...stateCreate(),
    moduleName: 'userManager',
    entityDescription: '用户管理',
    searchParams: {
      pageNo: 1,
      total: 0,
    },
    entity: {},
    list: [],
    pageNo: 1,
    pageSize: 10,
    pageTotal: 0,
  },
  effects: {
    ...effectsCreate({ list, detail, update, create, remove }, localDataHandle),

    * list({ payload }, { call, put, select }) {
      const searchParams = yield select(
        state => state[payload.module].searchParams,
      );
      console.log(searchParams);
      const { data, error } = yield call(userList, searchParams);
      console.log(data);
      if (data) {
        yield put({
          type: 'listSuccess',
          payload: data,
        });
      } else if (error && error.fieldErrors) {
        yield put({
          type: 'putGlobalError',
          payload: {
            fieldErrors: error.fieldErrors,
          },
        });
      }
    },
    * deleteItem({ payload }, { call, put, select }) {
      const { id } = payload;
      console.log(id, 'id');
      const { data, error } = yield call(deleteUser, id);
      console.log(data);
      if (data) {
        yield put({
          type: 'list',
          payload: { module: 'userManager' },
        });
      }
    },

    * read({ payload }, { call, put, select }) {
      const { data, error } = yield call(userInfo, payload);
      console.log(data);
      yield put({
        type: 'getDetailSuccess',
        payload: data,
      });
    },

    * saveDetail({ payload }, { call, put, select }) {
      console.log(payload);
      const modalType = yield select(state => state[payload.module].modalType);
      const {
        param: { id },
      } = payload;
      const { param, module } = payload;
      if (modalType === 'create') {
        const { data, error } = yield call(addUser, param);
        if (data) {
          yield put({
            type: 'list',
            payload: { module },
          });
          yield put({
            type: 'closeDetailModal',
          });
        }
      } else {
        const { status, mobile, email, password, roleId, username } = param;
        const newParam = { status, mobile, email, password, username };

        const { data, error } = yield call(editUser, id, newParam);
        if (data) {
          yield put({
            type: 'list',
            payload: { module },
          });
          yield put({
            type: 'closeDetailModal',
          });
        }
      }
    },
  },
  reducers: {
    ...reducers,
    getDetailSuccess(state, action) {
      return { ...state, entity: action.payload.data };
    },
    listSuccess(state, action) {
      let datas = action.payload.data.list || [];
      const { searchParams } = state;
      searchParams.total = action.payload.data.total;
      searchParams.pageNo = action.payload.data.pageNo;
      console.log(action.payload, searchParams, 'list');
      datas = datas.map((item, index) => ({
        ...item,
        key: index + new Date().getTime(),
        antOrderNumber: index + 1,
      }));
      return {
        ...state,
        list: datas,
        searchParams,
      };
    },
  },
};
