import React, { PropTypes } from 'react';
import { Modal, Button } from 'antd';

class AppInfoCard extends React.Component {
  render() {
    const data = this.props.data;
    const style = { marginRight: '20px' };
    return (
      <div>
        <span style={style}>Appid：{data.id}</span>
        <span style={style}>编码：{data.projectCode}</span>
        <span>负责人：{data.masterUserName}</span>
      </div>
    );
  }
}

AppInfoCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default AppInfoCard;
