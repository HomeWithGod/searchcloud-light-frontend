import React, { PropTypes } from 'react';
import { Modal, Button } from 'antd';
import { systemConstants } from 'src/utils/constants';

class IndexInfoCard extends React.Component {
  render() {
    const data = this.props.data;
    const style = { marginRight: '20px' };
    return (
      <div>
        <span style={style}>索引分片数：{data.shardCount}</span>
        <span style={style}>每分片副本数：{data.replicationFactor}</span>
        <span style={style}>当前版本：{data.configVersion}</span>
      </div>
    );
  }
}

IndexInfoCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default IndexInfoCard;
