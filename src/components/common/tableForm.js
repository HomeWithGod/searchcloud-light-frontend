import React, { PropTypes } from 'react';
import { Button, Table } from 'antd';
import EditableCell from './editableCell';

const data = {
  1: 'key',
  2: 'all',
};
class TabelForm extends React.Component {
  constructor(props) {
    super(props);
    this.columns = [
      {
        title: 'age',
        dataIndex: 'age',
        render: (text, record, index) => (
          <EditableCell
            value={text}
            onChange={this.onCellChange(index, 'age')}
          />
        ),
      },
      {
        title: 'name',
        dataIndex: 'name',
        render: (text, record, index) => (
          <EditableCell
            cellType="Select"
            dataSource={data}
            value={1}
            onChange={this.onCellChange(index, 'name')}
          />
        ),
      },
      {
        title: 'names',
        dataIndex: 'names',
        render: (text, record, index) => (
          <EditableCell
            cellType="MultipleSelect"
            dataSource={data}
            value={1}
            onChange={this.onCellChange(index, 'names')}
          />
        ),
      },
      {
        title: 'index',
        dataIndex: 'index',
        render: (text, record, index) => (
          <EditableCell
            cellType="Checkbox"
            value
            onChange={this.onCellChange(index, 'index')}
          />
        ),
      },
    ];
  }

  state = {
    dataSource: this.props.dataSource,
    selectedRowKeys: [],
    value: '1',
  }

  onCellChange = (index, key) => (value) => {
    const dataSource = [...this.state.dataSource];
    dataSource[index][key] = value;
    this.setState({ dataSource });
    this.onChange();
  }

  onSelectChange = (selectedRowKeys) => {
    this.setState({ selectedRowKeys });
  }

  onChange = () => {
    this.props.onChange(this.state.value);
  }

  render() {
    const rowSelection = {
      selectedRowKeys: this.state.selectedRowKeys,
      onChange: this.onSelectChange,
      type: 'radio',

      selections: {
        text: '主键',
      },
    };

    return (
      <div>
        <Table
          columns={this.columns}
          dataSource={this.state.dataSource}
          rowSelection={rowSelection}
          pagination={false}
        />
      </div>
    );
  }
}

TabelForm.propTypes = {};

export default TabelForm;
