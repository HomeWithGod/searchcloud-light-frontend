/*
 * Created by Alex Zhang on 2019/1/14
 */
import React, { PropTypes } from 'react';
import { Button, Form, Select, Input } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import FormModal from 'components/decorator/formModal';
import { systemConstants } from 'src/utils/constants';

const Option = Select.Option;
const FormItem = Form.Item;

class PasswordForm extends React.Component {
  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 14 },
    };

    // debugger;
    return (
      <Form>
        <FormItem {...formItemLayout} label="原密码" required>
          {getFieldDecorator('prePassword', {})(<Input placeholder="" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="新密码" required>
          {getFieldDecorator('newPassword', {})(<Input placeholder="" type="password" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="确认密码" required>
          {getFieldDecorator('confirmPassword', {})(<Input placeholder="" type="password" />)}
        </FormItem>
      </Form>
    );
  }
}
export default Form.create({})(FormModal(PasswordForm));
