import React, { PropTypes } from 'react';
import { Table } from 'antd';
import { systemConstants } from 'src/utils/constants';

export default class List extends React.Component {

  static propTypes = {
    columns: PropTypes.array,
    dataSource: PropTypes.array,
    pagination: PropTypes.object,
    loading: PropTypes.bool,
    bordered: PropTypes.bool,
    onChange: PropTypes.func,
  };

  static defaultProps = {
    columns: [],
    dataSource: [],
    pagination: {},
    loading: false,
    bordered: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      expandedRowKeys: [],
    };
  }

  onPageChange(page) {
    this.props.onChange({
      beginDate: page.beginDate,
      pageNo: page.current,
      pageSize: page.pageSize,
      endDate: page.endDate,
    });
  }

  // 点击展开的时候，闭合其他行
  onExpand(expanded, record) {
    this.setState({
      expandedRowKeys: expanded ? [record.key] : [],
    });
  }

  render() {
    const paginationType = {
      showSizeChanger: true,
      pageSizeOptions: systemConstants.pageSizeOption,
    };

    const { columns, dataSource, pagination, loading, expandedRowRender, rowKey, bordered } = this.props;
    return (
      <Table
        size="middle"
        columns={columns}
        dataSource={dataSource}
        pagination={{ ...paginationType, ...pagination }}
        loading={loading}
        onChange={::this.onPageChange}
        expandedRowRender={expandedRowRender}
        bordered={bordered}
        expandedRowKeys={this.state.expandedRowKeys}
        onExpand={::this.onExpand}
      />
    );
  }
}
