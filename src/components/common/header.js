import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Menu, Icon } from 'antd';
import './header.less';
import PasswordForm from './passwordForm';
import DetailForm from '../../views/userManager/detailForm';

function Header({
  logoutPage,
  frame,
  showPasswordModal,
  cancelSubmit,
  doSubmit,
}) {
  const header = frame.header;
  return (
    <div className="layout-header">
      <Link to={'/searchcloud/app'}>
        <div className="logo">搜索云平台</div>
      </Link>
      {frame.userName && (
        <div className="user-info">
          <i className="user-pic" />
          <span className="iconfont icon-user" />
          <div className="user-name">{frame.userName}</div>
          <a
            href="javascript:;"
            className="user-logout"
            onClick={showPasswordModal}
          >
            修改密码
          </a>
          <a href="javascript:;" className="user-logout" onClick={logoutPage}>
            退出
          </a>
        </div>
      )}
      <PasswordForm
        visible={frame.showPasswordModal}
        title="修改密码"
        onCancel={cancelSubmit}
        fieldErrors={frame.fieldErrors}
        onSubmit={doSubmit}
      />
    </div>
  );
}

Header.propTypes = {
  logoutPage: PropTypes.func.isRequired,
};

export default Header;
