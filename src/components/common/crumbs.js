import React, { PropTypes } from 'react';
import { Breadcrumb } from 'antd';
import { Link } from 'dva/router';
import './crumbs.less';

function Crumbs({ crumbList }) {
  const breadcrumbItems = crumbList.map((item, index) => {
    if (index === (crumbList.length - 1)) {
      return (<Breadcrumb.Item key={item.key}>{ item.name }</Breadcrumb.Item>);
    }
    return (<Breadcrumb.Item key={item.key}><Link to={item.path}>{ item.name }</Link></Breadcrumb.Item>);
  });

  return (
    <Breadcrumb separator=">">
      { breadcrumbItems }
    </Breadcrumb>
  );
}

Crumbs.propTypes = {
  crumbList: PropTypes.array.isRequired,
};

export default Crumbs;
