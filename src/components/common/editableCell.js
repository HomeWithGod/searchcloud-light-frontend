import React, { PropTypes } from 'react';
import { Icon, Input, Select, Checkbox } from 'antd';
import { selectOptions } from 'src/utils/transform';

class EditableCell extends React.Component {
  state = {
    value: this.props.value,
    editable: false,
    cellType: this.props.cellType || 'Input',
  }

  handleTargetChange = (type) => {
    const valueType = type || 'value';
    return (e) => {
      const value = e.target[valueType];
      this.handleValueChange(value);
    };
  }

  handleValueChange = (value) => {
    this.setState({ value });
    if (this.props.onChange) {
      this.props.onChange(value);
    }
  }

  multipleSelectChange = (value) => {
    this.handleValueChange(value.join(','));
  }

  render() {
    const { value, editable } = this.state;

    let cell;

    if (this.state.cellType === 'Input') {
      cell = (
        <div className="editable-cell-input-wrapper">
          <Input
            value={value}
            onChange={this.handleTargetChange()}
          />
        </div>
      );
    } else if (this.state.cellType === 'Select') {
      cell = (
        <Select
          defaultValue={value}
          style={{ width: this.props.width || '100px' }}
          dropdownStyle={{ overflow: 'scroll' }}
          onChange={this.handleValueChange}
        >
          { selectOptions(this.props.dataSource) }
        </Select>
      );
    } else if (this.state.cellType === 'Checkbox') {
      cell = (
        <Checkbox
          onChange={this.handleTargetChange('checked')}
          checked={this.state.value}
        />
      );
    } else if (this.state.cellType === 'textAreaType') {
      cell = (
        <Input
          type="textarea" className="ant-input-textarea"
          value={this.state.value}
          autosize
          style={{ width: this.props.width || '300px' }}
          onChange={this.handleTargetChange()}
        />
      );
    } else if (this.state.cellType === 'MultipleSelect') {
      cell = (
        <Select
          style={{ width: this.props.width || '300px' }}
          onChange={this.multipleSelectChange}
          mode="multiple"
          defaultValue={value}
        >
          { selectOptions(this.props.dataSource) }
        </Select>
      );
    }


    return (
      <div className="editable-cell">
        { cell }
      </div>
    );
  }
}

export default EditableCell;
