import React, { Component } from 'react';
import { Modal, Button } from 'antd';
import _ from 'underscore';

function getInnerValue(key, fields) {
  const splitKeys = key.split('.');
  return splitKeys.reduce(
    (value, key) => value[key],
    fields,
  );
}

const FormModal = (Wrapper) => {
  class WrapperComponent extends Component {
    constructor(props) {
      super(props);
      this.state = {
        key: +new Date(),
      };
    }

    componentWillReceiveProps(nextProps) {
      // // 对比两次的error，如果不相同，则处理
      if (!_.isEqual(this.props.fieldErrors, nextProps.fieldErrors)) {
        this.handleError(nextProps.fieldErrors, nextProps.form.getFieldsValue());
      }

      if (nextProps.visible !== this.props.visible) {
        this.setState({
          key: +new Date(),
        });
      }
    }

    doSubmit = () => {
      this.props.form.validateFields(
        { force: true },
        (err, values) => {
          if (!err) {
            console.log(JSON.stringify(values));
            this.props.onSubmit(values);
          }
        },
      );
    }

    handleSubmit = (e) => {
      e.preventDefault();
      this.clearError();
      this.doSubmit();
    }

    handleCancel = (e) => {
      // this.clearError();
      this.props.onCancel();
    }

    componentDidMount = () => {
      if (this.props.autoSubmit) {
        this.doSubmit();
      }
    }

    clearError() {
      const errors = this.props.fieldErrors;
      const fields = this.props.form.getFieldsValue();
      if (Object.keys(errors).length) {
        const fieldsReset = _.mapObject(errors,
          (item, key) => {
            const value = getInnerValue(key, fields);
            return { value };
          },
        );

        this.props.form.setFields(fieldsReset);
      }
    }

    handleError(errors, fields) {
      const fieldsReset = _.mapObject(errors,
        (item, key) => {
          const value = getInnerValue(key, fields);
          return {
            value,
            errors: [item],
          };
        },
      );

      this.props.form.setFields(fieldsReset);
    }

    render() {
      const formItemLayout = {
        labelCol: { span: 6 },
        wrapperCol: { span: 14 },
      };
      return (
        <Modal
          key={this.state.key}
          visible={this.props.visible}
          title={this.props.title}
          onCancel={this.handleCancel}
          footer={false}
          wrapClassName="vertical-center-modal form-modal"
          width={this.props.width || 1000}
          height={this.props.height}
          maskClosable={false}
        >
          <Wrapper
            {...this.props}
            formItemLayout={formItemLayout}
            onSubmit={this.handleSubmit}
          />
          {
            (this.props.modalType !== 'read')
            &&
            <div className="modal-bottom-bar clearfix" style={{ textAlign: 'center' }}>
              <div className="btn-wrap cancel-btn" style={{ display: 'inline-block', float: 'none' }}>
                <Button onClick={this.handleCancel} >取消</Button>
              </div>
              <div className="btn-wrap" style={{ display: 'inline-block', float: 'none' }}>
                <Button onClick={this.handleSubmit} >提交</Button>
              </div>
            </div>
          }
        </Modal>
      );
    }
  }

  return WrapperComponent;
};

export default FormModal;
