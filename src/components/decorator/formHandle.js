import React, { Component } from 'react';
import _ from 'underscore';

const FormHandle = (Wrapper) => {
  class WrapperComponent extends Component {

    componentWillMount() {
      if (!_.isEmpty(this.props.fieldErrors)) {
        this.handleError(this.props.fieldErrors);
      }
    }

    componentWillReceiveProps(nextProps) {
      if (!_.isEqual(this.props.fieldErrors, nextProps.fieldErrors)) {
        this.handleError(nextProps.fieldErrors, nextProps.form.getFieldsValue());
      }
    }

    doSubmit = () => {
      this.props.form.validateFields(
        { force: true },
        (err, values) => {
          if (!err) {
            this.props.onSubmit(values);
          }
        },
      );
    }

    handleSubmit = (e) => {
      e.preventDefault();
      this.doSubmit();
    }

    componentDidMount = () => {
      if (this.props.autoSubmit) {
        this.doSubmit();
      }
    }

    handleError(errors, fields) {
      const fieldErrors = _.mapObject(
        errors,
        (item, key) => ({
          errors: [item],
        }),
      );

      if (fields) {
        const fieldsReset = _.mapObject(fields,
          (item, key) => {
            const errors = fieldErrors[key] ? fieldErrors[key].errors : false;
            if (errors) {
              return {
                value: item,
                errors,
              };
            }
            return {
              value: item,
            };
          },
        );
        this.props.form.setFields(fieldsReset);
      } else {
        this.props.form.setFields(fieldErrors);
      }
    }

    render() {
      return (<Wrapper
        {...this.props}
        handleSubmit={this.handleSubmit}
      />);
    }
  }

  return WrapperComponent;
};

export default FormHandle;
