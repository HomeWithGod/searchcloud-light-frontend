import { Form } from 'antd';

const FormCreate = Form.create({
  onFieldsChange(props, fields) {
    props.dispatch({
      type: `${props.type}/toggleDownload`,
      payload: false,
    });
  },
});

export default FormCreate;
