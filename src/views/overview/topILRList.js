// top 10 Index Lose Rate
import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class TopIndexLoseRateList extends Component {

  get topNum() {
    return 10;
  }

  get columns() {
    return [
      {
        title: '索引',
        dataIndex: 'index',
        key: 'index',
        width: 70,
      },
      {
        title: '项目',
        dataIndex: 'project',
        key: 'project',
        width: 70,
      },
      {
        title: '调用次数',
        dataIndex: 'callNum',
        key: 'callNum',
        width: 70,
      },
      {
        title: '失败次数',
        dataIndex: 'loseNum',
        key: 'loseNum',
        width: 70,
      },
      {
        title: '失败率',
        dataIndex: 'failedRate',
        key: 'failedRate',
        width: 70,
      },
      // {
      //   title: '时间',
      //   dataIndex: 'time',
      //   key: 'time',
      //   width: 70,
      // }
    ];
  }

  render() {
    const { data } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={false} title={() => <h4 style={{ textAlign: 'center' }}>索引失败率top10</h4>} />
    );
  }

}
