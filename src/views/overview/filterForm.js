import React, { PropTypes } from 'react';
import { Button, Form, DatePicker } from 'antd';
import FormHandle from 'components/decorator/formHandle';
import { systemConstants } from 'src/utils/constants';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;

class FilterForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.props.handleSubmit} layout="inline">
        <FormItem
          label="日期"
        >
          {
            getFieldDecorator('range', {
              initialValue: systemConstants.range,
            })(
              <RangePicker
                format={systemConstants.dateFormat.frontEnd}
              />,
            )
          }
        </FormItem>
        <Button type="primary" size={'default'} htmlType="submit" style={{ marginRight: '12px' }}>查询</Button>
      </Form>
    );
  }
}
export default Form.create()(FormHandle(FilterForm));
