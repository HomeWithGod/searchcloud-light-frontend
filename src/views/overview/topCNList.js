// top 10 Call Num List
import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class TopCallNumList extends Component {

  get topNum() {
    return 10;
  }

  get columns() {
    return [
      {
        title: '索引',
        dataIndex: 'index',
        key: 'index',
        width: 70,
      },
      {
        title: '项目',
        dataIndex: 'project',
        key: 'project',
        width: 70,
      },
      {
        title: '调用量',
        dataIndex: 'callNum',
        key: 'callNum',
        width: 70,
      },
      {
        title: '平响',
        dataIndex: 'averageResponseTime',
        key: 'averageResponseTime',
        width: 70,
      },
      // {
      //   title: '成功率',
      //   dataIndex: 'successRate',
      //   key: 'successRate',
      //   width: 70,
      // }
    ];
  }

  render() {
    const { data } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={false} title={() => <h4 style={{ textAlign: 'center' }}>调用量top10</h4>} />
    );
  }

}
