// top 10 Index Consumed Time
import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class TopIndexCosumedTimeList extends Component {

  get topNum() {
    return 10;
  }

  get columns() {
    return [
      {
        title: '索引',
        dataIndex: 'index',
        key: 'index',
        width: 70,
      },
      {
        title: '项目',
        dataIndex: 'project',
        key: 'project',
        width: 70,
      },
      {
        title: '调用次数',
        dataIndex: 'callNum',
        key: 'callNum',
        width: 70,
      },
      {
        title: '成功次数',
        dataIndex: 'successNum',
        key: 'successNum',
        width: 70,
      },
      {
        title: '索引量',
        dataIndex: 'indexNum',
        key: 'indexNum',
        width: 70,
      },
      {
        title: '耗时',
        dataIndex: 'consumedTime',
        key: 'consumedTime',
        width: 70,
      },
    ];
  }

  render() {
    const { data } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={false} title={() => <h4 style={{ textAlign: 'center' }}>索引耗时top10</h4>} />
    );
  }

}
