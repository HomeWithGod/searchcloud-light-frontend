import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class GeneralList extends Component {

  get columns() {
    return [
      {
        title: '索引量',
        dataIndex: 'indexNum',
        key: 'indexNum',
        width: 70,
      },
      {
        title: '项目量',
        dataIndex: 'projectNum',
        key: 'projectNum',
        width: 70,
      },
     /* {
        title: '请求量',
        dataIndex: 'requestNum',
        key: 'requestNum',
        width: 70,
      },
      {
        title: '平响',
        dataIndex: 'averageResponseTime',
        key: 'averageResponseTime',
        width: 70,
      },
      {
        title: 'APi成功率',
        dataIndex: 'apiSuccessRate',
        key: 'apiSuccessRate',
        width: 70,
      },
      {
        title: '4XX次数',
        dataIndex: 'fourXXTimes',
        key: 'fourXXTimes',
        width: 70,
      },
      {
        title: '5XX次数',
        dataIndex: 'fiveXXTimes',
        key: 'fiveXXTimes',
        width: 70,
      }, */
      {
        title: '索引成功率',
        dataIndex: 'indexSuccessRate',
        key: 'indexSuccessRate',
        width: 70,
      },
      {
        title: '索引耗时',
        dataIndex: 'indexConsumedTime',
        key: 'indexConsumedTime',
        width: 70,
      },
      {
        title: 'solr健康率',
        dataIndex: 'solrHealthRate',
        key: 'solrHealthRate',
        width: 180,
      },
      {
        title: '硬盘使用率',
        dataIndex: 'harddiskUsageRate',
        key: 'harddiskUsageRate',
        width: 180,
      },
      // {
      //   title: 'cpu使用率',
      //   dataIndex: 'cpuUsageRate',
      //   key: 'cpuUsageRate',
      //   width: 70,
      // },
    ];
  }

  render() {
    const { data } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={false} />
    );
  }

}
