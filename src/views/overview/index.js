import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { Row, Col } from 'antd';
import FilterForm from './filterForm';
import GeneralList from './generalList';
import TopCNList from './topCNList';
import TopARTList from './topARTList';
import TopICTList from './topICTList';
import TopILRList from './topILRList';
import { fieldsToParams } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';
import ListCURDCreator from '../creator/list';

class Main extends Component {

  constructor(props) {
    super(props);
    this.dispatcher = ListCURDCreator({
      moduleName: this.moduleName,
    }, props.dispatch);
  }

  get moduleName() {
    return 'overview';
  }

  search = (fields) => {
    const { range } = fieldsToParams(fields);
    const params = {};

    if (range && range.length && range[0] && range[1]) {
      params.beginDate = range[0].startOf('day').format(systemConstants.dateFormat.backEnd);
      params.endDate = range[1].endOf('day').format(systemConstants.dateFormat.backEnd);
    }

    this.dispatcher.filteredList({ ...params });
  }

  render() {
    const { searchParams, fieldErrors, relatedData } = this.props[this.moduleName];

    const generalListProps = {
      data: relatedData.generalList,
    };

/*    const topCNListProps = {
      data: relatedData.topCallNumList
    };

    const topARTListProps = {
      data: relatedData.topAverageResponseTimeList
    }; */

    const topILRListProps = {
      data: relatedData.topIndexLoseRateList,
    };

    const topICTListProps = {
      data: relatedData.topIndexConsumedTimeList,
    };

    return (
      <div>
        <div style={{ marginBottom: 20 }}>
          <FilterForm
            {...searchParams}
            onSubmit={this.search}
            fieldErrors={fieldErrors}
            autoSubmit
          />
        </div>
        <div style={{ marginBottom: 50 }}>
          <GeneralList {...generalListProps} />
        </div>
        {/* <div style={{marginBottom: 50}}>
          <TopCNList {...topCNListProps}/>
        </div>
        <div style={{marginBottom: 50}}>
          <TopARTList {...topARTListProps}/>
        </div> */}
        <div style={{ marginBottom: 50 }}>
          <TopILRList {...topILRListProps} />
        </div>
        <div style={{ marginBottom: 50 }}>
          <TopICTList {...topICTListProps} />
        </div>
        {/* <div style={{marginBottom: 50}}>
          <Row gutter={16}>
            <Col span={8}>
              <TopCNList {...topCNListProps}/>
            </Col>
            <Col span={16}>
              <TopARTList {...topARTListProps}/>
            </Col>
          </Row>
        </div>
        <div style={{marginBottom: 50}}>
          <Row gutter={16}>
            <Col span={12}>
              <TopILRList {...topILRListProps}/>
            </Col>
            <Col span={12}>
              <TopICTList {...topICTListProps}/>
            </Col>
          </Row>
        </div> */}
      </div>
    );
  }

}

export default connect(({ overview }) => ({ overview }))(Main);
