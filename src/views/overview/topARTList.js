// top 10 Average Response Time
import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class TopAverageResponseTimeList extends Component {

  get topNum() {
    return 10;
  }

  get columns() {
    return [
      {
        title: '索引',
        dataIndex: 'index',
        key: 'index',
        width: 70,
      },
      {
        title: '项目',
        dataIndex: 'project',
        key: 'project',
        width: 70,
      },
      {
        title: '调用量',
        dataIndex: 'callNum',
        key: 'callNum',
        width: 70,
      },
      {
        title: '平响',
        dataIndex: 'averageResponseTime',
        key: 'averageResponseTime',
        width: 70,
      },
      // {
      //   title: '成功率',
      //   dataIndex: 'successRate',
      //   key: 'successRate',
      //   width: 70,
      // },
      {
        title: '(0,100]',
        dataIndex: 'num_0_100',
        key: 'num_0_100',
        width: 70,
      },
      {
        title: '(100,1000]',
        dataIndex: 'num_100_1000',
        key: 'num_100_1000',
        width: 70,
      },
      {
        title: '(1000,3000]',
        dataIndex: 'num_1000_3000',
        key: 'num_1000_3000',
        width: 70,
      },
      {
        title: '(3000,+∞)',
        dataIndex: 'num_3000_',
        key: 'num_3000_',
        width: 70,
      },
    ];
  }

  render() {
    const { data } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={false} title={() => <h4 style={{ textAlign: 'center' }}>平响top10</h4>} />
    );
  }

}
