import React, { PropTypes } from 'react';
import { Button, Form, Select, Input, Row, Col } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import FormModal from 'components/decorator/formModal';
import { systemConstants } from 'src/utils/constants';

const Option = Select.Option;
const FormItem = Form.Item;

class DetailForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;

    // debugger;
    const entity = { ...this.props.entity };
    const { selections } = this.props;

    const formItemLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 14 },
    };

    return (
      <Form>
        <FormItem
          {...formItemLayout}
          label="硬盘使用率"
        >
          {
            getFieldDecorator('entity.hardDiskUsageRate', {
              initialValue: entity ? entity.hardDiskUsageRate : '',
              rules: [{ pattern: /^\d*$/, message: '只能输入整数' }],
            })(
              <Input
                addonBefore=">"
                addonAfter="%"
                placeholder="请输入整数"
              />,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="solr节点健康率"
        >
          {
            getFieldDecorator('entity.nodeHealthRate', {
              initialValue: entity ? entity.nodeHealthRate : '',
              rules: [{ pattern: /^\d*$/, message: '只能输入整数' }],
            })(
              <Input
                addonBefore="<"
                addonAfter="%"
                placeholder="请输入整数"
              />,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="预警接收人"
          required
        >
          {
            getFieldDecorator('entity.mailTo', {
              initialValue: entity && entity.mailTo || '',
              rules: [{ message: '必填', required: true }],
            })(
              <Input placeholder="多个邮箱请用逗号分隔" />,
            )
          }
        </FormItem>
      </Form>
    );
  }
}
export default Form.create({})(FormModal(DetailForm));
