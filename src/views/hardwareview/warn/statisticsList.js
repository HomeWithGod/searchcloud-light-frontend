import React, { Component, PropTypes } from 'react';
import { Table, Row, Col, Button, Popconfirm } from 'antd';
import DetailForm from './detailForm';

export default class StatisticsList extends Component {

  get columns() {
    return [
      {
        title: '预警条件',
        dataIndex: 'warnCondition',
        key: 'warnCondition',
        width: 70,
      },
      {
        title: '预警人',
        dataIndex: 'warnMan',
        key: 'warnMan',
        width: 70,
      },
      {
        title: '预警次数',
        dataIndex: 'warnTimes',
        key: 'warnTimes',
        width: 70,
      },
      {
        title: '最新预警时间',
        dataIndex: 'lastWarnTime',
        key: 'lastWarnTime',
        width: 70,
      },
      {
        title: '操作',
        key: 'action',
        width: 70,
        render: (text, item) => (
          <div className="list-action">
            <Popconfirm title="确定要删除?" onConfirm={() => this.props.onDeleteWarn(item.id)}>
              <a href="javascript:;" >删除</a>
            </Popconfirm>
          </div>
        ),
      },
    ];
  }

  onEdit = (e) => {
    this.props.dispatcher.toUpdate(e);
  }

  onCreate = () => {
    this.props.dispatcher.toCreate();
  }

  render() {
    const { data, dispatcher, selections, fieldErrors, showDetailModal, detailModalTitle, entity, paginationType, onPageChange } = this.props;

    return (
      <div>
        <div style={{ textAlign: 'right', marginBottom: '20px' }}>
          <Button type="primary" size={'default'} onClick={this.onCreate} >新建预警</Button>
        </div>
        <Table size="middle" columns={this.columns} dataSource={data} pagination={{ ...paginationType }} onChange={onPageChange} />
        <DetailForm
          visible={showDetailModal}
          title={detailModalTitle}
          selections={selections}
          entity={entity}
          fieldErrors={fieldErrors}
          onCancel={dispatcher.cancelSubmit}
          onSubmit={dispatcher.doSubmit}
        />
      </div>
    );
  }

}
