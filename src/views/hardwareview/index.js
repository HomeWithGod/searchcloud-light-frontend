import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { browserHistory } from 'react-router';
import { Radio, Row, Col } from 'antd';
import WarnFilterForm from './warn/filterForm';
import WarnStatisticsList from './warn/statisticsList';
import DefaultFilterForm from './default/filterForm';
import DefaultStatisticsList from './default/statisticsList';
import DefaultLeftChart from './default/leftChart';
import DefaultRightChart from './default/rightChart';
import { fieldsToParams } from 'src/utils/transform';
import { systemConstants, appUrlPrefix } from 'src/utils/constants';
import ListCURDCreator from '../creator/list';

const { Group: RadioGroup, Button: RadioButton } = Radio;

class Main extends Component {

  constructor(props) {
    super(props);
    this.dispatcher = ListCURDCreator({
      moduleName: this.moduleName,
    }, props.dispatch);
  }

  get moduleName() {
    return 'hardwareview';
  }

  componentDidMount() {
    this.props.dispatch({
      type: `${this.moduleName}/clusterList`,
    });
  }

  componentWillUnmount() {
    this.props.dispatch({
      type: `${this.moduleName}/tabChange`,
      payload: '',
    });
  }

  onTabChange = ({ target }) => {
    this.props.dispatch({
      type: `${this.moduleName}/updateTrigger`,
      payload: 'clickTabRadio',
    });
    this.props.dispatch({
      type: `${this.moduleName}/tabChange`,
      payload: target.value,
    });
    this.props.dispatch({
      type: `${this.moduleName}/changePage`,
      payload: {
        pageNo: 1,
      },
    });
  }

  onClusterSelect = (value, option) => {
    this.props.dispatch({
      type: `${this.moduleName}/paramsChange`,
      payload: {
        selectedCluster: value,
      },
    });
  }

  onPageChange = (page) => {
    this.props.dispatch({
      type: `${this.moduleName}/changePage`,
      payload: {
        pageNo: page.current,
        pageSize: page.pageSize,
      },
    });
    this.props.dispatch({
      type: `${this.moduleName}/list`,
      payload: {
        module: this.moduleName,
      },
    });
  }

  onDeleteWarn = (id) => {
    this.props.dispatch({
      type: `${this.moduleName}/deleteWarnCfg`,
      payload: id,
    });
  }

  search = (fields) => {
    const { date, range } = fieldsToParams(fields);
    // delete fields.range;
    const params = { ...fields };

    if (date) {
      params.date = date.startOf('day').format(systemConstants.dateFormat.frontEnd);
    } else {
      params.data = null;
    }

    if (range && range.length && range[0] && range[1]) {
      params.beginDate = range[0].startOf('day').format(systemConstants.dateFormat.frontEnd);
      params.endDate = range[1].endOf('day').format(systemConstants.dateFormat.frontEnd);
    } else {
      params.beginDate = null;
      params.endDate = null;
    }

    if (this.props[this.moduleName].currentTab === 'warn') {
      this.props.dispatch({
        type: `${this.moduleName}/paramsChange`,
        payload: { ...params },
      });

      this.props.dispatch({ type: `${this.moduleName}/warningConfigList`, payload: { module: this.moduleName } });
    } else {
      this.dispatcher.filteredList({ ...params });
    }
  }

  render() {
    const moduleObj = this.props[this.moduleName];
    const { searchParams, selections, fieldErrors, relatedData, currentTab, pagination, radioDisabled } = moduleObj;
    const paginationType = {
      showSizeChanger: false,
      pageSizeOptions: systemConstants.pageSizeOption,
      ...pagination,
      current: pagination.pageNo,
    };

    let TargetFilterForm,
      TargetLeftChart,
      TargetRightChart,
      TargetStatisticsList,
      targetStatisticsListProps,
      targetChartData;

    switch (currentTab) {
      case 'warn':
        TargetFilterForm = WarnFilterForm;
        TargetLeftChart = null;
        TargetRightChart = null;
        TargetStatisticsList = WarnStatisticsList;
        targetStatisticsListProps = { ...moduleObj, data: relatedData.warn.statisticsList, dispatcher: this.dispatcher, paginationType, onPageChange: this.onPageChange, onDeleteWarn: this.onDeleteWarn };
        targetChartData = relatedData.warn.chart;
        break;
      default:
        TargetFilterForm = DefaultFilterForm;
        TargetLeftChart = DefaultLeftChart;
        TargetRightChart = DefaultRightChart;
        TargetStatisticsList = DefaultStatisticsList;
        targetStatisticsListProps = { data: relatedData.default.statisticsList, dispatcher: this.dispatcher, paginationType, onPageChange: this.onPageChange };
        targetChartData = relatedData.default.chart;
    }

    return (
      <div>
        <div style={{ marginBottom: 20 }}>
          <RadioGroup value={currentTab} buttonStyle="solid" onChange={this.onTabChange}>
            <RadioButton value="warn" disabled={radioDisabled}>预警</RadioButton>
          </RadioGroup>
        </div>
        <div style={{ marginBottom: 50 }}>
          <TargetFilterForm
            {...searchParams}
            selections={selections}
            onClusterSelect={this.onClusterSelect}
            onSubmit={this.search}
            fieldErrors={fieldErrors}
            autoSubmit
          />
        </div>
        {
          TargetLeftChart && TargetRightChart &&
          <div style={{ marginBottom: 50 }}>
            <Row>
              <Col span={12}>
                <TargetLeftChart chartData={targetChartData} />
              </Col>
              <Col span={12}>
                <TargetRightChart chartData={targetChartData} />
              </Col>
            </Row>
          </div>
        }
        <div style={{ marginBottom: 50 }}>
          <TargetStatisticsList {...targetStatisticsListProps} />
        </div>
      </div>
    );
  }

}

export default connect(({ hardwareview }) => ({ hardwareview }))(Main);
