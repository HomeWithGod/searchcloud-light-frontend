// chart of index average consumed time
import React, { Component, PropTypes } from 'react';
import ReactEcharts from 'echarts-for-react';

export default class RightChart extends Component {

  get defaultOption() {
    return {
      title: {
        left: 0,
        show: false,
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: {
        type: 'category',
        name: '日期(精确到分钟)',
        nameLocation: 'center',
        nameGap: 40,
        axisTick: {
          alignWithLabel: true,
        },
        data: [],
      },
      yAxis: {
        name: '硬盘使用率',
        // nameLocation: 'center',
        // nameGap: 40
      },
      series: [
        {
          type: 'line',
          data: [],
        },
      ],
    };
  }

  render() {
    const dataSet = this.props.chartData.dataSet || {};
    const diskOccupancyRate = dataSet.diskOccupancyRate || { x: [], serieData: [] };
    const series = [];
    diskOccupancyRate.serieData.forEach((item) => {
      series.push({
        type: 'line',
        name: item.name,
        data: item.y,
      });
    });

    const defaultOption = this.defaultOption;

    return (
      <ReactEcharts
        notMerge
        option={{
          ...defaultOption,
          series,
          xAxis: {
            ...defaultOption.xAxis,
            data: diskOccupancyRate.x,
          },
        }}
      />
    );
  }

}
