import React, { PropTypes } from 'react';
import { Button, Form, DatePicker, Select, Input } from 'antd';
import FormHandle from 'components/decorator/formHandle';
import { selectOptions } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const Option = Select.Option;

class FilterForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      startHardDiskUsageRate,
      endHardDiskUsageRate,
      startNodeHealthRate,
      endNodeHealthRate,
      selectedCluster,
      selections,
      onClusterSelect,
    } = this.props;

    return (
      <Form onSubmit={this.props.handleSubmit} layout="inline">
        <FormItem
          label="日期"
        >
          {
            getFieldDecorator('date', {
              initialValue: systemConstants.range[1],
            })(
              <DatePicker
                format={systemConstants.dateFormat.frontEnd}
              />,
            )
          }
        </FormItem>
        <FormItem
          label="集群"
        >
          {
            getFieldDecorator('selectedCluster', {
              initialValue: selectedCluster || '-1',
            })(
              <Select style={{ width: '150px' }} onSelect={onClusterSelect}>
                <Option key="-1" value="-1">请选择</Option>
                { selectOptions(selections.cluster) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          label="节点健康率"
        >
          {
            getFieldDecorator('startNodeHealthRate', {
              initialValue: startNodeHealthRate,
            })(
              <Input placeholder="" style={{ display: 'inline', width: '50px' }} />,
            )
          }
        </FormItem>
        <span style={{ marginRight: '16px' }}>to</span>
        <FormItem>
          {
            getFieldDecorator('endNodeHealthRate', {
              initialValue: endNodeHealthRate,
            })(
              <Input placeholder="" style={{ display: 'inline', width: '50px' }} />,
            )
          }
        </FormItem>
        <FormItem
          label="硬盘使用率"
        >
          {
            getFieldDecorator('startHardDiskUsageRate', {
              initialValue: startHardDiskUsageRate,
            })(
              <Input placeholder="" style={{ display: 'inline', width: '50px' }} />,
            )
          }
        </FormItem>
        <span style={{ marginRight: '16px' }}>to</span>
        <FormItem>
          {
            getFieldDecorator('endHardDiskUsageRate', {
              initialValue: endHardDiskUsageRate,
            })(
              <Input placeholder="" style={{ display: 'inline', width: '50px' }} />,
            )
          }
        </FormItem>
        <Button type="primary" size={'default'} htmlType="submit" style={{ marginRight: '12px' }}>查询</Button>
      </Form>
    );
  }
}
export default Form.create()(FormHandle(FilterForm));
