// chart of index average consumed time
import React, { Component, PropTypes } from 'react';
import ReactEcharts from 'echarts-for-react';

export default class LeftChart extends Component {

  get defaultOption() {
    return {
      title: {
        left: 0,
        show: false,
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: {
        type: 'category',
        name: '日期(精确到分钟)',
        nameLocation: 'center',
        nameGap: 40,
        axisTick: {
          alignWithLabel: true,
        },
        data: [],
      },
      yAxis: {
        name: '节点健康率',
        // nameLocation: 'center',
        // nameGap: 40
      },
      series: [
        {
          type: 'line',
          data: [],
        },
      ],
    };
  }

  render() {
    const dataSet = this.props.chartData.dataSet || {};
    const sorlHealthRate = dataSet.sorlHealthRate || { x: [], serieData: [] };
    const series = [];
    sorlHealthRate.serieData.forEach((item) => {
      series.push({
        type: 'line',
        name: item.name,
        data: item.y,
      });
    });

    const defaultOption = this.defaultOption;

    return (
      <ReactEcharts
        notMerge
        option={{
          ...defaultOption,
          series,
          xAxis: {
            ...defaultOption.xAxis,
            data: sorlHealthRate.x,
          },
        }}
      />
    );
  }

}
