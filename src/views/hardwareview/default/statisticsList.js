import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class StatisticsList extends Component {

  get columns() {
    return [
      {
        title: '时间',
        dataIndex: 'dateTime',
        key: 'dateTime',
        width: 70,
      },
      {
        title: '集群名',
        dataIndex: 'clusterName',
        key: 'clusterName',
        width: 70,
      },
      {
        title: '节点数',
        dataIndex: 'nodeNum',
        key: 'nodeNum',
        width: 70,
      },
      {
        title: '健康节点数',
        dataIndex: 'healthyNodeNum',
        key: 'healthyNodeNum',
        width: 70,
      },
      {
        title: '节点健康率',
        dataIndex: 'nodeHealthRate',
        key: 'nodeHealthRate',
        width: 70,
      },
      {
        title: '硬盘使用率',
        dataIndex: 'hardDiskUsageRate',
        key: 'hardDiskUsageRate',
        width: 50,
      },
    ];
  }

  render() {
    const { data, paginationType, onPageChange } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={{ ...paginationType }} onChange={onPageChange} />
    );
  }

}
