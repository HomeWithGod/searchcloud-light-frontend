import { connect } from 'dva';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Table, Button, Card, Col, Row, Input, Popconfirm } from 'antd';
import _ from 'underscore';

import { fieldsToParams } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';
import AppInfoCard from 'src/components/biz/AppInfoCard';
import IndexInfoCard from 'src/components/biz/IndexInfoCard';

import Detail from './detailForm';
import BuildForm from './buildForm';
import ListCURDCreator from '../creator/list';

function List({ dispatch, datasource: moduleModel }) {
  const { moduleName } = moduleModel;
  const { list, toCreate, toDetail, toDelete, cancelSubmit, doSubmit } = ListCURDCreator(moduleModel, dispatch);

  function onBuildConfigChange(e) {
    dispatch({ type: `${moduleName}/changeBuildConfig`, payload: e.target.value });
  }

  function saveBuildConfig(fieldsValue) {
    const postParams = fieldsToParams(fieldsValue);
    dispatch({
      type: `${moduleName}/saveBuildConfig`,
      payload: {
        ...buildConfig,
        ...postParams,
      },
    });
  }

  function changeIncImportType(e) {
    dispatch({ type: `${moduleName}/changeIncImportType`, payload: e.target.checked });
  }

  function incDataSourceRpcBufferStatus(e) {
    dispatch({ type: `${moduleName}/incDataSourceRpcBufferStatus`, payload: e.target.value });
  }

  function onOutputAPIDemo(params) {
    dispatch({
      type: `${moduleName}/outputAPIDemo`,
      payload: params,
    });
  }

  function onTriggerTest(params) {
    dispatch({
      type: `${moduleName}/triggerTest`,
      payload: params,
    });
  }

  function clearIncApiResults() {
    clearApiResults('inc');
  }

  function clearFullApiResults() {
    clearApiResults('full');
  }

  function clearApiResults(type) {
    dispatch({
      type: `${moduleName}/clearApiResults`,
      payload: type,
    });
  }

  function deleteDataSource(id) {
    dispatch({
      type: `${moduleName}/deleteItem`,
      payload: { id, module: moduleName },
    });
  }

  const { buildConfig, relatedData } = moduleModel;

  const { appData, indexData } = relatedData;

  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
      width: 70,
    },
    {
      title: '数据源名称',
      dataIndex: 'name',
      key: 'name',
      width: 70,
    },
    {
      title: '创建人',
      dataIndex: 'createByName',
      key: 'createByName',
      width: 70,
    },
    {
      title: '全量方式',
      dataIndex: 'fullDatasourceType',
      key: 'fullDatasourceType',
      width: 70,
      render: value => systemConstants.datasource.datasourceTypes[value],
    },
    {
      title: '增量方式',
      dataIndex: 'incDatasourceType',
      key: 'incrementaincDatasourceTypelType',
      width: 70,
      render: value => systemConstants.datasource.datasourceTypes[value],
    },
    {
      title: '操作',
      width: 50,
      key: 'action',
      render: (text, item) => (
        <div>
          <Row>
            <Col span={12}>
              <Popconfirm title="确定要删除?" onConfirm={deleteDataSource.bind(null, item.id)} id={`detailId_${item.id}`}>
                <a href="javascript:;" >删除</a>
              </Popconfirm>
            </Col>
            <Col span={12}><a href="javascript:;" onClick={toDetail} id={`detailId_${item.id}`}>查看</a></Col>
          </Row>
        </div>
      ),
    },
  ];

  const commonCardConfig = {
    bordered: false,
  };

  const appCardConfig = {
    ...commonCardConfig,
    title: <div><Row>
      <Col span="24">{appData.name}项目信息</Col>
    </Row></div>,
  };

  const indexCardConfig = {
    ...commonCardConfig,
    title: <div><Row>
      <Col span="24">{indexData.name}索引信息</Col>
    </Row></div>,
  };

  return (
    <div>
      <Row style={{ marginBottom: '20px' }}>
        <Col span="5">
          <Card {...appCardConfig}>
            <AppInfoCard data={appData} />
          </Card>
        </Col>
        <Col span="1" />
        <Col span="5">
          <Card {...indexCardConfig}>
            <IndexInfoCard data={indexData} />
          </Card>
        </Col>
        <Col span="1" />
        <Col span="11">
          <Card title={'定时构建时机设置'} {...commonCardConfig}>
            <BuildForm
              onSubmit={saveBuildConfig}
              entity={buildConfig}
            />
          </Card>
        </Col>
      </Row>
      <Button type="primary" size={'default'} onClick={toCreate}>新建</Button>
      <div className="query-result">
        <div>
          <Table size="middle" columns={columns} dataSource={moduleModel.list} pagination={false} />
        </div>
      </div>
      <Detail
        {...moduleModel}
        visible={moduleModel.showDetailModal}
        title={moduleModel.detailModalTitle}
        width={1200}
        onCancel={cancelSubmit}
        onSubmit={doSubmit}
        onIncDatasourceTypeChange={changeIncImportType}
        onIncDataSourceRpcBufferStatus={incDataSourceRpcBufferStatus}
        onOutputAPIDemo={onOutputAPIDemo}
        onTriggerTest={onTriggerTest}
        clearFullApiResults={clearFullApiResults}
        clearIncApiResults={clearIncApiResults}
      />
    </div>
  );
}

List.propTypes = {
  dispatch: PropTypes.func.isRequired,
  datasource: PropTypes.object.isRequired,
};

export default connect(({ datasource }) => ({ datasource }))(List);
