import React, { PropTypes } from 'react';
import { Button, Form, Input } from 'antd';
import FormHandle from 'components/decorator/formHandle';

const FormItem = Form.Item;

class BuildForm extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields(
      { force: true },
      (err, values) => {
        if (!err) {
          this.props.onSubmit(values);
        }
      },
    );
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayout = {
      labelCol: { span: 8 },
      wrapperCol: { span: 12 },
    };

    const { entity } = this.props;

    return (
      <Form layout="inline">
        <FormItem
          style={{ marginRight: '2px' }}
          {...formItemLayout}
          label="全量定时"
        >
          {
            getFieldDecorator('cron', {
              initialValue: entity ? entity.cron : '',
            })(
              <Input placeholder="" />,
            )
          }
        </FormItem>
        <FormItem
          style={{ marginRight: '2px' }}
          {...formItemLayout}
          label="增量定时"
        >
          {
                getFieldDecorator('incCron', {
                  initialValue: entity ? entity.incCron : '',
                })(
                  <Input placeholder="" />,
                )
            }
        </FormItem>
        <Button type="primary" size={'default'} style={{ marginRight: '2px' }} onClick={this.handleSubmit}>保存构建</Button>
      </Form>
    );
  }
}
export default Form.create()(FormHandle(BuildForm));
