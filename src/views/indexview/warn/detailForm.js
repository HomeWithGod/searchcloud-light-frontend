import React, { PropTypes } from 'react';
import { Button, Form, Select, Input, Row, Col } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import FormModal from 'components/decorator/formModal';
import { systemConstants } from 'src/utils/constants';

const Option = Select.Option;
const FormItem = Form.Item;

class DetailForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;

    // debugger;
    const entity = { ...this.props.entity };
    const { selections } = this.props;

    const formItemLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 14 },
    };

    return (
      <Form>
        <FormItem
          {...formItemLayout}
          label="项目"
          required
        >
          {
            getFieldDecorator('entity.selectedProject', {
              initialValue: entity.selectedProject,
              rules: [{ message: '必填', required: true }],
            })(
              <Select style={{ width: '150px' }}>
                <Option key="-1" value="-1">请选择</Option>
                { selectOptions(selections.project) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="索引"
        >
          {
            getFieldDecorator('entity.selectedIndex', {
              initialValue: entity.selectedIndex,
            })(
              <Select mode="multiple">
                {/* <Option key="-1" value="-1">请选择</Option> */}
                { selectOptions(selections.index) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="单条索引平均耗时"
        >
          {
            getFieldDecorator('entity.singleIndexAverageConsumedTime', {
              initialValue: entity ? entity.singleIndexAverageConsumedTime : '',
              rules: [{ pattern: /^\d*$/, message: '只能输入整数' }],
            })(
              <Input
                addonBefore=">"
                addonAfter="毫秒"
                placeholder=""
              />,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="索引耗时"
        >
          {
            getFieldDecorator('entity.indexConsumedTime', {
              initialValue: entity ? entity.indexConsumedTime : '',
              rules: [{ pattern: /^\d*$/, message: '只能输入整数' }],
            })(
              <Input
                addonBefore=">"
                addonAfter="小时"
                placeholder=""
              />,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="成功率"
        >
          {
            getFieldDecorator('entity.successRate', {
              initialValue: entity ? entity.successRate : '',
              rules: [{ pattern: /^\d*$/, message: '只能输入整数' }],
            })(
              <Input
                addonBefore="<"
                addonAfter="%"
                placeholder="请输入整数"
              />,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="预警接收人"
          required
        >
          {
            getFieldDecorator('entity.mailTo', {
              initialValue: entity && entity.mailTo || '',
              rules: [{ message: '必填', required: true }],
            })(
              <Input placeholder="多个邮箱请用逗号分隔" />,
            )
          }
        </FormItem>
      </Form>
    );
  }
}
export default Form.create({})(FormModal(DetailForm));
