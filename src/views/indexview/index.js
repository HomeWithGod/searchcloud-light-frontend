import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { browserHistory } from 'react-router';
import { Radio, Row, Col } from 'antd';
import ProjectFilterForm from './project/filterForm';
import ProjectStatisticsList from './project/statisticsList';
import ProjectLeftChart from './project/leftChart';
import ProjectRightChart from './project/rightChart';
import IndexFilterForm from './index/filterForm';
import IndexStatisticsList from './index/statisticsList';
import IndexLeftChart from './index/leftChart';
import IndexRightChart from './index/rightChart';
import WarnFilterForm from './warn/filterForm';
import WarnStatisticsList from './warn/statisticsList';
import DefaultFilterForm from './default/filterForm';
import DefaultStatisticsList from './default/statisticsList';
import DefaultLeftChart from './default/leftChart';
import DefaultRightChart from './default/rightChart';
import { fieldsToParams } from 'src/utils/transform';
import { systemConstants, appUrlPrefix } from 'src/utils/constants';
import ListCURDCreator from '../creator/list';

const { Group: RadioGroup, Button: RadioButton } = Radio;

class Main extends Component {

  constructor(props) {
    super(props);
    this.dispatcher = ListCURDCreator({
      moduleName: this.moduleName,
    }, props.dispatch);
  }

  // get moduleName() {
  //   return 'indexview';
  // }

  moduleName = 'indexview';

  componentDidMount() {
    this.props.dispatch({
      type: `${this.moduleName}/initSelections`,
    });
  }

  onProjectSelect = (value, option) => {
    this.props.dispatch({
      type: `${this.moduleName}/paramsChange`,
      payload: {
        selectedProject: value,
      },
    });

    // this.props.dispatch({
    //   type: `${this.moduleName}/updateSelection`,
    //   payload: {
    //     index: {}
    //   }
    // });

    this.props.dispatch({
      type: `${this.moduleName}/indexList`,
      payload: {
        projectId: value,
      },
    });
  }

  componentWillUnmount() {
    this.props.dispatch({
      type: `${this.moduleName}/tabChange`,
      payload: '',
    });
  }

  onIndexSelect = (value, option) => {
    this.props.dispatch({
      type: `${this.moduleName}/paramsChange`,
      payload: {
        selectedIndex: value,
      },
    });
  }

  onTabChange = ({ target }) => {
    this.props.dispatch({
      type: `${this.moduleName}/updateTrigger`,
      payload: 'clickTabRadio',
    });
    this.props.dispatch({
      type: `${this.moduleName}/tabChange`,
      payload: target.value,
    });
    this.props.dispatch({
      type: `${this.moduleName}/changePage`,
      payload: {
        pageNo: 1,
      },
    });
    // browserHistory.push(`${appUrlPrefix}/${this.moduleName}?tab=${target.value}`);
  }

  onFormSubmitButtonClick = () => {
    this.props.dispatch({
      type: `${this.moduleName}/updateTrigger`,
      payload: 'clickFormSubmitButton',
    });
    this.props.dispatch({
      type: `${this.moduleName}/changePage`,
      payload: {
        pageNo: 1,
      },
    });
  }

  onDeleteWarn = (id) => {
    this.props.dispatch({
      type: `${this.moduleName}/deleteWarnCfg`,
      payload: id,
    });
  }

  search = (fields) => {
    const { range } = fieldsToParams(fields);
    delete fields.range;
    const params = { ...fields };

    if (range && range.length && range[0] && range[1]) {
      params.beginDate = range[0].startOf('day').format(systemConstants.dateFormat.backEnd);
      params.endDate = range[1].endOf('day').format(systemConstants.dateFormat.backEnd);
    } else {
      params.beginDate = null;
      params.endDate = null;
    }

    if (this.props[this.moduleName].currentTab === 'warn') {
      this.props.dispatch({
        type: `${this.moduleName}/paramsChange`,
        payload: { ...params },
      });

      this.props.dispatch({ type: `${this.moduleName}/warningConfigList`, payload: { module: this.moduleName } });
    } else {
      this.dispatcher.filteredList({ ...params });
    }
  }

  onPageChange = (page) => {
    this.props.dispatch({
      type: `${this.moduleName}/changePage`,
      payload: {
        pageNo: page.current,
        pageSize: page.pageSize,
      },
    });
    this.props.dispatch({
      type: `${this.moduleName}/list`,
      payload: {
        module: this.moduleName,
      },
    });
  }

  render() {
    const moduleObj = this.props[this.moduleName];
    const { searchParams, selections, fieldErrors, relatedData, currentTab, pagination } = moduleObj;
    const paginationType = {
      showSizeChanger: false,
      pageSizeOptions: systemConstants.pageSizeOption,
      ...pagination,
      current: pagination.pageNo,
    };

    let TargetFilterForm,
      TargetLeftChart,
      TargetRightChart,
      TargetStatisticsList,
      targetStatisticsListProps,
      targetChartData;

    switch (currentTab) {
      case 'project':
        TargetFilterForm = ProjectFilterForm;
        TargetLeftChart = ProjectLeftChart;
        TargetRightChart = ProjectRightChart;
        TargetStatisticsList = ProjectStatisticsList;
        targetStatisticsListProps = { data: relatedData.project.statisticsList, dispatcher: this.dispatcher, paginationType, onPageChange: this.onPageChange };
        targetChartData = relatedData.project.chart;
        break;
      case 'index':
        TargetFilterForm = IndexFilterForm;
        TargetLeftChart = IndexLeftChart;
        TargetRightChart = IndexRightChart;
        TargetStatisticsList = IndexStatisticsList;
        targetStatisticsListProps = { data: relatedData.index.statisticsList, dispatcher: this.dispatcher, paginationType, onPageChange: this.onPageChange };
        targetChartData = relatedData.index.chart;
        break;
      case 'warn':
        TargetFilterForm = WarnFilterForm;
        TargetLeftChart = null;
        TargetRightChart = null;
        TargetStatisticsList = WarnStatisticsList;
        targetStatisticsListProps = { ...moduleObj, data: relatedData.warn.statisticsList, dispatcher: this.dispatcher, paginationType, onPageChange: this.onPageChange, onDeleteWarn: this.onDeleteWarn };
        targetChartData = relatedData.warn.chart;
        break;
      default:
        TargetFilterForm = DefaultFilterForm;
        TargetLeftChart = DefaultLeftChart;
        TargetRightChart = DefaultRightChart;
        TargetStatisticsList = DefaultStatisticsList;
        targetStatisticsListProps = { data: relatedData.default.statisticsList, dispatcher: this.dispatcher, paginationType, onPageChange: this.onPageChange };
        targetChartData = relatedData.default.chart;
    }

    return (
      <div>
        <div style={{ marginBottom: 20 }}>
          <RadioGroup value={currentTab} buttonStyle="solid" onChange={this.onTabChange}>
            <RadioButton value="project">项目</RadioButton>
            <RadioButton value="index">索引</RadioButton>
            <RadioButton value="warn">预警</RadioButton>
          </RadioGroup>
        </div>
        <div style={{ marginBottom: 50 }}>
          <TargetFilterForm
            {...searchParams}
            selections={selections}
            onProjectSelect={this.onProjectSelect}
            onIndexSelect={this.onIndexSelect}
            onSubmit={this.search}
            fieldErrors={fieldErrors}
            autoSubmit
            onSubmitButtonClick={this.onFormSubmitButtonClick}
          />
        </div>
        {
          TargetLeftChart && TargetRightChart &&
          <div style={{ marginBottom: 50 }}>
            <Row>
              <Col span={12}>
                <TargetLeftChart chartData={targetChartData} />
              </Col>
              <Col span={12}>
                <TargetRightChart chartData={targetChartData} />
              </Col>
            </Row>
          </div>
        }
        <div style={{ marginBottom: 50 }}>
          <TargetStatisticsList {...targetStatisticsListProps} />
        </div>
      </div>
    );
  }

}

export default connect(({ indexview }) => ({ indexview }))(Main);
