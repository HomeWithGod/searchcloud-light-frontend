import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class StatisticsList extends Component {

  get columns() {
    return [
      {
        title: '项目',
        dataIndex: 'project',
        key: 'project',
        width: 70,
      },
      {
        title: '索引数量',
        dataIndex: 'indexNum',
        key: 'indexNum',
        width: 70,
      },
      {
        title: '数据源数量',
        dataIndex: 'dataSourceNum',
        key: 'dataSourceNum',
        width: 70,
      },
      {
        title: '索引平均耗时',
        dataIndex: 'indexAverageConsumedTime',
        key: 'indexAverageConsumedTime',
        width: 70,
      },
      {
        title: '记录平均耗时',
        dataIndex: 'recordAverageConsumedTime',
        key: 'recordAverageConsumedTime',
        width: 70,
      },
      {
        title: '预期执行次数',
        dataIndex: 'expectedExecutionTimes',
        key: 'expectedExecutionTimes',
        width: 70,
      },
      {
        title: '定时执行次数',
        dataIndex: 'timedExecutionTimes',
        key: 'timedExecutionTimes',
        width: 70,
      },
      {
        title: '定时成功率',
        dataIndex: 'timedSuccessRate',
        key: 'timedSuccessRate',
        width: 70,
      },
      {
        title: '手动执行次数',
        dataIndex: 'manualExecutionTimes',
        key: 'manualExecutionTimes',
        width: 70,
      },
      {
        title: '手动成功率',
        dataIndex: 'manualSuccessRate',
        key: 'manualSuccessRate',
        width: 70,
      },
    ];
  }

  render() {
    const { data, paginationType, onPageChange } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={{ ...paginationType }} onChange={onPageChange} />
    );
  }

}
