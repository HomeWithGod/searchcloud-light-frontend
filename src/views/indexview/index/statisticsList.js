import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class StatisticsList extends Component {

  get columns() {
    return [
      {
        title: 'id',
        dataIndex: 'id',
        key: 'id',
        width: 70,
      },
      {
        title: '索引',
        dataIndex: 'index',
        key: 'index',
        width: 70,
      },
      {
        title: '项目',
        dataIndex: 'project',
        key: 'project',
        width: 70,
      },
      {
        title: '数据源数量',
        dataIndex: 'dataSourceNum',
        key: 'dataSourceNum',
        width: 70,
      },
      {
        title: '手动/定时',
        dataIndex: 'manualOrTimed',
        key: 'manualOrTimed',
        width: 70,
      },
      {
        title: '状态',
        dataIndex: 'status',
        key: 'status',
        width: 70,
      },
      {
        title: '索引总量',
        dataIndex: 'indexNum',
        key: 'indexNum',
        width: 70,
      },
      {
        title: '索引耗时',
        dataIndex: 'indexConsumedTime',
        key: 'indexConsumedTime',
        width: 70,
      },
      {
        title: '记录平均耗时',
        dataIndex: 'recordAverageConsumedTime',
        key: 'recordAverageConsumedTime',
        width: 70,
      },
    ];
  }

  render() {
    const { data, paginationType, onPageChange } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={{ ...paginationType }} onChange={onPageChange} />
    );
  }

}
