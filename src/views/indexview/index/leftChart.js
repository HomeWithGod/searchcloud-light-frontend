// chart of index average consumed time
import React, { Component, PropTypes } from 'react';
import ReactEcharts from 'echarts-for-react';

export default class LeftChart extends Component {

  get defaultOption() {
    return {
      title: {
        left: 0,
        show: false,
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: {
        type: 'category',
        name: '次数(id)',
        nameLocation: 'center',
        nameGap: 40,
        axisTick: {
          alignWithLabel: true,
        },
        data: [],
      },
      yAxis: {
        name: '耗时',
        // nameLocation: 'center',
        // nameGap: 40
      },
      series: [
        {
          type: 'line',
          data: [],
        },
      ],
    };
  }

  render() {
    const dataSet = this.props.chartData.dataSet || {};
    const indexCost = dataSet.indexCost || { x: [], serieData: [] };
    const series = [];
    indexCost.serieData.forEach((item) => {
      series.push({
        type: 'line',
        name: item.name,
        data: item.y,
      });
    });

    const defaultOption = this.defaultOption;

    return (
      <ReactEcharts
        notMerge
        option={{
          ...defaultOption,
          series,
          xAxis: {
            ...defaultOption.xAxis,
            data: indexCost.x,
          },
        }}
      />
    );
  }

}
