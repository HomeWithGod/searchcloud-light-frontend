/**
 * 欢迎页
 * 临时方案，无设计稿，先这么滴吧
 */
import React, { Component } from 'react';
import './index.less';

export default class Home extends Component {
  render() {
    return (
      <div className="searchcloud-home">
        欢迎使用搜索云平台
      </div>
    );
  }
}
