/*
 * Created by Alex Zhang on 2019/1/18
 */
import React from 'react';
import { Row, Modal, Input } from 'antd';

class Suggest extends React.Component {
  constructor() {
    super();
    this.state = {
      value: '',
    };
  }
  handleChange = (e) => {
    const value = e.target.value;
    this.setState({
      value,
    }, () => {
      const { suggest } = this.props;
      suggest(value);
    });
  }
  cancel =() => {
    this.setState({
      value: null,
    }, () => {
      this.props.onCancel();
    });
  }
  render() {
    const { suggests } = this.props;
    const { value } = this.state;
    return (
      <Modal
        visible={this.props.visible}
        title={this.props.title}
        onCancel={this.cancel}
        footer={false}
        wrapClassName="vertical-center-modal form-modal"
        width={this.props.width || 1000}
        height={this.props.height || 450}
        maskClosable={false}
      >
        <div>
          <Row>
            <Input placeholder="请输入" onChange={this.handleChange} value={value} />
          </Row>
          <Row>
            <ul>
              {
               suggests.map(item => (<li>{item}</li>))
               }
              {
                suggests && suggests.length === 0 && <div>暂无suggest</div>
              }
            </ul>
          </Row>
        </div>
      </Modal>
    );
  }
}
export default Suggest;
