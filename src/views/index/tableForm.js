import React, { PropTypes } from 'react';
import { Button, Table, message, Tooltip, Icon, Radio } from 'antd';
import CopyToClipboard from 'react-copy-to-clipboard';
import _ from 'underscore';
import EditableCell from 'src/components/common/editableCell';
import IndexImportModal from 'src/views/index/indexImportModal';

class TabelForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRowKeys: [],
      maxVersion: null,
      visible: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    const keys = this.getChangedKey(nextProps.value);
    const maxVersion = this.getChangedVersion(nextProps.value);
    this.setState({
      selectedRowKeys: keys,
	     maxVersion,
    });
  }

  componentDidMount() {
    document.getElementsByClassName('createForm')[0].getElementsByClassName('ant-table-thead')[0]
      .getElementsByClassName('ant-table-selection-column')[0].firstChild.innerHTML = '主键';
  }

  getChangedKey = (values) => {
    const primaryIndex = _.find(
      values,
      item => item.uniqueKey == true,
    );

    return primaryIndex ? [primaryIndex.key] : [];
  }

  getChangedVersion = (values) => {
    const Versionindex = _.find(
     values,
     item => item.versionKey == true,
    );

    return Versionindex ? Versionindex.key : null;
  }

  onCellChange = (index, key) => (val) => {
    const value = [...this.props.value];
    value[index][key] = val;

    this.onChange(value);
  }

  onSelectChange = (no, record) => {
    const key = record[0].key;
    const selectedKey = this.state.selectedRowKeys && this.state.selectedRowKeys[0];
    const value = this.props.value;
    _.each(
      value,
      (item) => {
        if (item.key === key) {
          item.uniqueKey = true;
        } else {
          item.uniqueKey = false;
        }
      },
    );
    this.setState({ selectedRowKeys: [key] });
    this.onChange(value);
  }

  onVersionChange = (index, key) => (val) => {
	  const value = [...this.props.value];
	  _.each(
		  value,
		  (item) => {
				  item.versionKey = false;
		    },
		  ),
	  value[index][key] = val.target.checked;
	  this.setState({ maxVersion: index });

	  this.onChange(value);
	 }

  onChange = (value) => {
    this.props.onChange(value);
  }

  toDelete = (record) => {
    const value = _.filter(
      this.props.value,
      item => item.key !== record.key,
    );
    this.onChange(value);
  }

  onDelete = (index) => {
    const dataSource = [...this.state.dataSource];
    dataSource.splice(index, 1);
    this.setState({ dataSource });
  };

  pasteData = (value) => {
    try {
      const entity = JSON.parse(value);
      if (!entity.basicInfo || !entity.fields) {
        message.error('粘贴的内容格式不正确', 3);
      }
      this.props.pasteData(entity);
    } catch (e) {
      message.error('粘贴的内容格式不正确', 3);
    }
  };

  exportSuccess = () => {
    message.success('已复制到剪切板', 3);
  }
  handleAdd = () => {
    const { value } = this.props;
    const newLine = {
      boost: 0,
      copyToField: '',
      defaultValue: '',
      fieldComment: 'string',
      fieldName: 'fieldName',
      fieldTypeId: 1,
      id: '', // 这个字段
      indexed: false,
      multiValued: false,
      required: false,
      stored: false,
      termVectors: false,
      uniqueKey: false,
	     versionKey: false,
      key: value.length,
    };
    value.push(newLine);
    this.onChange(value);
  };

  render() {
    const metaTypes = _.reduce(
      this.props.enums.metaTypes,
      (memo, item) => {
        memo[item.id] = item.fieldType;
        return memo;
      },
      {},
    );
    const entity = JSON.stringify({ basicInfo: this.props.basicInfo, fields: this.props.value });
    const columns = [
      {
        title: '字段名称',
        dataIndex: 'fieldName',
        width: 220,
        render: (value, record, index) => (
          <EditableCell
            value={value}
            onChange={this.onCellChange(index, 'fieldName')}
          />
        ),
      },
      {
        title: '字段类型',
        dataIndex: 'fieldTypeId',
        width: 70,
        render: (value, record, index) => (
          <EditableCell
            cellType="Select"
            dataSource={metaTypes}
            value={`${value || 1}`}
            onChange={this.onCellChange(index, 'fieldTypeId')}
          />
        ),
      },
      {
        title: 'versionKey',
        dataIndex: 'versionKey',
        width: 70,
        render: (value, record, index) => (
          <Radio
            checked={index === this.state.maxVersion}
            value={`${value || false}`}
            onChange={this.onVersionChange(index, 'versionKey')}
          />
        ),
      },
      {
        title: (<span>
            必填&nbsp;
            <Tooltip title="是否为必填项">
              <Icon type="question-circle-o" />
            </Tooltip>
        </span>),
        dataIndex: 'required',
        width: 50,
        render: (value, record, index) => (
          <EditableCell
            cellType="Checkbox"
            value={value}
            onChange={this.onCellChange(index, 'required')}
          />
        ),
      },
      {
        title:
          (<span>
            索引&nbsp;
            <Tooltip title="若作为搜索条件，必须启用">
              <Icon type="question-circle-o" />
            </Tooltip>
          </span>),
        dataIndex: 'indexed',
        width: 50,
        render: (value, record, index) => (
          <EditableCell
            cellType="Checkbox"
            value={value}
            onChange={this.onCellChange(index, 'indexed')}
          />
        ),
      },
      {
        title: (<span>
          存储原始值&nbsp;
            <Tooltip title="若作为select字段，必须启用">
              <Icon type="question-circle-o" />
            </Tooltip>
        </span>),
        dataIndex: 'stored',
        width: 70,
        render: (value, record, index) => (
          <EditableCell
            cellType="Checkbox"
            value={value}
            onChange={this.onCellChange(index, 'stored')}
          />
        ),
      },
      {
        title:
          (<span>
            多值&nbsp;
            <Tooltip title="多个值以数组形式保存，查询时任意元素满足条件即可命中">
              <Icon type="question-circle-o" />
            </Tooltip>
          </span>),
        dataIndex: 'multiValued',
        width: 70,
        render: (value, record, index) => (
          <EditableCell
            cellType="Checkbox"
            value={value}
            onChange={this.onCellChange(index, 'multiValued')}
          />
        ),
      },
      {
        title:
          (<span>
            默认值&nbsp;
            <Tooltip title="当数据导入时，若该字段为空时，需要填入的默认值">
              <Icon type="question-circle-o" />
            </Tooltip>
          </span>),
        dataIndex: 'defaultValue',
        width: 100,
        render: (value, record, index) => (
          <EditableCell
            value={value}
            onChange={this.onCellChange(index, 'defaultValue')}
          />
        ),
      },
      {
        title:
          (<span>
            复制到&nbsp;
            <Tooltip title="需要在多个字段查询时，可以将字段复制到默认字段，使用默认字段来搜索。">
              <Icon type="question-circle-o" />
            </Tooltip>
          </span>),
        dataIndex: 'copyToField',
        width: 100,
        render: (value, record, index) => {
          const val = value ? value.split(',') : [];
          return (
            <EditableCell
              cellType="MultipleSelect"
              dataSource={this.props.enums.copyPath}
              value={val}
              width={'200px'}
              onChange={this.onCellChange(index, 'copyToField')}
            />
          );
        },
      },
      {
        title: '注释',
        dataIndex: 'fieldComment',
        width: 100,
        render: (value, record, index) => (
          <EditableCell
            value={value}
            cellType={'textAreaType'}
            width={'200px'}
            onChange={this.onCellChange(index, 'fieldComment')}
          />
        ),
      },
      {
        title: '操作',
        width: 70,
        key: 'action',
        render: (text, record, index) => (
          <Button
            type="primary" onClick={
                (e) => {
                  this.toDelete(record);
                }
              }
          >删除</Button>),
      },
    ];

    const rowSelection = {
      selectedRowKeys: this.state.selectedRowKeys,
      onChange: this.onSelectChange,
      type: 'radio',
    };

    return (
      <div>
        <Table
          className="createForm"
          columns={columns}
          dataSource={this.props.value}
          rowSelection={rowSelection}
          pagination={false}
          rowKey="key"
        />
        <Button type="primary" className="ant-btn" onClick={this.handleAdd} style={{ marginTop: 15 }}>添加</Button>
        <CopyToClipboard text={entity} >
          <Button style={{ marginLeft: 10 }} onClick={this.exportSuccess}>导出</Button>
        </CopyToClipboard>
        {
          (!this.props.basicInfo || !this.props.basicInfo.id)
          &&
          <IndexImportModal
            onOk={this.pasteData}
          >
            <Button style={{ marginLeft: 10 }} >导入</Button>
          </IndexImportModal>
        }


      </div>
    );
  }
}


TabelForm.propTypes = {};

export default TabelForm;
