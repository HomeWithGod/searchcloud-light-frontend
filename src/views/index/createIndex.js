import { connect } from 'dva';
import React, { PropTypes } from 'react';
import CreateForm from './createForm';
import TableForm from 'components/common/tableForm';

function createIndex(props) {
  const { createIndex: moduleData, dispatch } = props;

  function submit(field) {
    dispatch({
      type: 'createIndex/updateIndex',
      payload: field,
    });
  }

  return (
    <div>
      <CreateForm
        onSubmit={submit}
        {...moduleData}
      />
    </div>
  );
}

createIndex.propTypes = {
  dispatch: PropTypes.func,
};

export default connect(({ createIndex }) => ({ createIndex }))(createIndex);
