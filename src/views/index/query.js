import React from 'react';
import { Button, Input, Card, Modal, Col, Row, Tabs, Form, Table } from 'antd';
import Highlight from 'react-highlight';

import { formatJson } from 'src/utils/transform';

const TabPane = Tabs.TabPane;
const FormItem = Form.Item;

class Query extends React.Component {

  state = {
    sql: { sql: '' },
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { sql } = this.state.sql;
    this.props.onQuerySubmit({ sql: { sql } });
  }

  onSqlChange = (e) => {
    this.setState({ sql: { sql: e.target.value } });
  }

  render() {
    const { appData, entity, dataQueryResult } = this.props;
    const cardConfig = {
      bordered: false,
    };

    const { list, columns, generalInfo, result } = dataQueryResult;

    const scroll = { y: 300, x: 150 * columns.length };

    return (
      <Modal
        visible={this.props.visible}
        title={this.props.title}
        onCancel={this.props.onCancel}
        footer={false}
        wrapClassName="vertical-center-modal form-modal"
        width={this.props.width || 1000}
        height={this.props.height}
        maskClosable={false}
      >
        <div>
          <Tabs defaultActiveKey="sql">
            <TabPane tab="按SQL" key="sql">
              <Input type="textarea" placeholder="输入sql语句" autosize={{ minRows: 2, maxRows: 10 }} onChange={this.onSqlChange} />
            </TabPane>
          </Tabs>
          <Button type="primary" size={'default'} onClick={this.handleSubmit}>查询</Button>
          <Row style={{ marginTop: '20px' }}>
            <Col span="24">
              <div>{generalInfo}</div>
              <div>
                {
                  list.length > 0
                  &&
                  <Tabs defaultActiveKey="raw">
                    <TabPane tab="RAW" key="raw">
                      <p className="code">{formatJson(result)}</p>
                    </TabPane>
                    <TabPane tab="TABLE" key="table">
                      <Table
                        className="query-table"
                        scroll={scroll} size="middle" columns={columns} dataSource={list} pagination={false}
                      />
                    </TabPane>
                  </Tabs>
                }
              </div>
            </Col>
          </Row>
        </div>
      </Modal>
    );
  }
}
export default Form.create()(Query);
