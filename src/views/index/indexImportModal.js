import React from 'react';
import { Modal, Input, Alert } from 'antd';

class IndexImportModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      json: null,
    };
  }

  onJsonChange = (e) => {
    if (e) e.stopPropagation();
    this.setState({
      json: e.target.value,
    });
  };

  showModelHandler = (e) => {
    if (e) e.stopPropagation();
    this.setState({
      visible: true,
    });
  };

  hideModelHandler = () => {
    this.setState({
      visible: false,
      json: null,
    });
  };


  okHandler = () => {
    const { onOk } = this.props;
    onOk(this.state.json);
    this.hideModelHandler();
  };

  render() {
    const { children } = this.props;

    return (
      <span>
        <span onClick={this.showModelHandler}>
          { children }
        </span>
        <Modal
          style={{ width: 700 }}
          title="索引配置导入"
          visible={this.state.visible}
          onOk={this.okHandler}
          onCancel={this.hideModelHandler}
        >
          <Alert message="点击确定后，请检查索引结构是否正确" />
          <Input
            type="textarea"
            value={this.state.json}
            onChange={this.onJsonChange}
            style={{ width: '100%', minHeight: 300 }}
          />
        </Modal>
      </span>
    );
  }
}

export default IndexImportModal;
