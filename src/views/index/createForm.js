import React, { PropTypes } from 'react';
import { Button, Form, Select, Input, Alert, Tooltip, Icon } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormModal from 'components/decorator/formModal';
import { systemConstants } from 'src/utils/constants';
import TableForm from './tableForm';
import _ from 'underscore';

const FormItem = Form.Item;

class CreateForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      seletedEngine: '',
    };
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.entity.basicInfo.engineType !== nextProps.entity.basicInfo.engineType) {
      this.setState({
        seletedEngine: nextProps.entity.basicInfo.engineType,
      });
    }
  }

  // 引擎类型变化，集群联动修改
  onEngineTypesChange = (value) => {
    this.setState({
      seletedEngine: value,
    });
    this.props.form.setFieldsValue({
      'basicInfo.clusterName': '',
    });
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { entity, relatedData } = this.props;
    const { basicInfo, fields } = entity;
    const { metaTypes, copyPath, engineTypes } = relatedData;

    const formItemLayout = {
      wrapperCol: { span: 24 },
    };
    return (
      <Form layout="inline" onSubmit={this.props.doSubmit}>
        <FormItem
          label="索引名称："
        >
          {
            getFieldDecorator(
              'basicInfo.indexName',
              {
                initialValue: basicInfo.indexName || '',
              })(
                <Input style={{ width: '150px' }} />,
            )
          }
        </FormItem>
        <FormItem
          label="分片数："
        >
          {
            getFieldDecorator(
              'basicInfo.shardCount',
              {
                initialValue: basicInfo.shardCount || 1,
              })(
                <Input style={{ width: '70px' }} />,
            )
          }
        </FormItem>
        <FormItem
          label={(
            <span>
              每分片副本数：
              <Tooltip title="指每个分片的总副本数，建议大于2来保持可用，否则副本节点宕机会导致数据不可用。">
                <Icon type="question-circle-o" />
              </Tooltip>
            </span>
          )}
        >
          {
            getFieldDecorator(
              'basicInfo.replicationFactor',
              {
                initialValue: basicInfo.replicationFactor || 1,
              })(
                <Input style={{ width: '70px' }} />,
            )
          }
        </FormItem>
        <FormItem
          label="选择引擎"
        >
          {
            getFieldDecorator('basicInfo.engineType', {
              initialValue: basicInfo.engineType ? (`${basicInfo.engineType}`) : (`${engineTypes[0].engineType}`),
            })(
              <Select style={{ width: '150px' }} onChange={this.onEngineTypesChange} >
                { selectOptions(engineTypes, { key: 'engineType', value: 'engineTypeName' }) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          label="选择集群"
        >
          {
            getFieldDecorator('basicInfo.clusterName', {
              initialValue: basicInfo.clusterName ? (`${basicInfo.clusterName}`) : '',
            })(
              <Select style={{ width: '150px' }} >
                { selectOptions(engineTypes[this.state.seletedEngine]
                  ? engineTypes[this.state.seletedEngine].clusters
                   : engineTypes[0].clusters,
                   { key: 'name', value: 'description', disable: 'disableCreate' }) }
              </Select>,
            )
          }
        </FormItem>
        <div className="form-br" />

        <FormItem {...formItemLayout}>
          {
            getFieldDecorator('fields', {
              initialValue: fields || [],
            })(
              <TableForm
                dataSource={fields}
                modalType={this.props.modalType}
                basicInfo={basicInfo}
                pasteData={this.props.pasteData}
                enums={{
                  metaTypes,
                  copyPath,
                }}
              />,
            )
          }
        </FormItem>
        <div className="form-br" />
        <Alert
          message="注意"
          description="修改索引后，需要点击启用索引，并且成功全索引方式导入数据后，方可生效"
          type="warning"
          showIcon
        />
        <div className="form-br" />
      </Form>
    );
  }
}

export default Form.create({})(FormModal(CreateForm));

