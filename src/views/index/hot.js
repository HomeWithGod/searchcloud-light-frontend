/*
 * Created by Alex Zhang on 2019/1/18
 */
import React from 'react';
import { Modal } from 'antd';

class Hot extends React.Component {

  render() {
    const { hotWords } = this.props;
    return (
      <Modal
        visible={this.props.visible}
        title={this.props.title}
        onCancel={this.props.onCancel}
        footer={false}
        wrapClassName="vertical-center-modal form-modal"
        width={this.props.width || 1000}
        height={this.props.height || 450}
        maskClosable={false}
      >
        <div>
          {hotWords.map(item => (<div>
            {item}
          </div>))}
          {hotWords && hotWords.length === 0 && <div>暂无热词</div>}
        </div>
      </Modal>
    );
  }
}
export default Hot;
