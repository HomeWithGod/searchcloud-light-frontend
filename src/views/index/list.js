import { connect } from 'dva';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Table, Button, Col, Row, Card, message } from 'antd';

import { fieldsToParams, createPageConfig } from 'src/utils/transform';
import AppInfoCard from 'src/components/biz/AppInfoCard';

import { systemConstants } from 'src/utils/constants';
import DetailForm from './createForm';
import Query from './query';
import Suggest from './suggest';
import Hot from './hot';

import './index.less';
import ListCURDCreator from '../creator/list';

function List({ dispatch, index: moduleModel }) {
  const { moduleName } = moduleModel;
  const basePayload = { module: moduleName };
  const { filteredList: list, toCreate, toUpdate, cancelSubmit } = ListCURDCreator(moduleModel, dispatch);
  function toChangeMetaStatus(e) {
    const id = e.currentTarget.id.split('_')[1];
    dispatch({
      type: `${moduleName}/enableItem`,
      payload: { id },
    });
  }

  function toQuery(e) {
    const id = e.currentTarget.id.split('_')[1];

    dispatch({
      type: `${moduleName}/readBasic`,
      payload: id,
    });

    dispatch({
      type: `${moduleName}/showQueryModal`,
    });
  }

  function toSuggest(e) {
    console.log('showSuggest');
    const id = e.currentTarget.id;
    dispatch({
      type: `${moduleName}/showSuggestModal`,
      payload: id,
    });
  }

  function toHot(e) {
    const id = e.currentTarget.id;
    console.log('showhOT');
    dispatch({
      type: `${moduleName}/readHot`,
      payload: id,
    });
    dispatch({
      type: `${moduleName}/showHotModal`,
    });
  }

  function suggest(keyword) {
    const indexName = moduleModel.indexName;
    dispatch({
      type: `${moduleName}/getSuggest`,
      payload: { indexName, keyword, size: 5 },
    });
  }

  function handleQuerySubmit(postParams) {
    dispatch({
      type: `${moduleName}/handleQuerySubmit`,
      payload: {
        module: moduleName,
        ...postParams,
      },
    });
  }

  function doSubmit(fieldsValue) {
    dispatch({
      type: `${moduleName}/updateIndex`,
      payload: {
        basicInfo: {
          ...moduleModel.entity.basicInfo,
          ...fieldsValue.basicInfo,
          projectId: moduleModel.relatedData.appData.id,
        },
        fields: fieldsValue.fields,
      },
    });
  }

  function onDataQueryTypeChange(type) {
    dispatch({ type: `${moduleName}/onDataQueryTypeChange`, payload: type });
  }

  function closeQueryModal() {
    dispatch({ type: `${moduleName}/closeQueryModal` });
  }
  function closeSuggestModal() {
    dispatch({ type: `${moduleName}/closeSuggestModal` });
  }
  function closeHotModal() {
    dispatch({ type: `${moduleName}/closeHotModal` });
  }

  function pasteData(entity) {
    dispatch({ type: `${moduleName}/pasteData`, payload: entity });
  }
  const paginationInfo = createPageConfig(moduleModel, list);
  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
      width: 30,
    },
    {
      title: '名称',
      dataIndex: 'indexName',
      key: 'indexName',
      width: 80,
    },
    {
      title: '当前版本',
      dataIndex: 'configVersion',
      key: 'configVersion',
      width: 30,
    },
    {
      title: '创建人',
      dataIndex: 'createByName',
      key: 'createByName',
      width: 50,
    },
    {
      title: '分片数',
      dataIndex: 'shardCount',
      key: 'shardCount',
      width: 50,
    },
    {
      title: '副本数',
      dataIndex: 'replicationFactor',
      key: 'replicationFactor',
      width: 50,
    },
    {
      title: '状态',
      dataIndex: 'metadataStatus',
      key: 'metadataStatus',
      width: 50,
      render: value => systemConstants.index.metadataStatus[value],
    },
    {
      title: '操作',
      width: 260,
      key: 'action',
      render: (text, item) => (
        <div>
          <Row className="opration-group">
            { item.metadataStatus === 1 && <a href="javascript:;" onClick={toChangeMetaStatus} id={`detailId_${item.id}`}>启用</a> }
            <a href="javascript:;" onClick={toUpdate} id={`detailId_${item.id}`}>修改</a>
            <Link to={`/searchcloud/datasource?indexId=${item.id}&projectId=${moduleModel.searchParams.projectId}`}>数据源管理</Link>
            <Link to={`/searchcloud/control?indexId=${item.id}&projectId=${moduleModel.searchParams.projectId}`}>数据源调度</Link>
            <a href="javascript:;" onClick={toQuery} id={`detailId_${item.id}`}>查数据</a>
            <a href="javascript:;" onClick={toSuggest} id={`${item.indexName}`}>suggest</a>
            <a href="javascript:;" onClick={toHot} id={`${item.indexName}`}>热词</a>
          </Row>
        </div>
      ),
    },
  ];

  const { appData, metaTypes, copyPath } = moduleModel.relatedData;

  const appCardConfig = {
    bordered: false,
    title: <div><Row>
      <Col span="20">{appData.name}项目信息</Col>
    </Row></div>,
  };

  return (
    <div>
      <Row style={{ marginBottom: '20px' }}>
        <Col span="24">
          <Card {...appCardConfig}>
            <AppInfoCard data={appData} />
          </Card>
        </Col>
      </Row>
      <Button type="primary" size={'default'} onClick={toCreate}>新建</Button>
      <div className="query-result">
        <div>
          <Table size="middle" columns={columns} dataSource={moduleModel.list} pagination={paginationInfo} />
        </div>
      </div>
      <DetailForm
        visible={moduleModel.showDetailModal}
        title={moduleModel.detailModalTitle}
        {...moduleModel}
        pasteData={pasteData}
        onCancel={cancelSubmit}
        onSubmit={doSubmit}
        width={1250}
      />
      <Query
        visible={moduleModel.showQueryModal}
        title={`数据搜索(索引：${moduleModel.entity.basicInfo.indexName})`}
        entity={moduleModel.entity}
        appData={appData}
        onCancel={closeQueryModal}
        onQuerySubmit={handleQuerySubmit}
        onDataQueryTypeChange={onDataQueryTypeChange}
        dataQueryResult={moduleModel.dataQueryResult}
      />
      <Suggest
        visible={moduleModel.showSuggestModal}
        title="suggest"
        suggest={suggest}
        onCancel={closeSuggestModal}
        suggests={moduleModel.suggests}
      />
      <Hot
        visible={moduleModel.showHotModal}
        title="热词"
        hotWords={moduleModel.hotWords}
        onCancel={closeHotModal}
      />
    </div>
  );
}

List.propTypes = {
  dispatch: PropTypes.func.isRequired,
  index: PropTypes.object.isRequired,
};

export default connect(({ index }) => ({ index }))(List);
