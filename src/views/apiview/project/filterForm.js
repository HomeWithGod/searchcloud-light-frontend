import React, { PropTypes } from 'react';
import { Button, Form, DatePicker, Select, Input } from 'antd';
import FormHandle from 'components/decorator/formHandle';
import { selectOptions } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';

const { RangePicker } = DatePicker;
const FormItem = Form.Item;
const Option = Select.Option;

class FilterForm extends React.Component {
  
  componentWillReceiveProps(nextProps){
    if( nextProps.selectedIndex !== this.props.selectedIndex ){
      const { form: { resetFields } } = this.props;
      resetFields(["selectedIndex"]);
    }
  }
  
  render() {
    const { getFieldDecorator } = this.props.form;
    const {
      selectedIndex,
      selectedProject,
      selections,
      startAverageResponseTime,
      endAverageResponseTime,
      onProjectSelect,
      onIndexSelect,
    } = this.props;

    return (
      <Form onSubmit={this.props.handleSubmit} layout="inline">
        <FormItem
          label="项目"
        >
          {
            getFieldDecorator('selectedProject', {
              initialValue: selectedProject || '-1',
            })(
              <Select style={{ width: '150px' }} onSelect={onProjectSelect}>
                <Option key="-1" value="-1">请选择</Option>
                { selectOptions(selections.project) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          label="索引"
        >
          {
            getFieldDecorator('selectedIndex', {
              initialValue: selectedIndex || '-1',
            })(
              <Select style={{ width: '150px' }} onSelect={onIndexSelect}>
                <Option key="-1" value="-1">请选择</Option>
                { selectOptions(selections.index) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          label="日期"
        >
          {
            getFieldDecorator('range', {
              initialValue: systemConstants.range2,
            })(
              <RangePicker
                format={systemConstants.dateFormat.frontEnd}
              />,
            )
          }
        </FormItem>
        <FormItem
          label="平响"
        >
          {
            getFieldDecorator('startAverageResponseTime', {
              initialValue: startAverageResponseTime,
            })(
              <Input placeholder="" style={{ display: 'inline', width: '50px' }} />,
            )
          }
        </FormItem>
        <span style={{ marginRight: '16px' }}>to</span>
        <FormItem>
          {
            getFieldDecorator('endAverageResponseTime', {
              initialValue: endAverageResponseTime,
            })(
              <Input placeholder="" style={{ display: 'inline', width: '50px' }} />,
            )
          }
        </FormItem>
        <Button onClick={this.props.onSubmitButtonClick} type="primary" size={'default'} htmlType="submit" style={{ marginRight: '12px' }}>查询</Button>
      </Form>
    );
  }
}
export default Form.create()(FormHandle(FilterForm));
