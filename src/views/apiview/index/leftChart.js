// chart of index average consumed time
import React, { Component, PropTypes } from 'react';
import ReactEcharts from 'echarts-for-react';

export default class LeftChart extends Component {

  get defaultOption() {
    return {
      title: {
        left: 0,
        show: false,
      },
      tooltip: {
        trigger: 'axis',
      },
      xAxis: {
        type: 'category',
        name: '日期',
        nameLocation: 'center',
        nameGap: 40,
        axisTick: {
          alignWithLabel: true,
        },
      },
      yAxis: {
        name: '请求量',
        // nameLocation: 'center',
        // nameGap: 40
      },
      series: [
        {
          type: 'line',
          dimensions: ['x', 'y'],
          encode: {
            x: 'x',
            y: 'y',
          },
        },
      ],
      dataset: {
        source: [
        ],
      },
    };
  }

  render() {
    return (
      <ReactEcharts
        notMerge
        option={{
          ...this.defaultOption,
          dataset: {
            source: this.props.chartData && this.props.chartData.dataSet && this.props.chartData.dataSet.invokeCount || [],
          },
        }}
      />
    );
  }

}
