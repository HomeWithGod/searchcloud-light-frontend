import React, { Component, PropTypes } from 'react';
import { Table } from 'antd';

export default class StatisticsList extends Component {

  get columns() {
    return [
      {
        title: '日期',
        dataIndex: 'date',
        key: 'date',
        width: 70,
      },
      {
        title: '索引',
        dataIndex: 'index',
        key: 'index',
        width: 70,
      },
      {
        title: '项目',
        dataIndex: 'project',
        key: 'project',
        width: 70,
      },
      {
        title: '请求量',
        dataIndex: 'requestNum',
        key: 'requestNum',
        width: 70,
      },
      {
        title: '平响',
        dataIndex: 'averageResponseTime',
        key: 'averageResponseTime',
        width: 70,
      },
      {
        title: '最大响应时间',
        dataIndex: 'maxResponseTime',
        key: 'maxResponseTime',
        width: 70,
      },
      {
        title: '(0,100]',
        dataIndex: 'num_0_100',
        key: 'num_0_100',
        width: 70,
      },
      {
        title: '(100,1000]',
        dataIndex: 'num_100_1000',
        key: 'num_100_1000',
        width: 70,
      },
      {
        title: '(1000,3000]',
        dataIndex: 'num_1000_3000',
        key: 'num_1000_3000',
        width: 70,
      },
      {
        title: '(3000,+∞)',
        dataIndex: 'num_3000_',
        key: 'num_3000_',
        width: 70,
      },
    ];
  }

  render() {
    const { data, paginationType, onPageChange } = this.props;

    return (
      <Table size="middle" columns={this.columns} dataSource={data} pagination={{ ...paginationType }} onChange={onPageChange} />
    );
  }

}
