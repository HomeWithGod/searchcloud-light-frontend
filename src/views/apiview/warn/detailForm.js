import React, { PropTypes } from 'react';
import { Button, Form, Select, Input, Row, Col } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import FormModal from 'components/decorator/formModal';
import { systemConstants } from 'src/utils/constants';

const Option = Select.Option;
const FormItem = Form.Item;

class DetailForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;

    // debugger;
    const entity = { ...this.props.entity };
    const { selections } = this.props;

    const formItemLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 14 },
    };

    return (
      <Form className="apiview-warn-create">
        <FormItem
          {...formItemLayout}
          label="项目"
          required
        >
          {
            getFieldDecorator('entity.selectedProject', {
              initialValue: entity.selectedProject,
              rules: [{ message: '必填', required: true }],
            })(
              <Select style={{ width: '150px' }}>
                <Option key="-1" value="-1">请选择</Option>
                { selectOptions(selections.project) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="索引"
        >
          {
            getFieldDecorator('entity.selectedIndex', {
              initialValue: entity.selectedIndex,
            })(
              <Select mode="multiple">
                {/* <Option key="-1" value="-1">请选择</Option> */}
                { selectOptions(selections.index) }
              </Select>,
            )
          }
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="响应时间"
        >
          {
            getFieldDecorator('entity.responseTime', {
              initialValue: entity ? entity.responseTime : '',
              rules: [/* { message: '必填', required: true }, */{ pattern: /^\d*$/, message: '只能输入整数' }],
            })(
              <Input
                addonBefore=">"
                addonAfter={
                  getFieldDecorator('entity.responseTimeWarnTimes', {
                    initialValue: entity && entity.responseTimeWarnTimes,
                    rules: [/* { message: '必填', required: true }, */{ pattern: /^\d*$/, message: '只能输入整数' }],
                  })(
                    <Input
                      style={{ width: '250px', borderTop: 'none', borderBottom: 'none' }}
                      placeholder="单位：次"
                    />,
                  )
                }
                placeholder="单位：毫秒"
                className="input-responseTime"
              />,
            )
          }
        </FormItem>
        {/* <FormItem
          { ...formItemLayout }
          label="5xx次数"
          required={true}
        >
          {
            getFieldDecorator('entity.fivexxTimes', {
              initialValue: entity ? entity.fivexxTimes : '',
            })(
              <Input
                addonBefore={
                  getFieldDecorator('entity.fivexxTimesOpertaor', {
                    initialValue: entity && entity.fivexxTimesOpertaor || '1',
                  })(
                    <Select
                      style={{ width: '30px' }}
                      showArrow={false}
                    >
                      { selectOptions(systemConstants.warn.operators) }
                    </Select>,
                  )
                }
                placeholder=""
              />,
            )
          }
        </FormItem>
        <FormItem
          { ...formItemLayout }
          label="4xx次数"
          required={true}
        >
          {
            getFieldDecorator('entity.fourxxTimes', {
              initialValue: entity ? entity.fourxxTimes : '',
            })(
              <Input
                addonBefore={
                  getFieldDecorator('entity.fourxxTimesOpertaor', {
                    initialValue: entity && entity.fourxxTimesOpertaor || '1',
                  })(
                    <Select
                      style={{ width: '30px' }}
                      showArrow={false}
                    >
                      { selectOptions(systemConstants.warn.operators) }
                    </Select>,
                  )
                }
                placeholder=""
              />,
            )
          }
        </FormItem>
        <FormItem
          { ...formItemLayout }
          label="成功率"
          required={true}
        >
          {
            getFieldDecorator('entity.successRate', {
              initialValue: entity ? entity.successRate : '',
            })(
              <Input
                addonBefore={
                  getFieldDecorator('entity.successRateOpertaor', {
                    initialValue: entity && entity.successRateOpertaor || '2',
                  })(
                    <Select
                      style={{ width: '30px' }}
                      showArrow={false}
                    >
                      { selectOptions(systemConstants.warn.operators) }
                    </Select>,
                  )
                }
                placeholder=""
              />,
            )
          }
        </FormItem> */}
        <FormItem
          {...formItemLayout}
          label="预警接收人"
          required
        >
          {
            getFieldDecorator('entity.mailTo', {
              initialValue: entity && entity.mailTo || '',
              rules: [{ message: '必填', required: true }],
            })(
              <Input placeholder="多个邮箱请用逗号分隔" />,
            )
          }
        </FormItem>
      </Form>
    );
  }
}
export default Form.create({})(FormModal(DetailForm));
