import React, { PropTypes } from 'react';
import { Button, Form, Select, Input } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import FormModal from 'components/decorator/formModal';
import { systemConstants } from 'src/utils/constants';
import _ from 'underscore';

const Option = Select.Option;
const FormItem = Form.Item;

class DetailForm extends React.Component {
  render() {
    const { form, entity } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 14 },
    };
    return (
      <Form>
        <FormItem {...formItemLayout} label="手机号" required>
          {getFieldDecorator('entity.mobile', {
            initialValue: entity ? entity.mobile : '',
          })(<Input placeholder="请输入手机号" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="密码" required>
          {getFieldDecorator('entity.password', {
            initialValue: entity ? entity.password : undefined,
          })(<Input placeholder="请输入密码" type="password" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="是否为管理员" required>
          {getFieldDecorator('entity.roleId', {
            initialValue: (entity && entity.roleId) || 1,
          })(
            <Select style={{ width: '150px' }}>
              <Option value={0}>是</Option>
              <Option value={1}>否</Option>
            </Select>,
          )}
        </FormItem>
      </Form>
    );
  }
}
export default Form.create({})(FormModal(DetailForm));
