import { connect } from 'dva';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Table, Button, Col, Row } from 'antd';

import { fieldsToParams, createPageConfig } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';

import FilterForm from './filterForm';
import DetailForm from './detailForm';
import ListCURDCreator from '../creator/list';

function List({ dispatch, userManager: moduleModel }) {
  const {
    filteredList: list,
    toCreate,
    toUpdate,
    toDelete,
    moduleName,
    doSubmit,
    cancelSubmit,
  } = ListCURDCreator(moduleModel, dispatch);

  function search(fields) {
    const params = fieldsToParams(fields);
    list({
      ...params,
      pageNo: 1,
    });
  }

  function pageChange(pageNo) {
    list({
      pageNo,
    });
  }

  const { searchParams, fieldErrors } = moduleModel;

  const columns = [
    {
      title: '用户Id',
      dataIndex: 'id',
      key: 'id',
      width: 70,
    },
    {
      title: '手机号',
      dataIndex: 'mobile',
      key: 'mobile',
      width: 150,
    },
    {
      title: '操作',
      width: 70,
      key: 'action',
      render: (text, item) => (
        <div className="list-action">
          <Row>
            <Col span="12">
              <a
                href="javascript:;"
                onClick={toUpdate}
                id={`detailId_${item.id}`}
              >
                编辑
              </a>
            </Col>
            <Col span="12">
              <a
                href="javascript:;"
                onClick={toDelete}
                id={`detailId_${item.id}`}
              >
                删除
              </a>
            </Col>
          </Row>
        </div>
      ),
    },
  ];

  return (
    <div>
      <Row style={{ marginBottom: '20px' }}>
        <Col span="22">
          <div className="query-form">
            <FilterForm {...searchParams} onSubmit={search} autoSubmit />
          </div>
        </Col>
        <Col span="2">
          <Button type="primary" size={'default'} onClick={toCreate}>
            新建
          </Button>
        </Col>
      </Row>
      <div className="query-result">
        <Table
          size="middle"
          columns={columns}
          dataSource={moduleModel.list}
          pagination={{
            // 分页
            simple: true,
            current: moduleModel.searchParams.pageNo,
            total: moduleModel.searchParams.total,
            onChange: pageChange,
          }}
        />
      </div>
      <DetailForm
        visible={moduleModel.showDetailModal}
        title={moduleModel.detailModalTitle}
        entity={moduleModel.entity}
        fieldErrors={fieldErrors}
        onCancel={cancelSubmit}
        onSubmit={doSubmit}
      />
    </div>
  );
}

List.propTypes = {
  dispatch: PropTypes.func.isRequired,
  userManager: PropTypes.object.isRequired,
};

export default connect(({ userManager }) => ({ userManager }))(List);
