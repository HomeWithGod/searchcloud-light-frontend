import React, { PropTypes } from 'react';
import { Button, Form, Input } from 'antd';
import FormHandle from 'components/decorator/formHandle';

const FormItem = Form.Item;

class FilterForm extends React.Component {
  render() {
    const { form, handleSubmit } = this.props;
    const { getFieldDecorator } = form;
    return (
      <Form onSubmit={handleSubmit} layout="inline">
        <FormItem label="手机号：">
          {getFieldDecorator('keyword', {
            initialValue: '',
          })(<Input placeholder="" />)}
        </FormItem>
        <Button
          type="primary"
          size={'default'}
          htmlType="submit"
          style={{ marginRight: '12px' }}
        >
          查询
        </Button>
      </Form>
    );
  }
}
export default Form.create()(FormHandle(FilterForm));
