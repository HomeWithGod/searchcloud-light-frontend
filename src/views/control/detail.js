import React from 'react';
import { Card, Modal, Col, Row, Timeline, Icon, Tag, Progress } from 'antd';
import Highlight from 'react-highlight';

import { formatJson } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';
import AppInfoCard from 'src/components/biz/AppInfoCard';
import IndexInfoCard from 'src/components/biz/IndexInfoCard';

class Detail extends React.Component {

  getEnumText(type) {
    const { entity } = this.props;
    const value = entity[type];
    const controlEnum = systemConstants.control;
    return controlEnum[type][value];
  }

  getTagColor(type) {
    const value = this.props.entity[type];
    if (type === 'status') {
      if (value === 0) {
        return 'orange';
      }

      if (value === 1) {
        return 'blue';
      }

      if (value === 2) {
        return 'green';
      }

      if (value === 3) {
        return 'red';
      }
    }

    return 'green';
  }

  getProgressInfo(field) {
    const { entity } = this.props;
    const total = entity[`${field}TotalCount`];
    const success = entity[`${field}SuccessCount`];

    return !total ? total : (success / total) * 100;
  }

  getPercent(field) {
    const { entity } = this.props;
    const total = entity[`${field}TotalCount`];
    const success = entity[`${field}SuccessCount`];

    return !total ? total : +((success / total) * 100).toFixed(0);
  }

  render() {
    const { relatedData, entity } = this.props;
    const { appData, indexData } = relatedData;


    const cardConfig = {
      bordered: false,
    };

    const displayItemStyle = { marginRight: '20px' };
    const sectionStyle = { marginBottom: '20px' };

    const clockIconStyle = { dot: <Icon type="clock-circle-o" style={{ fontSize: '16px' }} /> };

    return (
      <Modal
        visible={this.props.visible}
        title={this.props.title}
        onCancel={this.props.onCancel}
        footer={false}
        wrapClassName="vertical-center-modal form-modal"
        width={this.props.width || 800}
        height={this.props.height}
        maskClosable={false}
      >
        <div>
          <Row style={sectionStyle}>
            <Col span="24">
              <Card title={`${appData.projectName}项目信息`} {...cardConfig} >
                <AppInfoCard data={appData} />
              </Card>
            </Col>
          </Row>
          <Row style={sectionStyle}>
            <Col span="24">
              <Card title={`${indexData.indexName}索引信息`} {...cardConfig}>
                <IndexInfoCard data={indexData} />
              </Card>
            </Col>
          </Row>
          <Row style={sectionStyle}>
            <Col span="24">
              <Card title={'基本数据'} {...cardConfig}>
                <span style={displayItemStyle}>调度ID：{entity.id}</span>
                <span style={displayItemStyle}>数据源名称：{entity.datasourceName}</span>
                <span style={displayItemStyle}>运行类型：{this.getEnumText('runType')}</span>
                <span style={displayItemStyle}>任务粒度：{this.getEnumText('gradingType')}</span>
                <span style={displayItemStyle}>索引构建类型：{this.getEnumText('indexBuildType')}</span>
              </Card>
            </Col>
          </Row>
          <Card title={'执行进度'} {...cardConfig}>
            <Row style={sectionStyle}>
              <Col span="15">
                <span style={displayItemStyle}>执行状态： <Tag color={this.getTagColor('status')}>{this.getEnumText('status')}</Tag></span>
                {
                    entity.status === 3
                      && <div>
                        <span style={displayItemStyle}>失败原因: {entity.failReason || '--'}</span>
                        </div>
                 }

                <div>
                  <div style={{ marginBottom: '20px' }}>
                    <span style={displayItemStyle}>抓取总记录数：{entity.fetchTotalCount}</span>
                    <span style={displayItemStyle}>抓取成功记录数：{entity.fetchSuccessCount}</span>
                    <Progress type="circle" percent={this.getPercent('fetch')} width={40} />
                  </div>
                  <div>
                    <span style={displayItemStyle}>导入总记录数：{entity.importTotalCount}</span>
                    <span style={displayItemStyle}>导入成功记录数：{entity.importSuccessCount}</span>
                    <Progress type="circle" percent={this.getPercent('import')} width={40} />
                  </div>
                </div>
              </Col>
              <Col span="9">
                <Timeline>
                  <Timeline.Item {...clockIconStyle} color="green">启动时间：{entity.startTime}</Timeline.Item>
                  <Timeline.Item {...clockIconStyle} color="blue">执行开始时间：{entity.executeStartTime || '--'}</Timeline.Item>
                  <Timeline.Item {...clockIconStyle} color="red">执行结束时间：{entity.executeEndTime || '--'}</Timeline.Item>
                  <Timeline.Item {...clockIconStyle} >执行消耗总时间：{entity.executeCostTime}秒</Timeline.Item>
                </Timeline>
              </Col>
            </Row>
          </Card>
        </div>
      </Modal>
    );
  }
}
export default Detail;
