import React, { PropTypes } from 'react';
import { Button, Form, Select, Input, DatePicker } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import { systemConstants } from 'src/utils/constants';
import moment from 'moment';

const { RangePicker } = DatePicker;
const Option = Select.Option;
const FormItem = Form.Item;

class FilterForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.props.handleSubmit} layout="inline">
        <FormItem
          label="日期"
        >
          {
            getFieldDecorator('range', {
              initialValue: systemConstants.range,
            })(
              <RangePicker
                format={systemConstants.dateFormat.frontEnd}
              />,
            )
          }
        </FormItem>
        <FormItem
          label="状态"
        >
          {
            getFieldDecorator('status', {
              initialValue: '-1',
            })(
              <Select style={{ width: '150px' }}>
                <Option key="-1" value="-1">全部</Option>
                { selectOptions(systemConstants.control.status) }
              </Select>,
            )
          }
        </FormItem>
        <Button type="primary" size={'default'} htmlType="submit" style={{ marginRight: '12px' }}>查询</Button>
      </Form>
    );
  }
}
export default Form.create()(FormHandle(FilterForm));
