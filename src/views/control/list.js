import { connect } from 'dva';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Table, Button, Card, Col, Row, Radio, Select, Alert } from 'antd';

import { fieldsToParams, createPageConfig, radioOptions, selectOptions } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';
import AppInfoCard from 'src/components/biz/AppInfoCard';
import IndexInfoCard from 'src/components/biz/IndexInfoCard';

import FilterForm from './filterForm';
import Detail from './detail';
import ListCURDCreator from '../creator/list';
import { message } from 'antd';

const RadioGroup = Radio.Group;

function List({ dispatch, control: moduleModel }) {
  const { moduleName } = moduleModel;
  const basePayload = { module: moduleName };

  const { filteredList: list, toDetail, cancelSubmit } = ListCURDCreator(moduleModel, dispatch);

  const { frame } = moduleModel.relatedData;

  function search(fields) {
    const { range, status } = fieldsToParams(fields);
    const params = {};
    if (+status !== -1) {
      params.status = status;
    }
    if (range && range.length && range[0] && range[1]) {
      params.beginDate = range[0].startOf('day').format(systemConstants.dateFormat.backEnd);
      params.endDate = range[1].endOf('day').format(systemConstants.dateFormat.backEnd);
    }
    dispatch({ type: `${moduleName}/removeStatus` });

    list({ ...params });
  }

  function onGradingTypeChange(e) {
    dispatch({ type: `${moduleName}/gradingTypeChange`, payload: e.target.value });
  }

  function doImport() {
    const { gradingType, importedDatasource, relatedData } = moduleModel;
    if (gradingType !== '0' && importedDatasource == null) {
      message.error('请选择数据源', 3);
      return false;
    }

    const payload = { indexId: relatedData.indexData.id };
    if (gradingType !== '0') {
      payload.datasourceId = importedDatasource;
    }
    dispatch({ type: `${moduleName}/doImport`, payload });
  }

  function onIncrementTypeChange(e) {
    console.log(e.target.value);
    dispatch({ type: `${moduleName}/IncrementTypeChange`, payload: e.target.value });
  }

  function doIncrementImport() {
    const { IncrementType, importedIncrementDatasource, relatedData } = moduleModel;
    if ((relatedData && relatedData.incList.length === 0)) {
      message.error('该数据源没有增量配置', 3);
      return false;
    }
    if (IncrementType !== '0' && importedIncrementDatasource == null) {
      message.error('请选择数据源', 3);
      return false;
    }

    const payload = { indexId: relatedData.indexData.id };
    if (IncrementType !== '0') {
      payload.datasourceId = importedIncrementDatasource;
    }
    console.log(IncrementType, payload);
    dispatch({ type: `${moduleName}/doIncrementImport`, payload });
  }

  function onIncrementDatasourceChange(value) {
    dispatch({ type: `${moduleName}/onIncrementDatasourceChange`, payload: value });
  }

  function onDatasourceChange(value) {
    dispatch({ type: `${moduleName}/datasourceChange`, payload: value });
  }

  function toInterrupt(e) {
    const id = e.currentTarget.id.split('_')[1];
    dispatch({
      type: `${moduleName}/doInterrupt`,
      payload: id,
    });
  }

  function toPause(e) {
    const id = e.currentTarget.id.split('_')[1];
    dispatch({
      type: `${moduleName}/doPause`,
      payload: id,
    });
  }

  function toResume(e) {
    const id = e.currentTarget.id.split('_')[1];
    dispatch({
      type: `${moduleName}/doResume`,
      payload: id,
    });
  }

  function toLog(e) {
    const id = e.currentTarget.id.split('_')[1];
    dispatch({
      type: `${moduleName}/doLog`,
      payload: id,
    });
  }

  const paginationInfo = createPageConfig(moduleModel, list);

  const { searchParams, fieldErrors, relatedData } = moduleModel;
  const { appData, indexData, datasourceList, incList } = relatedData;

  const logEnvArr = [systemConstants.frame.env[2], systemConstants.frame.env[4]];

  const columns = [
    {
      title: 'id',
      dataIndex: 'id',
      key: 'id',
      width: 70,
    },
    {
      title: '任务粒度',
      dataIndex: 'gradingType',
      key: 'gradingType',
      width: 70,
      render: value => systemConstants.control.gradingType[value],
    },
    {
      title: '构建类型',
      dataIndex: 'indexBuildType',
      key: 'indexBuildType',
      width: 70,
      render: value => systemConstants.control.indexBuildType[value],
    },
    {
      title: '触发类型',
      dataIndex: 'runType',
      key: 'runType',
      width: 70,
      render: value => systemConstants.control.runType[value],
    },
    {
      title: '时间',
      dataIndex: 'startTime',
      key: 'startTime',
      width: 120,
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: 70,
      render: value => systemConstants.control.status[value],
    },
    {
      title: '操作',
      width: 120,
      key: 'action',
      render: (text, item) => (
        <div>
          <Row type="flex" justify="center" >
            <Col span={4}><span><a href="javascript:;" onClick={toDetail} id={`detailId_${item.id}`}>详情</a></span></Col>
            {item.indexBuildType === 0 && frame && item.status === 1 && <Col span={4}><span><a href="javascript:;" onClick={toInterrupt} id={`detailId_${item.id}`}>中断</a></span></Col>}
            {item.indexBuildType === 0 && frame && item.status === 1 && <Col span={4}><span><a href="javascript:;" onClick={toPause} id={`detailId_${item.id}`}>暂停</a></span></Col>}
            {item.indexBuildType === 0 && frame && item.status === 5 && <Col span={4}><span><a href="javascript:;" onClick={toResume} id={`detailId_${item.id}`}>恢复任务</a></span></Col>}
            {item.indexBuildType === 0 && frame && logEnvArr.indexOf(frame.env) != -1 && <Col span={4}><span><a href="javascript:;" onClick={toLog} id={`detailId_${item.id}`}>日志</a></span></Col>}
          </Row>
        </div>
      ),
    },
  ];

  const commonCardConfig = {
    bordered: false,
  };

  const appCardConfig = {
    ...commonCardConfig,
    title: <div><Row>
      <Col span="20">{appData.name}项目信息</Col>
    </Row></div>,
  };

  const indexCardConfig = {
    ...commonCardConfig,
    title: <div><Row>
      <Col span="20">{indexData.name}索引信息</Col>
    </Row></div>,
  };

  const gradingTypes = radioOptions(systemConstants.control.gradingType);
  const datasource = selectOptions(datasourceList, { key: 'id', value: 'name' });
  const incDatasource = selectOptions(incList, { key: 'id', value: 'name' });

  return (
    <div>
      <Row style={{ marginBottom: '20px' }}>
        <Col span="11">
          <Card {...appCardConfig}>
            <AppInfoCard data={appData} />
          </Card>
        </Col>
        <Col span="2" />
        <Col span="11">
          <Card {...indexCardConfig}>
            <IndexInfoCard data={indexData} />
          </Card>
        </Col>
      </Row>
      <Row style={{ marginBottom: '20px' }}>
        <Col span="11">
          <Card title={'全量索引导入'} {...commonCardConfig}>
            {/* <Alert */}
            {/* message="注：全索引构建才会重新生效新的索引设置，并在成功会弃用原有索引数据，单数据源只会按照ID更新该数据源相应数据" */}
            {/* description="" */}
            {/* type="warning" */}
            {/* closable */}
            {/* showIcon */}
            {/* /> */}
            <RadioGroup onChange={onGradingTypeChange} value={moduleModel.gradingType} options={gradingTypes} />
            <Select
              placeholder="选择一个数据源" onChange={onDatasourceChange}
              style={moduleModel.datasourceSelectStyle}
            >{ datasource }</Select>
            <Button onClick={doImport} type="primary" style={{ marginLeft: '12px' }}>执行导入</Button>
          </Card>
        </Col>
        <Col span="2" />
        <Col span="11">
          <Card title={'增量索引导入'} {...commonCardConfig}>
            {/* <Alert */}
            {/* message="注：全索引构建才会重新生效新的索引设置，并在成功会弃用原有索引数据，单数据源只会按照ID更新该数据源相应数据" */}
            {/* description="" */}
            {/* type="warning" */}
            {/* closable */}
            {/* showIcon */}
            {/* /> */}
            <RadioGroup onChange={onIncrementTypeChange} value={moduleModel.IncrementType} options={gradingTypes} />
            {moduleModel.IncrementType !== '0' && <Select
              placeholder="选择一个数据源" style={{ width: 200 }} onChange={onIncrementDatasourceChange}
            >{ incDatasource }</Select>}
            <Button onClick={doIncrementImport} type="primary" style={{ marginLeft: '12px' }}>执行导入</Button>
          </Card>
        </Col>
      </Row>
      <Row style={{ marginBottom: '20px' }} />
      <Row style={{ marginBottom: '20px' }}>
        <Col span="24">
          <Card title={'调度历史'} {...commonCardConfig}>
            <div className="query-form">
              <FilterForm
                {...searchParams}
                onSubmit={search}
                fieldErrors={fieldErrors}
                autoSubmit
              />
            </div>
            <div className="query-result">
              <div>
                <Table size="middle" columns={columns} dataSource={moduleModel.list} pagination={paginationInfo} />
              </div>
            </div>
          </Card>
        </Col>
      </Row>
      <Detail
        {...moduleModel}
        visible={moduleModel.showDetailModal}
        title={'调度详情'}
        onCancel={cancelSubmit}
      />
    </div>
  );
}

List.propTypes = {
  dispatch: PropTypes.func.isRequired,
  control: PropTypes.object.isRequired,
};

export default connect(({ control }) => ({ control }))(List);
