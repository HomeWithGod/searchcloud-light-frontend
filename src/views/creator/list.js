const ListCURDCreator = (model, dispatch) => {
  const { moduleName } = model;
  const basePayload = { module: moduleName };

  return {
    list() {
      dispatch({
        type: `${moduleName}/list`,
        payload: basePayload,
      });
    },

    filteredList(searchParams) {
      dispatch({
        type: `${moduleName}/paramsChange`,
        payload: searchParams,
      });

      dispatch({ type: `${moduleName}/list`, payload: basePayload });
    },

    toCreate() {
      dispatch({
        type: `${moduleName}/showDetailModal`,
        payload: { modalType: 'create' },
      });
    },

    toUpdate(e) {
      const id = e.currentTarget.id.split('_')[1];

      dispatch({
        type: `${moduleName}/read`,
        payload: id,
      });

      dispatch({
        type: `${moduleName}/showDetailModal`,
        payload: { modalType: 'update' },
      });
    },

    toDetail(e) {
      const id = e.currentTarget.id.split('_')[1];
      dispatch({
        type: `${moduleName}/read`,
        payload: id,
      });

      dispatch({
        type: `${moduleName}/showDetailModal`,
        payload: { modalType: 'read' },
      });
    },

    toDelete(e) {
      const id = e.currentTarget.id.split('_')[1];
      dispatch({
        type: `${moduleName}/deleteItem`,
        payload: { id, ...basePayload },
      });
    },

    doSubmit(fieldsValue) {
      dispatch({
        type: `${moduleName}/saveDetail`,
        payload: {
          param: {
            ...model.entity,
            ...fieldsValue.entity,
          },
          ...basePayload,
        },
      });
    },

    cancelSubmit() {
      dispatch({ type: `${moduleName}/closeDetailModal` });
    },
  };
};

export default ListCURDCreator;
