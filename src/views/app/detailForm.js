import React, { PropTypes } from 'react';
import { Button, Form, Select, Input } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import FormModal from 'components/decorator/formModal';
import { systemConstants } from 'src/utils/constants';

const Option = Select.Option;
const FormItem = Form.Item;

class DetailForm extends React.Component {
  render() {
    const { form, entity, users } = this.props;
    const { getFieldDecorator } = form;
    const formItemLayout = {
      labelCol: { span: 7 },
      wrapperCol: { span: 14 },
    };

    // 用户选项
    const userList = users
      ? users.map(item => ({
        key: item.accountId,
        title: item.userName,
        value: item.accountId,
      }))
      : [];
    const accountIds =
      entity && entity.accountList
        ? entity.accountList.map(item => item.accountId)
        : [];
    return (
      <Form>
        <FormItem
          {...formItemLayout}
          label="编码"
          required
          extra="项目唯一编码，服务调用时会需要使用，一旦确定，不允许修改"
        >
          {getFieldDecorator('entity.projectCode', {
            initialValue: entity ? entity.projectCode : '',
          })(
            <Input placeholder="" disabled={!!(entity && entity.projectCode)} />,
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="名称"
          required
          extra="项目中文名称"
        >
          {getFieldDecorator('entity.projectName', {
            initialValue: entity ? entity.projectName : '',
          })(<Input placeholder="" />)}
        </FormItem>
        <FormItem {...formItemLayout} label="状态" required>
          {getFieldDecorator('entity.status', {
            initialValue: (entity && entity.status) || '0',
          })(
            <Select style={{ width: '150px' }} disabled>
              {selectOptions(systemConstants.app.status)}
            </Select>,
          )}
        </FormItem>
        <FormItem {...formItemLayout} label="用户">
          {getFieldDecorator('entity.accountIds', {
            initialValue: accountIds || undefined,
          })(
            <Select mode="multiple">
              {userList.map(item => (
                <Option key={item.key} value={item.value}>
                  {item.title}
                </Option>
              ))}
            </Select>,
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="告警邮箱"
          extra="多个邮箱请用逗号分隔"
        >
          {getFieldDecorator('entity.mailTo', {
            initialValue: (entity && entity.mailTo) || '',
          })(
            <Input placeholder="example1@pingan.com.cn,example2@pingan.com.cn" />,
          )}
        </FormItem>
        <FormItem
          {...formItemLayout}
          label="描述"
          required
          extra="一段文字描述一下项目用途、背景等"
        >
          {getFieldDecorator('entity.introduction', {
            initialValue: entity ? entity.introduction : '',
          })(<Input type="textarea" rows={4} />)}
        </FormItem>
      </Form>
    );
  }
}
export default Form.create({})(FormModal(DetailForm));
