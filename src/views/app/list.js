import { connect } from 'dva';
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import { Table, Button, Col, Row } from 'antd';

import { fieldsToParams, createPageConfig } from 'src/utils/transform';
import { systemConstants } from 'src/utils/constants';

import FilterForm from './filterForm';
import DetailForm from './detailForm';
import ListCURDCreator from '../creator/list';

function List({ dispatch, app: moduleModel }) {
  const {
    filteredList: list,
    toCreate,
    toUpdate,
    toDelete,
    doSubmit,
    cancelSubmit,
  } = ListCURDCreator(moduleModel, dispatch);

  function search(fields) {
    const params = fieldsToParams(fields);
    list({
      ...params,
    });
  }

  const { searchParams, fieldErrors } = moduleModel;

  const columns = [
    {
      title: 'Appid',
      dataIndex: 'id',
      key: 'id',
      width: 70,
    },
    {
      title: '名称',
      dataIndex: 'projectName',
      key: 'projectName',
      width: 120,
    },
    {
      title: '编码',
      dataIndex: 'projectCode',
      key: 'projectCode',
      width: 70,
    },
    {
      title: '负责人',
      dataIndex: 'masterUserName',
      key: 'masterUserName',
      width: 170,
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: 70,
      render: value => systemConstants.app.status[value],
    },
    {
      title: '操作',
      width: 70,
      key: 'action',
      render: (text, item) => (
        <div className="list-action">
          <Row>
            <Col span="12">
              <Link to={`/searchcloud/index?projectId=${item.id}`}>
                索引管理
              </Link>
            </Col>
            <Col span="12">
              <a
                href="javascript:;"
                onClick={toUpdate}
                id={`detailId_${item.id}`}
              >
                修改
              </a>
            </Col>
          </Row>
        </div>
      ),
    },
  ];

  return (
    <div>
      <Row style={{ marginBottom: '20px' }}>
        <Col span="22">
          <div className="query-form">
            <FilterForm {...searchParams} onSubmit={search} autoSubmit />
          </div>
        </Col>
        <Col span="2">
          <Button type="primary" size={'default'} onClick={toCreate}>
            新建
          </Button>
        </Col>
      </Row>
      <div className="query-result">
        <Table
          size="middle"
          columns={columns}
          dataSource={moduleModel.list}
          pagination={false}
        />
      </div>
      <DetailForm
        visible={moduleModel.showDetailModal}
        title={moduleModel.detailModalTitle}
        users={moduleModel.users}
        entity={moduleModel.entity}
        fieldErrors={fieldErrors}
        onCancel={cancelSubmit}
        onSubmit={doSubmit}
      />
    </div>
  );
}

List.propTypes = {
  dispatch: PropTypes.func.isRequired,
  app: PropTypes.object.isRequired,
};

export default connect(({ app }) => ({ app }))(List);
