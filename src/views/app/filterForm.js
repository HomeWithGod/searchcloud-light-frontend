import React, { PropTypes } from 'react';
import { Button, Form, Select, Input } from 'antd';
import { selectOptions } from 'src/utils/transform';
import FormHandle from 'components/decorator/formHandle';
import { systemConstants } from 'src/utils/constants';

const Option = Select.Option;
const FormItem = Form.Item;

class FilterForm extends React.Component {

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Form onSubmit={this.props.handleSubmit} layout="inline">
        <FormItem
          label="编码"
        >
          {
            getFieldDecorator('projectCode', {
              initialValue: '',
            })(
              <Input placeholder="" />,
            )
          }
        </FormItem>
        <FormItem
          label="名称"
        >
          {
            getFieldDecorator('projectName', {
              initialValue: '',
            })(
              <Input placeholder="" />,
            )
          }
        </FormItem>
        <FormItem
          label="状态"
        >
          {
            getFieldDecorator('status', {
              initialValue: '-1',
            })(
              <Select style={{ width: '150px' }}>
                <Option key="-1" value="-1">全部</Option>
                { selectOptions(systemConstants.app.status) }
              </Select>,
            )
          }
        </FormItem>
        <Button type="primary" size={'default'} htmlType="submit" style={{ marginRight: '12px' }}>查询</Button>
      </Form>
    );
  }
}
export default Form.create()(FormHandle(FilterForm));
