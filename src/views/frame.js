import React, { PropTypes } from 'react';
// import { connect } from 'dva'
import { connect } from 'react-redux';

import Header from 'components/common/header';
import Container from 'components/common/container';
import SideBar from 'components/common/sidebar';
import { config } from '../utils';
import { bindActionCreators } from 'redux';
import { Spin } from 'antd';


import 'css/main.less';

function Frame({ children, location, dispatch, frame, loading, routes, params }) {
  const headerProps = {
    frame,
    logoutPage() {
      dispatch({
        type: 'frame/logout',
      });
    },
    showPasswordModal() {
      dispatch({
        type: 'frame/showPasswordModal',
      });
    },
    doSubmit(fieldsValue) {
      console.log(fieldsValue);
      dispatch({
        type: 'frame/modifyPassword',
        payload: {
          param: {
            ...fieldsValue,
          },
        },
      });
    },

    cancelSubmit() {
      dispatch({ type: 'frame/closeDetailModal' });
    },
  };

  const containerStyle = {};
  if (!frame.login) {
    containerStyle.left = 0;
  }
  if (!frame.showHeader) {
    containerStyle.top = 0;
  }
  const containerProps = {
    children,
    containerClass: frame.containerClass,
    crumbList: { crumbList: frame.crumbList },
    login: frame.login,
    routes: frame.routes,
    params: frame.params,
    style: containerStyle,
  };

  const siderProps = {
    frame,
    handleSelect(payload) {
    },
    toggleSidebar(payload) {
    },
  };

  return (
    <div className="layout-frame">
      <Spin spinning={loading.global} />
      {frame.showHeader && <Header {...headerProps} />}
      <div>
        { frame.login && <SideBar {...siderProps} /> }
        <Container {...containerProps} />
      </div>
    </div>
  );
}


Frame.propTypes = {
  location: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  frame: PropTypes.object.isRequired,
};
export default connect(({ frame, loading }) => ({ frame, loading }))(Frame);
