import { request, addConverter } from '../utils';
import buildService from './buildService';

const baseService = buildService('searchcloudMonitorInfo');// 实参为向后端发起请求的实际路径前缀的一部分

export default {
  ...baseService,
  async list(params) {
    return request(
      `${baseService.requestHost}/top`,
      {
        method: 'GET',
        data: params,
      },
    );
  },
};


addConverter(
  'GET', // request method
  `${baseService.requestHost}/top`, // request url
  {
    post(data) { // posthandle function
      /* data.topAverageResponseTimeList = data.avgResponseTopList;
      data.topAverageResponseTimeList.forEach((item, i) => {
        item.index = item.indexName;
        item.project = item.projectName;
        item.callNum = item.requestNum;
        item.averageResponseTime = item.avgResponseTime;
        item.num_0_100 = item.level1Count;
        item.num_100_1000 = item.level2Count;
        item.num_1000_3000 = item.level3Count;
        item.num_3000_ = item.level4Count;
        item.key = i + '_' + new Date().getTime();
      });

      data.topCallNumList = data.requestCountTopList;
      data.topCallNumList.forEach((item, i) => {
        item.index = item.indexName;
        item.project =  item.projectName;
        item.callNum = item.requestNum;
        item.averageResponseTime = item.avgResponseTime;
        item.key = i + '_' + new Date().getTime();
      }); */

      data.topIndexLoseRateList = data.indexFailTop10;
      data.topIndexLoseRateList.forEach((item, i) => {
        item.index = item.indexName;
        item.project = item.projectName;
        item.callNum = item.execTotalNum;
        item.loseNum = item.failNum;
        item.failedRate = item.failRate;
        item.key = `${i}_${new Date().getTime()}`;
      });

      data.topIndexConsumedTimeList = data.indexCostTop10;
      data.topIndexConsumedTimeList.forEach((item, i) => {
        item.index = item.indexName;
        item.project = item.projectName;
        item.callNum = item.execTotalNum;
        item.failedRate = item.failRate;
        item.indexNum = item.indexTotal;
        item.consumedTime = item.costTimeTotal;
        item.key = `${i}_${new Date().getTime()}`;
      });

      data.generalList = [data.searchcloudStatusInfoDTO];
      data.generalList.forEach((item, i) => {
        item.indexNum = item.countIndex;
        item.projectNum = item.countProject;
        /* item.requestNum = item.total;
        item.averageResponseTime = item.avgResponse; */
        item.indexConsumedTime = item.indexCostTime;
        /* item.apiSuccessRate = item.successRate;
        item.fourXXTimes = item.count_4XX;
        item.fiveXXTimes = item.count_5XX; */
        item.key = `${i}_${new Date().getTime()}`;
        let content = '';
        for (const key in item.mapHealthRate) {
          content += `${key}: ${item.mapHealthRate[key]}\n`;
        }
        item.solrHealthRate = content;
        content = '';
        for (const key in item.mapDiskOccupancy) {
          content += `${key}: ${item.mapDiskOccupancy[key]}\n`;
        }
        item.harddiskUsageRate = content;
      });
    },
    pre(data) { // prehandle function
      data.fromTime = data.beginDate;
      data.toTime = data.endDate;
      delete data.beginDate;
      delete data.endDate;
    },
  },
);
