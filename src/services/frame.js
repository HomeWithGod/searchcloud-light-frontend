import { request } from '../utils';

export async function logout() {
  return request('/auth/logout', { method: 'GET' });
}

export async function login(data) {
  return request('/auth/login', { method: 'POST', data });
}

export async function userInfo() {
  return request('/session/visitor', { method: 'get' });
}

export async function envInfo() {
  return request('/session/env', { method: 'get' });
}

export async function getUsers() {
  return request('/user/optionList', { method: 'GET' });
}

export async function getMenu() {
  return request('/menu/list', { method: 'GET' });
}
