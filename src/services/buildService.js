import { request, addConverter } from '../utils';
import u from 'underscore';

const buildService = (entityName) => {
  const requestHost = `/${entityName}`;

  return {
    requestHost,
    async list(params) {
      return request(
        requestHost,
        {
          method: 'GET',
          data: params,
        },
      );
    },

    async create(entity) {
      return request(
        requestHost,
        {
          method: 'POST',
          data: entity,
        },
      );
    },

    async update(entity) {
      // let submitEntity = u.omit(entity, 'id');
      return request(
        requestHost,
        {
          method: 'POST',
          data: entity,
        },
      );
    },

    async remove(id) {
      return request(
        `${requestHost}/${id}`,
        {
          method: 'DELETE',
        },
      );
    },

    async detail(id) {
      return request(
        `${requestHost}/${id}`,
        {
          method: 'GET',
        },
      );
    },

    async projectList() {
      return request(
        '/dashboardCommon/projects',
        {
          method: 'GET',
        },
      );
    },

    async indexList(projectId) {
      return request(
        `/dashboardCommon/project/${projectId}/indexes`,
        {
          method: 'GET',
        },
        '/dashboardCommon/project/${projectId}/indexes', // urlPattern
      );
    },

    async warningConfigList(params) {
      return request(
        `/dashboardWarningConfig/${params.warningType}`,
        {
          method: 'GET',
          data: params,
        },
        '/dashboardWarningConfig/${warningType}', // urlPattern
      );
    },

    async deleteWarnCfg(id) {
      return request(
        `/dashboardWarningConfig/${id}`,
        {
          method: 'DELETE',
        },
        '/dashboardWarningConfig/${id}', // urlPattern
      );
    },
  };
};

export default buildService;


addConverter(
  'GET', // request method
  '/dashboardCommon/projects', // request url
  {
    post(data) { // posthandle function
      const result = {};
      data.forEach((item) => {
        result[item.id] = item.projectName;
      });
      return result;
    },
    pre(data) { // prehandle function

    },
  },
);

addConverter(
  'GET', // request method
  '/dashboardCommon/project/${projectId}/indexes', // request url
  {
    post(data) { // posthandle function
      const result = {};
      data.forEach((item) => {
        result[item.id] = item.indexName;
      });
      return result;
    },
    pre(data) { // prehandle function

    },
  },
);

addConverter(
  'GET', // request method
  '/dashboardWarningConfig/${warningType}', // request url
  {
    post(data) { // posthandle function
      const result = {
        statisticsList: [],
      };

      data.datas.forEach((item, i) => {
        result.statisticsList.push({
          id: item.id,
          index: item.indexName,
          project: item.projectName,
          warnCondition: item.warningConfig,
          warnMan: item.mailTo,
          warnTimes: item.waringNum,
          lastWarnTime: item.waringTime,
          key: `${i}_${new Date().getTime()}`,
        });
      });

      return {
        datas: result,
        total: data.total,
      };
    },
    pre(data) { // prehandle function
      if (data.selectedWarnStatus == '-1') {
        delete data.selectedWarnStatus;
      }
      if (data.selectedProject == '-1') {
        delete data.selectedProject;
      }
      if (!data.warnMan) {
        delete data.warnMan;
      }
      return {
        startDate: data.beginDate,
        endDate: data.endDate,
        status: data.selectedWarnStatus,
        projectId: data.selectedProject,
        waringUser: data.warnMan,
        pageNo: data.pageNo,
        pageSize: data.pageSize,
      };
    },
  },
);
