import { request, addConverter } from '../utils';
import buildService from './buildService';

const baseService = buildService('dashboardIndexSummaryInfo');// 实参为向后端发起请求的实际路径前缀的一部分

export default {
  ...baseService,
  async list(params) {
    return request(
      `${baseService.requestHost}/indexView`,
      {
        method: 'GET',
        data: params,
      },
    );
  },

  async chartList(params) {
    return request(
      `${baseService.requestHost}/chartList`,
      {
        method: 'GET',
        data: params,
      },
    );
  },

  async saveDetail(entity) {
    return request(
      '/dashboardWarningConfig/index',
      {
        method: 'POST',
        data: entity,
      },
    );
  },
};


addConverter(
  'GET', // request method
  `${baseService.requestHost}/indexView`, // request url
  {
    post(data) { // posthandle function
      const result = {
        statisticsList: [],
        chart: {},
      };
      let total;
      if (data.datas) {
        data.datas.forEach((item, i) => {
          result.statisticsList.push({
            project: item.projectName,
            dataSourceNum: item.datasourceNum,
            indexNum: item.indexNum,
            index: item.indexName,
            indexAverageConsumedTime: item.avgCost,
            expectedExecutionTimes: item.expectNum,
            recordAverageConsumedTime: item.recordCost,
            timedExecutionTimes: item.actualNum,
            manualExecutionTimes: item.manualNum,
            manualSuccessRate: item.manualSuccessRate,
            timedSuccessRate: item.actualSuccessRate,
            key: `${i}_${new Date().getTime()}`,
          });

          result.chart.dataSet = [];

          result.chart.dataSet.push({
            date: item.dataDate,
            indexAverageConsumedTime: item.avgCost,
            successRate: item.actualSuccessRate,
          });
        });

        total = data.total;
      } else if (data.pageDto) {
        data.pageDto.datas.forEach((item, i) => {
          result.statisticsList.push({
            id: item.id,
            index: item.indexName,
            project: item.projectName,
            dataSourceNum: item.datasourceNum,
            recordAverageConsumedTime: item.avgCost,
            manualOrTimed: item.runTypeFomamt,
            status: item.statusFommat,
            indexNum: item.recordTotalNum,
            indexConsumedTime: item.recordTotalCost,
            key: `${i}_${new Date().getTime()}`,
          });

          // result.chart.dataSet.push({
          //   date: item.dataDate,
          //   indexAverageConsumedTime: item.avgCost,
          //   successRate: item.actualSuccessRate,
          // })
          result.chart.dataSet = {
            indexAvgCost: {
              x: {},
              serieData: [],
            },
            indexCost: {
              x: {},
              serieData: [],
            },
          };

          const indexAvgCost = result.chart.dataSet.indexAvgCost;
          const indexCost = result.chart.dataSet.indexCost;

          data.indexAvgCost.forEach((item) => {
            const serie = {
              name: item.name,
              y: [],
            };
            item.xyMapList.forEach((item) => {
              indexAvgCost.x[item.x] = item.x;
              serie.y.push(item.y);
            });
            indexAvgCost.serieData.push(serie);
            indexAvgCost.x = Object.keys(indexAvgCost.x).sort((a, b) => a < b);
          });

          data.indexCost.forEach((item) => {
            const serie = {
              name: item.name,
              y: [],
            };
            item.xyMapList.forEach((item) => {
              indexCost.x[item.x] = item.x;
              serie.y.push(item.y);
            });
            indexCost.serieData.push(serie);
            indexCost.x = Object.keys(indexCost.x).sort((a, b) => a < b);
          });
        });
        total = data.pageDto.total;
      }

      return {
        datas: result,
        total,
      };
    },
    pre(data) { // prehandle function
      if (data.selectedIndex == '-1' || (Array.isArray(data.selectedIndex) && !data.selectedIndex.length)) {
        delete data.selectedIndex;
      }
      if (data.selectedProject == '-1') {
        delete data.selectedProject;
      }
      return {
        startDate: data.beginDate,
        endDate: data.endDate,
        indexId: Array.isArray(data.selectedIndex) ? data.selectedIndex.join(',') : data.selectedIndex,
        projectId: data.selectedProject,
        minAvgCost: data.startIndexAverageConsumedTime,
        maxAvgCost: data.endIndexAverageConsumedTime,
        minRecordCost: data.startSingleAverageConsumedTime,
        maxRecordCost: data.endSingleAverageConsumedTime,
        pageNo: data.pageNo,
        pageSize: data.pageSize,
      };
    },
    proxy(data) {
      const result = {
        url: `${baseService.requestHost}/indexViewByPage`,
      };

      if (data.selectedProject && data.selectedProject != -1) {
        result.url = `${baseService.requestHost}/indexViewByProjectOfPage`;
      }

      if (data.selectedIndex && data.selectedIndex != -1) {
        result.url = `${baseService.requestHost}/indexViewByIndex`;
      }

      return result;
    },
  },
);

addConverter(
  'GET', // request method
  `${baseService.requestHost}/chartList`, // request url
  {
    post(data) { // posthandle function
      return {
        dataSet: {
          indexAvgCost: data.indexAvgCost.xyMapList,
          indexSuccessRate: data.indexSuccessRate.xyMapList,
        },
      };
    },
    pre(data) { // prehandle function
      if (data.selectedIndex == '-1') {
        delete data.selectedIndex;
      }
      if (data.selectedProject == '-1') {
        delete data.selectedProject;
      }
      return {
        startDate: data.beginDate,
        endDate: data.endDate,
        indexId: data.selectedIndex,
        projectId: data.selectedProject,
        minAvgCost: data.startIndexAverageConsumedTime,
        maxAvgCost: data.endIndexAverageConsumedTime,
        minRecordCost: data.startSingleAverageConsumedTime,
        maxRecordCost: data.endSingleAverageConsumedTime,
      };
    },
    proxy(data) {
      const result = {
        url: `${baseService.requestHost}/indexViewByChart`,
      };

      if (data.selectedProject && data.selectedProject != -1 && data.selectedIndex == -1) {
        result.url = `${baseService.requestHost}/indexViewByProjectOfChart`;
      }

      return result;
    },
  },
);

addConverter(
  'POST', // request method
  '/dashboardWarningConfig/index', // request url
  {
    post(data) { // posthandle function
      // console.log(data)
    },
    pre(data) { // prehandle function
      const d = {
        indexIds: data.selectedIndex && data.selectedIndex.join(','),
        projectId: data.selectedProject,
        recordCost: data.singleIndexAverageConsumedTime,
        indexCost: data.indexConsumedTime,
        successRate: data.successRate,
        mailTo: data.mailTo,
      };

      if (!d.indexIds) {
        delete d.indexIds;
      }

      return d;
    },
  },
);
