import { request } from '../utils';
import buildService from './buildService';
import u from 'underscore';

const baseService = buildService('index');

export default {
  ...baseService,

  async basicInfo(id) {
    return request(
      `${baseService.requestHost}/basicInfo/${id}`,
      { method: 'GET' },
    );
  },

  async queryData(params) {
    const type = params.type;
    const searchParams = u.omit(params, 'type');
    if (params.type === 'sql') {
      return request(
        '/sql/select',
        { method: 'POST', data: searchParams },
      );
    }
  },

  async metaTypes() {
    return request(
      '/metadata/allTypes',
      { method: 'GET' },
    );
  },

  async clusterInfo() {
    return request(
      '/index/clusters',
      { method: 'GET' },
    );
  },

  async updateIndex(params) {
    return request(
      '/index',
      { method: 'POST', data: { ...params } },
    );
  },

  async enable(id) {
    return request(
      `${baseService.requestHost}/enableIndex/${id}`,
      { method: 'PUT' },
    );
  },

  async hot(indexName) {
    return request(
      `/sql/hot?indexName=${indexName}&num=10`,
      { method: 'GET' },
    );
  },

  async suggest(data) {
    return request(
      '/sql/suggest',
      { method: 'POST', data },
    );
  },

};
