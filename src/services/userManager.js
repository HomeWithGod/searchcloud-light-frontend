/*
 * Created by Alex Zhang on 2019/1/11
 */
import { request } from '../utils';

export async function userList(data) {
  return request('/user/list', { method: 'GET', data });
}

export async function editPassword(data) {
  return request('/user/modify/password', { method: 'POST', data });
}

export async function editUser(id, data) {
  return request(`/user/edit/${id}`, { method: 'POST', data });
}

export async function deleteUser(id) {
  return request(`/user/delete/${id}`, { method: 'POST' });
}

export async function userInfo(id) {
  return request(`/user/detail/${id}`, { method: 'GET' });
}

export async function projects() {
  return request('/dashboardCommon/projects', { method: 'GET' });
}

export async function addUser(data) {
  return request('/user/add', { method: 'POST', data });
}
