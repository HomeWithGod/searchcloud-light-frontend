import { request, addConverter } from '../utils';
import buildService from './buildService';

const baseService = buildService('dashboardApiSummaryInfo');// 实参为向后端发起请求的实际路径前缀的一部分

export default {
  ...baseService,
  async list(params) {
    return request(
      `${baseService.requestHost}/getTablePart`,
      {
        method: 'GET',
        data: params,
      },
    );
  },

  async chartList(params) {
    return request(
      `${baseService.requestHost}/getChartPart`,
      {
        method: 'GET',
        data: params,
      },
    );
  },

  async saveDetail(entity) {
    return request(
      '/dashboardWarningConfig/api',
      {
        method: 'POST',
        data: entity,
      },
    );
  },
};


addConverter(
  'GET', // request method
  `${baseService.requestHost}/getTablePart`, // request url
  {
    post(data) { // posthandle function
      const result = {
        statisticsList: [],
      };

      data.datas.forEach((item, i) => {
        result.statisticsList.push({
          date: item.dataDate,
          index: item.indexName,
          indexNum: item.indexCount,
          project: item.projectName,
          requestNum: item.requestNum,
          averageResponseTime: item.avgResponseTime,
          maxResponseTime: item.maxResponseTime,
          num_0_100: item.level1Rate,
          num_100_1000: item.level2Rate,
          num_1000_3000: item.level3Rate,
          num_3000_: item.level4Rate,
          key: `${i}_${new Date().getTime()}`,
        });
      });

      return {
        datas: result,
        total: data.total,
      };
    },
    pre(data) { // prehandle function
      if (data.selectedIndex == '-1') {
        delete data.selectedIndex;
      }
      if (data.selectedProject == '-1') {
        delete data.selectedProject;
      }
      return {
        startDate: data.beginDate,
        endDate: data.endDate,
        indexId: data.selectedIndex,
        projectId: data.selectedProject,
        minAvgResponseTime: data.startAverageResponseTime,
        maxAvgResponseTime: data.endAverageResponseTime,
        pageNo: data.pageNo,
        pageSize: data.pageSize,
      };
    },
  },
);

addConverter(
  'GET', // request method
  `${baseService.requestHost}/getChartPart`, // request url
  {
    post(data) { // posthandle function
      return {
        dataSet: {
          avgDuration: data.avgDuration.xyMapList,
          invokeCount: data.invokeCount.xyMapList,
        },
      };
    },
    pre(data) { // prehandle function
      delete data.selectedWarnStatus;
      if (data.selectedProject == -1) {
        delete data.selectedProject;
      }
      if (data.selectedIndex == -1) {
        delete data.selectedIndex;
      }
      return {
        projectId: data.selectedProject,
        indexId: data.selectedIndex,
        startDate: data.beginDate,
        endDate: data.endDate,
        minAvgResponseTime: data.startAverageResponseTime,
        maxAvgResponseTime: data.endAverageResponseTime,
      };
    },
  },
);

addConverter(
  'POST', // request method
  '/dashboardWarningConfig/api', // request url
  {
    post(data) { // posthandle function
      // console.log(data)
    },
    pre(data) { // prehandle function
      const d = {
        indexIds: data.selectedIndex && data.selectedIndex.join(','),
        projectId: data.selectedProject,
        recordCost: data.responseTime,
        warningNum: data.responseTimeWarnTimes,
        mailTo: data.mailTo,
      };

      if (!d.indexIds) {
        delete d.indexIds;
      }

      return d;
    },
  },
);
