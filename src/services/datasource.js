import { request } from '../utils';
import buildService from './buildService';
import _ from 'underscore';

const baseService = buildService('datasource');

export default {
  ...baseService,

  // 索引数据源执行导入
  async importData(param) {
    const data = _.omit(param, 'indexId');
    return request(
      `${baseService.requestHost}/schedule/import/${param.indexId}`,
      { method: 'PUT', data },
    );
  },

  // 索引数据源执行导入
  async importIncrementData(param) {
    return request(
      `${baseService.requestHost}/schedule/importInc?indexId=${param.indexId}&dataSourceId=${param.dataSourceId?param.dataSourceId:''}`,
      { method: 'GET' },
    );
  },

  // 设置数据源定时构建时机
  async saveBuild(params) {
    return request(
      `${baseService.requestHost}/build/${params.indexId}`,
      { method: 'POST', data: params },
    );
  },

  // 设置数据源定时构建时机
  async getBuild(indexId) {
    return request(
      `${baseService.requestHost}/build/${indexId}`,
      { method: 'GET' },
    );
  },

  async outputAPIDemo({ entity, type, indexId }) {
    let data = {};
    let apiAction;

    if (type === 'full') {
      if (+entity.fullDatasourceType === 0) {
        apiAction = 'fullRpc';
        data = entity.fullDatasourceRpc;
      }
    } else if (type === 'inc') {
      if (+entity.incDatasourceType === 0) {
        apiAction = 'incRpc';
        data = entity.incDatasourceRpc;
      }
    } else if (type === 'incCallback') {
      if (+entity.incDatasourceType === 0) {
        apiAction = 'incRpcCallback';
        data = entity.incDatasourceRpc;
      }
    }

    return request(
      `${baseService.requestHost}/example/${apiAction}/${indexId}`,
      { method: 'POST', data },
    );
  },

  async outputResultDemo({ entity, fullTestRequest, incTestRequest, type, indexId }) {
    let data = {};
    const { fullDatasourceRpc, incDatasourceRpc } = entity;

    let apiAction;

    if (type === 'full') {
      if (+entity.fullDatasourceType === 0) {
        apiAction = 'fullRpc';
        data = { ...fullTestRequest, fullDatasourceRpc };
      }
    } else if (type === 'incCallback') {
      if (+entity.incDatasourceType === 0) {
        apiAction = 'incRpcCallback';
        data = { ...incTestRequest, incDatasourceRpc };
      }
    }

    // todo: 请求是动态的
    return request(
      `${baseService.requestHost}/test/${apiAction}/${indexId}`,
      { method: 'POST', data },
    );
  },

  async scheduleList(params) {
    return request(
      `${baseService.requestHost}/schedule`,
      {
        method: 'GET',
        data: params,
      },
    );
  },

  async incList(indexId) {
    return request(
      `${baseService.requestHost}/inc?indexId=${indexId}`,
      {
        method: 'GET',
      },
    );
  },

  async scheduleDetail(id) {
    return request(
      `${baseService.requestHost}/schedule/${id}`,
      {
        method: 'GET',
      },
    );
  },

  async interrupt(id) {
    return request(
      `${baseService.requestHost}/schedule/interrupt/${id}`,
      {
        method: 'PUT',
      },
    );
  },

  async pause(id) {
    return request(
      `${baseService.requestHost}/schedule/pause/${id}`,
      {
        method: 'PUT',
      },
    );
  },

  async resume(id) {
    return request(
      `${baseService.requestHost}/schedule/resume/${id}`,
      {
        method: 'PUT',
      },
    );
  },

  async getLogUrl(id) {
    return request(
      `${baseService.requestHost}/schedule/getLogUrl/${id}`,
      {
        method: 'GET',
      },
    );
  },
};
