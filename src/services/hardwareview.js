import { request, addConverter } from '../utils';
import buildService from './buildService';

const baseService = buildService('dashboardResourceInfo');// 实参为向后端发起请求的实际路径前缀的一部分

export default {
  ...baseService,

  async list(params) {
    return request(
      `${baseService.requestHost}/view`,
      {
        method: 'GET',
        data: params,
      },
    );
  },

  async clusterList() {
    return request(
      `${baseService.requestHost}/clusterDict`,
      {
        method: 'GET',
      },
    );
  },

  async chartList(params) {
    return request(
      `${baseService.requestHost}/viewByList`,
      {
        method: 'GET',
        data: params,
      },
    );
  },

  async saveDetail(entity) {
    return request(
      '/dashboardWarningConfig/hardware',
      {
        method: 'POST',
        data: entity,
      },
    );
  },
};


addConverter(
  'GET', // request method
  `${baseService.requestHost}/view`, // request url
  {
    post(data) { // posthandle function
      const result = {
        statisticsList: [],
        chart: {
          dataSet: [],
        },
      };

      data.datas.forEach((item, i) => {
        let hardDiskUsageRate = '';
        item.diskConditonDTOList &&
        item.diskConditonDTOList.forEach((diskItem) => {
          hardDiskUsageRate += `${diskItem.nodeIp}: ${diskItem.diskOccupancy} \n`;
        });
        result.statisticsList.push({
          dateTime: item.date,
          clusterName: item.clusterName,
          nodeNum: item.nodeNum,
          healthyNodeNum: item.healthNum,
          nodeHealthRate: item.healthRate,
          hardDiskUsageRate,
          key: `${i}_${new Date().getTime()}`,
        });

        result.chart.dataSet.push({
          date: item.dataDate,
          indexAverageConsumedTime: item.avgCost,
          successRate: item.actualSuccessRate,
        });
      });

      return {
        datas: result,
        total: data.total,
      };
    },
    pre(data) { // prehandle function
      if (data.selectedCluster == '-1') {
        delete data.selectedCluster;
      }
      return {
        date: data.date,
        clusterName: data.selectedCluster,
        healthRateSmall: data.startNodeHealthRate,
        healthRateLarge: data.endNodeHealthRate,
        diskOccupancySmall: data.startHardDiskUsageRate,
        diskOccupancyLarge: data.endHardDiskUsageRate,
        pageNo: data.pageNo,
        pageSize: data.pageSize,
      };
    },
  },
);

addConverter(
  'GET', // request method
  `${baseService.requestHost}/clusterDict`, // request url
  {
    post(data) { // posthandle function
      const result = {};
      data.forEach((item) => {
        result[item.name] = item.description;
      });
      return result;
    },
    pre(data) { // prehandle function

    },
  },
);

addConverter(
  'GET', // request method
  `${baseService.requestHost}/viewByList`, // request url
  {
    post(data) { // posthandle function
      const dataSet = {
        diskOccupancyRate: {
          x: {},
          serieData: [],
        },
        sorlHealthRate: {
          x: {},
          serieData: [],
        },
      };

      const diskOccupancyRate = dataSet.diskOccupancyRate;
      const sorlHealthRate = dataSet.sorlHealthRate;

      data.diskOccupancyRate.forEach((item) => {
        const serie = {
          name: item.name,
          y: [],
        };
        item.xyMapList.forEach((item) => {
          diskOccupancyRate.x[item.x] = item.x;
          serie.y.push(percentStrToNumber(item.y));
        });
        diskOccupancyRate.serieData.push(serie);
      });
      diskOccupancyRate.x = Object.keys(diskOccupancyRate.x).sort((a, b) => a < b);

      data.sorlHealthRate.forEach((item) => {
        const serie = {
          name: item.name,
          y: [],
        };
        item.xyMapList.forEach((item) => {
          sorlHealthRate.x[item.x] = item.x;
          serie.y.push(percentStrToNumber(item.y));
        });
        sorlHealthRate.serieData.push(serie);
      });
      sorlHealthRate.x = Object.keys(sorlHealthRate.x).sort((a, b) => a < b);

      return { dataSet };
    },
    pre(data) { // prehandle function
      if (data.selectedCluster == -1) {
        delete data.selectedCluster;
      }
      if (data.selectedWarnStatus == -1) {
        delete data.selectedWarnStatus;
      }

      return {
        date: data.date,
        clusterName: data.selectedCluster,
        healthRateSmall: data.startNodeHealthRate,
        healthRateLarge: data.endNodeHealthRate,
        diskOccupancySmall: data.startHardDiskUsageRate,
        diskOccupancyLarge: data.endHardDiskUsageRate,
      };
    },
  },
);

addConverter(
  'POST', // request method
  '/dashboardWarningConfig/hardware', // request url
  {
    post(data) { // posthandle function
      // console.log(data)
    },
    pre(data) { // prehandle function
      return {
        diskOccupancyRate: data.hardDiskUsageRate,
        solrHealthRate: data.nodeHealthRate,
        mailTo: data.mailTo,
      };
    },
  },
);


function percentStrToNumber(s) {
  if (typeof s === 'number') {
    return s;
  }
  if (/%$/.test(s)) {
    return Number(s.replace(/%$/, '')) / 100;
  }

  return Number(s);
}
