exports.response = function response(req, res) {
  return {
    datas: [
      {
          id: 1,
          version: 'v1',
          name: 'rent_house',
          createdByName: '陆怡',
          shardCount: 1,
          replicationFactor: 2,
          configVersion: 2,
          status: 1
      },
      {
          id: 2,
          version: 'v2',
          name: 'second_house',
          createdByName: '陆怡',
          shardCount: 10,
          replicationFactor: 2,
          configVersion: 2,
          status: 0
      },
    ],
    pageNo: 1,
    pageSize: 10,
    totalPages: 3,
    total: 25,
  };
};
