exports.response = function response(req, res) {
  return {
    "basicInfo": {
      "createBy": 0,
      "createByName": "string",
      "engineType": 0,
      "host": "string",
      "id": 0,
      "indexAliasName": "string",
      "indexName": "string",
      "introduction": "string",
      "maxShardsPerNode": 0,
      "metadataStatus": 0,
      "projectId": 0,
      "replicationFactor": 0,
      "shardCount": 0,
      "status": 0
    },
    "fields": [
      {
        "boost": 0,
        "copyToField": "suggestion",
        "defaultValue": "",
        "fieldComment": "string",
        "fieldName": "fieldName",
        "fieldTypeId": 0,
        "id": 0,
        "indexed": false,
        "multiValued": false,
        "required": false,
        "stored": false,
        "termVectors": false,
        "unqueKey": false
      },
      {
        "boost": 0,
        "copyToField": "suggestion",
        "defaultValue": "",
        "fieldComment": "string",
        "fieldName": "fieldName",
        "fieldTypeId": 1,
        "id": 1,
        "indexed": true,
        "multiValued": true,
        "required": true,
        "stored": true,
        "termVectors": true,
        "unqueKey": true
      }
    ],
  };
};
