exports.response = function response(req, res) {
  return {
    datas: [
      {
          id: 1,
          type: '1',
          pattern: '1',
          time: '2017-04-26 11:11:11',
          status: '0',
      },
      {
          id: 2,
          type: '2',
          pattern: '2',
          time: '2017-04-25 11:11:11',
          status: '1',
      },
    ],
    relatedData: {
      datasourceList: [
        {
            id: 1,
            version: 'v1',
            name: 'datasource_1',
            creator: '陆怡',
            fullType: 'JSON_RPC',
            incrementalType: 'JSON_RPC',
        },
        {
            id: 2,
            version: 'v2',
            name: 'datasource_2',
            creator: '陆怡',
            fullType: 'JSON_RPC',
            incrementalType: 'JSON_RPC',
        },
      ],
      indexData: {
        id: 1,
        version: 'v1',
        name: 'rent_house',
        creator: '陆怡',
        slipCount: 1,
        copyCount: 2
      },
      appData: {
        id: 1,
        code: 'app',
        name: '好房app',
        operator: '1',
        status: '1'
      },
    },
  };
};
