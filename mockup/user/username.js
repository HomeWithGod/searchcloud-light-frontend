exports.response = function response(req, res) {
  return {
    "account": "string",
    "systemId": "string",
    "token": "string",
    "userId": 0,
    "userName": "string"
  }
}