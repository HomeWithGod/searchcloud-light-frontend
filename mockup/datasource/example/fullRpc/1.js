exports.response = function response(req, res) {
  return {
      "request": {
        "id": "78aba412-10c1-47c6-88bd-06400918b685",
        "jsonrpc": "2.0",
        "method": "callMethod",
        "params": [
          {
            "before": 1490254330,
            "pageNo": 1,
            "pageSize": 1000
          }
        ]
      },
      "response": {
        "id": "78aba412-10c1-47c6-88bd-06400918b685",
        "jsonrpc": "2.0",
        "result": {
            "datas": [],
            "errorCode": 1,
            "errorMessage": "参数错误",
            "total": 0
        }
      },
      url: "http://example.domain/path/to"
    };
};
