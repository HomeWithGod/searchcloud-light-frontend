exports.response = function response(req, res) {
  return {
    datas: [
      {
          id: 1,
          version: 'v1',
          name: 'datasource_1',
          creator: '陆怡',
          fullDatasourceType: 'JSON_RPC',
          incDatasourceType: 'JSON_RPC',
      },
      {
          id: 2,
          version: 'v2',
          name: 'datasource_2',
          creator: '陆怡',
          fullDatasourceType: 'JSON_RPC',
          incDatasourceType: 'JSON_RPC',
      },
    ]
  };
}
