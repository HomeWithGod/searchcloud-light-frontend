exports.response = function response(req, res) {
  return {
    datas: [
      {
        id: 1,
        projectCode: 'app',
        projectName: '好房app',
        masterUserName: '聂云飞',
        status: 1
      },
      {
        id: 2,
        projectCode: 'sop',
        projectName: '好房拓',
        masterUserName: '张弼',
        status: 0
      },
    ],
    pageNo: 1,
    pageSize: 10,
    totalPages: 3,
    total: 25,
  };
};
