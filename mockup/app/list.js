exports.response = function response(req, res) {
  return {
    datas: [
      {
        id: 1,
        code: 'app',
        name: '好房app',
        operator: '聂云飞',
        status: 1
      },
      {
        id: 2,
        code: 'sop',
        name: '好房拓',
        operator: '张弼',
        status: 0
      },
    ],
    pageNo: 1,
    pageSize: 10,
    totalPages: 3,
    total: 25,
  };
};
